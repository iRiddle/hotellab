import React from "react";
import { connect } from "react-redux";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Table,
  Row,
  Col
} from "reactstrap";
import { withTranslation } from "react-i18next";
import { formatDate } from "../utils";
import { MONTHLABELS } from "../constants";
import { fetchInitialStateSalesRate, SetFilterDate } from "./../redux/actions";

class DateRangePicker extends React.Component {
  constructor(props) {
    super(props);
    // this.toggle = this.toggle.bind(this);
    // this.selectDate = this.selectDate.bind(this);
    // this.onClickOnDate = this.onClickOnDate.bind(this);
    // this.onMouseEnterOnDate = this.onMouseEnterOnDate.bind(this);

    let d = new Date();
    this.state = {
      minDate: new Date(3600 * 24 * 1000),
      dropdownOpen: false,
      dateSelecting: false,
      startDate: new Date(props.dateStart),
      endDate: new Date(props.dateEnd),
      currentMonth: d.getMonth(),
      currentYear: d.getFullYear()
    };
  }

  toggle = () => {
    if (this.state.dropdownOpen) this.cancelDateSelecting(this.state);
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  };
  // selectDate(date) {
  selectDate = date => {
    // this.setState({
    //   currentOption: currentOption
    // })
    // this.props.onChange(currentOption);
  };
  nextMonth = () => {
    let y = this.state.currentYear;
    let m = this.state.currentMonth + 1;
    if (m >= 12) {
      y++;
      m = 0;
    }

    this.setState({
      currentMonth: m,
      currentYear: y
    });
  };
  prevMonth = () => {
    let y = this.state.currentYear;
    let m = this.state.currentMonth - 1;
    if (m < 0) {
      y--;
      m = 11;
    }

    this.setState({
      currentMonth: m,
      currentYear: y
    });
  };
  isDateLesThanMD(date) {
    return (
      date.getFullYear() < this.state.minDate.getFullYear() ||
      (date.getFullYear() === this.state.minDate.getFullYear() &&
        date.getMonth() < this.state.minDate.getMonth())
    );
  }

  onClickOnDate = (y, m, d) => {
    let date = new Date(y, m, d);
    if (this.state.dateSelecting === false) {
      this.setState({
        dateSelecting: true,
        selectedDate: date,
        mouseDate: new Date(y, m, d),
        dropdownOpen: false
      });
    } else {
      this.setState({
        dateSelecting: false,
        startDate: new Date(this.state.selectedDate.getTime()),
        endDate: date,
        dropdownOpen: true
      });
    }
  };

  parseDate(date) {
    var mm = date.getMonth() + 1;
    var dd = date.getDate();
    var yy = date.getFullYear();
    return dd + "." + mm + "." + yy;
  }

  cancelDateSelecting(dateCancel) {
    var postDate = {
      start: this.parseDate(dateCancel.startDate),
      end: this.parseDate(dateCancel.endDate)
    };
    this.props.SetFilterDate(this.props.typeDate, postDate);

    this.setState({
      dateSelecting: false
    });
  }

  onMouseEnterOnDate = (y, m, d) => {
    if (this.state.dateSelecting === true) {
      this.setState({
        mouseDate: new Date(y, m, d)
      });
    }
  };

  render() {
    const { t } = this.props;
    let tromToText =
      formatDate(this.state.startDate) + " - " + formatDate(this.state.endDate);
    let tableRows = [];
    let d = new Date(this.state.currentYear, this.state.currentMonth); // первый день месяца
    d.setDate(-(d.getDay() - 2)); // ближайшый предидущий понедельник
    let lastCDate = new Date(
      this.state.currentYear,
      this.state.currentMonth + 1
    ); // первый день месяца
    lastCDate.setDate(-1);

    let n = 0;
    while (d < lastCDate) {
      var dates = [];
      for (var i = 0; i < 7; i++) {
        var cn = "";
        var _y = d.getFullYear();
        var _m = d.getMonth();
        var date = d.getDate();

        if (d.getMonth() < this.state.currentMonth) {
          cn += " prev-month";
        } else if (d.getMonth() > this.state.currentMonth) {
          cn += " next-month";
        } else {
          cn += " current-month";
        }
        if (this.state.dateSelecting) {
          if (
            date === this.state.selectedDate.getDate() &&
            _m === this.state.selectedDate.getMonth()
          )
            cn += " start-date";
          if (
            date === this.state.mouseDate.getDate() &&
            _m === this.state.mouseDate.getMonth()
          )
            cn += " end-date";
          if (d > this.state.selectedDate && d < this.state.mouseDate)
            cn += " selected-date";
        } else {
          if (
            date === this.state.startDate.getDate() &&
            _m === this.state.startDate.getMonth()
          )
            cn += " start-date";
          if (
            date === this.state.endDate.getDate() &&
            _m === this.state.endDate.getMonth()
          )
            cn += " end-date";
          if (d > this.state.startDate && d < this.state.endDate)
            cn += " selected-date";
        }

        dates.push(
          <td
            className={cn}
            key={date}
            data-y={_y}
            data-m={_m}
            data-d={date}
            onMouseEnter={this.onMouseEnterOnDate.bind(null, _y, _m, date)}
            onClick={this.onClickOnDate.bind(null, _y, _m, date)}
          >
            <span className="date">{date}</span>
          </td>
        );

        d.setDate(d.getDate() + 1);
        // dates.push(<td className={cn} key={date} data-y={_y} data-m={_m} data-d={date} onClick={(event) => {
        //   var y = event.target.getAttribute('data-y');
        //   var m = event.target.getAttribute('data-m');
        //   var d = event.target.getAttribute('data-d');
        //   console.log(event.target,y,m,d);

        //   this.onClickOnDate(y,m,d)
        // }}><span className="date">{date}</span></td>)
      }
      tableRows.push(<tr key={n++}>{dates}</tr>);
    }
    return (
      <Dropdown
        className="date_range_picker"
        isOpen={this.state.dropdownOpen}
        toggle={this.toggle}
      >
        <DropdownToggle outline color="primary" onClick={this.toggle}>
          <input type="text" disabled value={tromToText} />
        </DropdownToggle>
        <DropdownMenu>
          <Row className="controls">
            <Col className="text-center">
              <span className="year">{this.state.currentYear}</span>
              <span
                className="month"
                style={{ minWidth: "100px", display: "inline-block" }}
              >
                {t(MONTHLABELS[this.state.currentMonth])}
              </span>
              {this.isDateLesThanMD(
                new Date(this.state.currentYear, this.state.currentMonth - 1)
              ) ? (
                ""
              ) : (
                <button onClick={this.prevMonth} className="prev">
                  &lt;-
                </button>
              )}
              <button onClick={this.nextMonth} className="next">
                -&gt;
              </button>
            </Col>
          </Row>
          <Table bordered>
            <thead>
              <tr>
                <th>{t("daysCalendar.mon")}</th>
                <th>{t("daysCalendar.tue")}</th>
                <th>{t("daysCalendar.wed")}</th>
                <th>{t("daysCalendar.th")}</th>
                <th>{t("daysCalendar.fri")}</th>
                <th>{t("daysCalendar.sat")}</th>
                <th>{t("daysCalendar.sun")}</th>
              </tr>
            </thead>
            <tbody>{tableRows}</tbody>
          </Table>
        </DropdownMenu>
      </Dropdown>
    );
  }
}

const mapStateToProps = state => {
  const getData = state;
  // return { tableData, filters, updateDate };
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  SetFilterDate: (typeDate, postDate) => {
    dispatch(SetFilterDate(typeDate, postDate));
    dispatch(fetchInitialStateSalesRate());
  }
});

export default withTranslation()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DateRangePicker)
);
