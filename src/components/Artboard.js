import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Container, Row, Col } from "reactstrap";
import ReactEcharts from "echarts-for-react";

import Panel from "./Panel";
import CurrentSalesTable from "./CurrentSalesTable";
import NearestSalesPanel from "./NearestSalesPanel";
import Select from "./ChildComponents/Select";

import { fetchInitialStateArtboard } from "../redux/actions";
import {
  getArtboardData,
  getCurrentHotel,
  getLastUpdateDate,
  getCurrencySymbolSelector
} from "../redux/selectors";

import { formatMoney } from "../utils";

const isEmpty = require("lodash/isEmpty");

class Artboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentNds: 0,
      currentBreakfast: 0,
      sales: this.props.data.sales,
      nsales: this.props.data.nsales,
      keyMetr: this.props.data.metrics,
      rating: this.props.data.rating || {},
      ndsArray: [
        { id: 0, title: "С НДС" },
        { id: 1, title: "Без НДС" }
      ],
      breakfastArray: [
        { id: 0, title: "С завтраком" },
        { id: 1, title: "Без завтрака" }
      ],
      titlesArray: [
        { id: 0, title: "Прошлым годом на эту дату" },
        { id: 1, title: "Бюджетом" }
      ],
      currentTitle: 0,
      currentLanguage: localStorage.getItem("i18nextLng")
    };
  }

  // TODO: add to static breakfast translate array

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.data.sales !== prevState.sales ||
      nextProps.data.nsales !== prevState.nsales ||
      nextProps.data.metrics !== prevState.keyMetr ||
      nextProps.data.rating !== prevState.rating
    ) {
      return {
        sales: nextProps.data.sales,
        nsales: nextProps.data.nsales,
        keyMetr: nextProps.data.metrics,
        rating: nextProps.data.rating
      };
    }
    if (localStorage.getItem("i18nextLng") !== nextProps.currentLanguage) {
      return {
        titlesArray: [
          { id: 0, title: nextProps.t("lastYearThisDate") },
          { id: 1, title: nextProps.t("byBudget") }
        ],
        ndsArray: [
          { id: 0, title: nextProps.t("vatIncluded") },
          { id: 1, title: nextProps.t("vatWithout") }
        ]
      };
    }
    return;
  }

  componentDidMount() {
    document.title = "hotellab - Dashboard ⚠";
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentNds, currentTitle, currentBreakfast } = this.state;
    if (
      prevProps.hotelId !== this.props.hotelId ||
      prevState.currentNds !== currentNds ||
      prevState.currentBreakfast !== currentBreakfast ||
      prevState.currentTitle !== currentTitle
    ) {
      this.props.fetchInitialStateArtboard(
        currentNds,
        currentBreakfast,
        currentTitle
      );
    }
  }

  monthParse(d) {
    let newDays = new Array();
    const { t } = this.props;
    for (let i = 0; i < d.length; i++) {
      let days = [
        "None",
        t("months.january"),
        t("months.february"),
        t("months.march"),
        t("months.april"),
        t("months.may"),
        t("months.june"),
        t("months.july"),
        t("months.august"),
        t("months.september"),
        t("months.october"),
        t("months.november"),
        t("months.december")
      ];
      newDays[i] = days[d[i]];
    }
    newDays[3] = t("months.total");
    return newDays;
  }

  selectNds = id => {
    id === "С НДС" || id === "VAT included"
      ? this.setState({
          currentNds: 0
        })
      : this.setState({
          currentNds: 1
        });
  };

  selectBreakfast = id => {
    id === "С завтраком" || id === "With breakfast"
      ? this.setState({
          currentBreakfast: 0
        })
      : this.setState({
          currentBreakfast: 1
        });
  };

  selectTitle = id => {
    id === "Прошлым годом на эту дату" || id === "Last year on this date"
      ? this.setState({
          currentTitle: 0
        })
      : this.setState({
          currentTitle: 1
        });
  };

  handleCompareBudget = () => {
    this.setState({
      isComparedBudget: !this.state.isComparedBudget
    });
  };

  render() {
    const { data, updatedDate, t, currencySymbol } = this.props;
    let sales = {};
    let nsales = [];
    let keyMetr = [];
    let ratingChart;

    if (!isEmpty(data)) {
      sales = {
        head: this.monthParse(this.state.sales.head),
        data: {
          [t("constants.loading")]: this.state.sales.data.load,
          ADR: this.state.sales.data.ADR,
          RevPAR: this.state.sales.data.RevPAR,
          [t("constants.income")]: this.state.sales.data.profit
        }
      };
      nsales = [
        {
          title: t("constants.incomes"),
          x: this.state.nsales.profit.x,
          y: this.state.nsales.profit.y
        },
        {
          title: t("constants.loading"),
          x: this.state.nsales.load.x,
          y: this.state.nsales.load.y
        },
        {
          title: "ADR",
          x: this.state.nsales.ADR.x,
          y: this.state.nsales.ADR.y
        },
        {
          title: "RevPAR",
          x: this.state.nsales.RevPAR.x,
          y: this.state.nsales.RevPAR.y
        }
      ];
      keyMetr = [
        {
          title: t("averageLength"),
          val: this.state.keyMetr.residence.val,
          good: this.state.keyMetr.residence.good,
          diff: this.state.keyMetr.residence.diff
        },
        {
          title: t("cancellations"),
          val: this.state.keyMetr.cancel.val,
          good: this.state.keyMetr.cancel.good,
          diff: this.state.keyMetr.cancel.diff
        },
        {
          title: t("commissions"),
          val: `${formatMoney(
            this.state.keyMetr.commition.val
          )} ${currencySymbol}`,
          good: this.state.keyMetr.commition.good,
          diff: this.state.keyMetr.commition.diff
        },
        {
          title: t("averageBooking"),
          val: this.state.keyMetr.bookingdepth.val,
          good: this.state.keyMetr.bookingdepth.good,
          diff: this.state.keyMetr.bookingdepth.diff
        }
      ];
      let rating = this.state.rating;
      if (rating.length === 0) {
        var ratingOptions = {};
        ratingChart = (
          <div className="eChartNoneValue">{t("noReservations")}</div>
        );
      } else {
        rating.sort((v1, v2) => v1[0] - v2[0]);
        var ratingOptions = {
          dataset: {
            source: rating
          },
          grid: { containLabel: true },
          xAxis: {
            show: false
          },
          yAxis: {
            type: "category",
            axisLine: {
              show: false
            },
            axisTick: {
              show: false
            },
            axisLabel: {
              show: true,
              fontSize: 18
            }
          },
          visualMap: {
            show: false,
            min: rating[0][0],
            max: rating[rating.length - 1][0],
            dimension: 0,
            inRange: {
              color: ["red", "green"]
            }
          },
          series: [
            {
              type: "bar",
              barWidth: 5,
              label: {
                normal: {
                  show: true,
                  position: "right",
                  fontSize: 16
                }
              },
              encode: {
                x: "amount",
                y: "product"
              }
            }
          ]
        };
        ratingChart = (
          <ReactEcharts style={{ height: "350px" }} option={ratingOptions} />
        );
      }
    }
    const {
      currentNds,
      currentBreakfast,
      ndsArray,
      titlesArray,
      breakfastArray,
      currentTitle
    } = this.state;
    return (
      <Container fluid className="sales">
        <Row className="filter panel ds-panel">
          <Col xs="12" sm="12" lg="12" xl="12">
            <div className="sales-depth__filters">
              <div className="custom-ml-dropdown">
                <Select
                  items={ndsArray}
                  selectedItem={currentNds}
                  onClick={this.selectNds}
                ></Select>
              </div>
              <div className="custom-ml-dropdown">
                <Select
                  items={breakfastArray}
                  selectedItem={currentBreakfast}
                  onClick={this.selectBreakfast}
                ></Select>
              </div>
            </div>
          </Col>
        </Row>

        <Row className="container-row">
          <Col sm="8" xs="12">
            <div className="sales__table-data">
              <Panel
                id="1-1"
                description="Текст подсказки 1.1"
                title={
                  <Select
                    items={titlesArray}
                    selectedItem={currentTitle}
                    onClick={this.selectTitle}
                    className="form-control-min-width"
                    column
                  ></Select>
                }
                preTitle={t("currentSales3Months")}
              >
                <CurrentSalesTable
                  data={sales}
                  currencySymbol={currencySymbol}
                />
              </Panel>
            </div>
          </Col>
          <Col sm="4" xs="12">
            <Panel
              id="1-2"
              title={t("keyMetrics")}
              description="Текст подсказки 1.2"
            >
              <Row className="key-metrics">
                {keyMetr.map((v, i) => {
                  return (
                    <Col
                      sm="6"
                      xs="12"
                      key={i}
                      className={"metric " + (v.good ? "good" : "bad")}
                    >
                      <h6 className="title">{v.title}</h6>
                      <h2 className="val">{v.val}</h2>
                      <span className="diff">
                        {v.diff > 0 ? "↗ +" : "↘ -"}
                        {v.diff}% {t("constants.lastYear")}
                      </span>
                    </Col>
                  );
                })}
              </Row>
            </Panel>
          </Col>
        </Row>

        <Row className="container-row">
          <Col sm="8" xs="12">
            <NearestSalesPanel data={nsales} currencySymbol={currencySymbol} />
          </Col>
          <Col sm="4" xs="12">
            <Panel
              id="2-2"
              title={t("rateLast7days")}
              description="Текст подсказки 2.2"
            >
              {ratingChart}
            </Panel>
          </Col>
        </Row>
        <Row className="footer">
          <Col>
            <p className="updated">
              <span className="text">{`${t("lastUpdate")}:`}</span>
              <span className="date">{updatedDate}</span>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  data: getArtboardData(state),
  hotelId: getCurrentHotel(state),
  updatedDate: getLastUpdateDate(state),
  currencySymbol: getCurrencySymbolSelector(state)
});

const mapDispatchToProps = dispatch => ({
  fetchInitialStateArtboard: (nds, breakfast, currentTitle) => {
    dispatch(fetchInitialStateArtboard(nds, breakfast, currentTitle));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(Artboard));
