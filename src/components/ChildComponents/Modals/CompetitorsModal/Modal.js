import React from "react";
import cx from "classnames";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Button from "../../Button";

import "./style.css";

const ModalWindow = ({
  isOpen,
  closeModal,
  modalTitle,
  btnClassName,
  consumers,
  addHotelCompetitor,
  checkHotelCompetitor,
  stepCompetitors
}) => {
  return (
    <Modal isOpen={isOpen} toggle={closeModal}>
      <ModalHeader>{modalTitle}</ModalHeader>
      <ModalBody>
        {!consumers.length ? (
          <h4>Вы не выбрали ни одного конкурента</h4>
        ) : (
          consumers.map((consumer, index) => (
            <div key={consumer.hotel_id} className="consumer">
              <label
                class={cx("consumer__title", { item_even: index % 2 === 0 })}
              >
                {consumer.hotel_title}
                <input
                  type="checkbox"
                  onClick={() => checkHotelCompetitor(consumer)}
                  checked={stepCompetitors.some(i => i === consumer)}
                />
                <span class="checkmark"></span>
              </label>
            </div>
          ))
        )}
      </ModalBody>
      <ModalFooter>
        <Button
          onClick={closeModal}
          title="Отмена"
          className={cx("disabled", btnClassName)}
        />
        <Button
          onClick={addHotelCompetitor}
          title="Применить"
          className={btnClassName}
        />
      </ModalFooter>
    </Modal>
  );
};

export default ModalWindow;
