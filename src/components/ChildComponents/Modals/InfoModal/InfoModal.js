import React from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Button from "../../Button";

const InfoModal = ({
  isOpen,
  closeModal,
  modalTitle,
  message,
  btnClassName
}) => {
  return (
    <Modal isOpen={isOpen} toggle={closeModal}>
      <ModalHeader>{modalTitle}</ModalHeader>
      <ModalBody>
        <span className="label">{message}</span>
      </ModalBody>
      <ModalFooter>
        <Button onClick={closeModal} title="Хорошо" className={btnClassName} />
      </ModalFooter>
    </Modal>
  );
};

export default InfoModal;
