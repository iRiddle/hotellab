import React from "react";
import cx from "classnames";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Button from "../../Button";
import Input from "../../Input";

import "./style.css";

const OtherSettingsModal = ({
  isOpen,
  closeModal,
  modalTitle,
  price_edges,
  btnClassName,
  currentPrice,
  handlePriceLevel,
  addPriceLevel,
  removePriceLevel,
  addOtherSettings,
  otherSettings,
  handleOtherInfo,
  excl_list,
  removeExclData,
  exclName,
  handleExclName,
  addExclName
}) => {
  const { cost, name } = currentPrice;
  const { profile_name, profile_group, current_category } = otherSettings;
  return (
    <Modal isOpen={isOpen} toggle={closeModal}>
      <ModalHeader>{modalTitle}</ModalHeader>
      <ModalBody>
        <Input
          label
          lableTitle="Имя профиля"
          id="osn-input"
          type="text"
          className="own-hotel-info__input"
          initial
          extraClass="own-hotel-gap"
          value={profile_name}
          onChange={e => handleOtherInfo("profile_name", e)}
        />
        <Input
          label
          lableTitle="Название группы"
          id="osg-input"
          type="text"
          className="own-hotel-info__input"
          initial
          extraClass="own-hotel-gap"
          value={profile_group}
          onChange={e => handleOtherInfo("profile_group", e)}
        />
        <Input
          label
          lableTitle="Название категории"
          id="osc-input"
          type="text"
          className="own-hotel-info__input"
          initial
          extraClass="own-hotel-gap"
          value={current_category}
          onChange={e => handleOtherInfo("current_category", e)}
        />
        <div className="price-level-primary-title">Уровни цен</div>
        {Object.keys(price_edges).map((item, index) => (
          <div
            className={cx(
              "price-level-confirmed",
              index % 2 === 0 && "price-level-confirmed_even"
            )}
          >
            <div>{price_edges[item]}</div>
            <div>
              <span>{item}</span>
              <span
                className="price-level-confirmed_remove"
                onClick={() => removePriceLevel(item)}
              >
                X
              </span>
            </div>
          </div>
        ))}

        <div className="container__price-levels">
          <div className="price-level">
            <span className="price-level-title">Название</span>
            <Input
              id="scpn-input"
              type="text"
              className="own-hotel-info__input"
              initial
              extraClass="own-hotel-gap"
              value={name}
              onChange={e => handlePriceLevel("name", e)}
            />
          </div>
          <div className="price-level">
            <span className="price-level-title">Цена</span>
            <Input
              id="scpc-input"
              type="text"
              className="own-hotel-info__input"
              initial
              extraClass="own-hotel-gap"
              value={cost}
              onChange={e => handlePriceLevel("cost", e)}
            />
          </div>
        </div>
        <div className="price-level-add">
          <Button
            onClick={addPriceLevel}
            title="+"
            className={cx(btnClassName, "price-level-width")}
            disabled={!name.length || !cost.length}
          />
        </div>

        <div className="price-level-primary-title excl-list">Список EXCL</div>
        {excl_list.length > 0 &&
          excl_list.map((item, index) => (
            <div
              className={cx(
                "own-hotel-info-history-level",
                index % 2 === 0 && "own-hotel-info-history-level_even"
              )}
            >
              <div>
                <span>{item}</span>
                <span
                  className="own-hotel-info-history-level-confirmed_remove"
                  onClick={() => removeExclData(item)}
                >
                  X
                </span>
              </div>
            </div>
          ))}
        <div className="container__excl-list">
          <span className="price-level-title">Название</span>
          <Input
            id="excl-input-list"
            type="text"
            className="own-hotel-info__input"
            initial
            extraClass="own-hotel-gap"
            value={exclName}
            onChange={handleExclName}
          />
        </div>
        <div className="price-level-add">
          <Button
            onClick={addExclName}
            title="+"
            className={cx(btnClassName, "price-level-width")}
            disabled={!exclName.length}
          />
        </div>
      </ModalBody>
      <ModalFooter>
        <Button
          onClick={closeModal}
          title="Отмена"
          className={cx("disabled", btnClassName)}
        />
        <Button
          onClick={addOtherSettings}
          title="Применить"
          className={btnClassName}
        />
      </ModalFooter>
    </Modal>
  );
};

export default OtherSettingsModal;
