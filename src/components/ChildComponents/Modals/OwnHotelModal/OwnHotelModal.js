import React from "react";
import cx from "classnames";
import isEqual from "lodash/isEqual";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

import Button from "../../Button";
import Input from "../../Input";

import "./style.css";

const OwnHotelModal = ({
  isOpen,
  closeModal,
  addOwnHotelInfoAction,
  modalTitle,
  btnClassName,
  breakfasts,
  cancels,
  ownHotelInfo,
  handleOwnHotelInfo,
  pmsCategories,
  addHistoryLevel,
  removeHistoryLevel,
  historyName,
  handleHistory
}) => {
  const {
    base_price,
    standart_category,
    person_capacity,
    breakfast,
    free_cancel
  } = ownHotelInfo;
  const { history } = pmsCategories[0].standart_category;
  return (
    <Modal isOpen={isOpen} toggle={closeModal}>
      <ModalHeader>{modalTitle}</ModalHeader>
      <ModalBody>
        <Input
          label
          lableTitle="Стандартная категория"
          id="sc-input"
          type="text"
          className="own-hotel-info__input"
          initial
          extraClass="own-hotel-gap"
          value={standart_category}
          onChange={e => handleOwnHotelInfo("standart_category", e)}
        />
        <Input
          label
          lableTitle="Базовая цена"
          value={base_price}
          onChange={e => handleOwnHotelInfo("base_price", e)}
          id="bc-input"
          type="number"
          className="own-hotel-info__input"
          initial
          max={999999}
          min={1}
          extraClass="own-hotel-gap"
        />
        <Input
          label
          lableTitle="Вместимость"
          id="cap-input"
          type="number"
          className="own-hotel-info__input"
          initial
          max={100}
          min={1}
          value={person_capacity}
          onChange={e => handleOwnHotelInfo("person_capacity", e)}
        />
        <div className="competitors-settings__breakfast">
          <span className="label">Завтрак</span>
          {breakfasts.map(bf => (
            <label className="breakfast__item" key={bf.id}>
              {bf.title}
              <input
                type="radio"
                name="breakfast"
                value={bf.value}
                onChange={e => handleOwnHotelInfo("breakfast", e)}
                checked={isEqual(breakfast, bf.value)}
              />
              <span className="breakfast__checkmark"></span>
            </label>
          ))}
        </div>
        <div className="competitors-settings__breakfast">
          <span className="label">Отмены</span>
          {cancels.map(bf => (
            <label className="breakfast__item" key={bf.id}>
              {bf.title}
              <input
                type="radio"
                name="cancels"
                value={bf.value}
                onChange={e => handleOwnHotelInfo("free_cancel", e)}
                checked={isEqual(free_cancel, bf.value)}
              />
              <span className="breakfast__checkmark"></span>
            </label>
          ))}
        </div>
        <div className="price-level-primary-title">Классы истории</div>
        {history.length > 0 &&
          history.map((item, index) => (
            <div
              className={cx(
                "own-hotel-info-history-level",
                index % 2 === 0 && "own-hotel-info-history-level_even"
              )}
            >
              <div>
                <span>{item}</span>
                <span
                  className="own-hotel-info-history-level-confirmed_remove"
                  onClick={() => removeHistoryLevel(item)}
                >
                  X
                </span>
              </div>
            </div>
          ))}
        <div className="container__price-levels">
          <span className="price-level-title">Название</span>
          <Input
            id="scown-input"
            type="text"
            className="own-hotel-info__input"
            initial
            extraClass="own-hotel-gap"
            value={historyName}
            onChange={handleHistory}
          />
        </div>
        <div className="price-level-add">
          <Button
            onClick={addHistoryLevel}
            title="+"
            className={cx(btnClassName, "price-level-width")}
            disabled={!historyName.length}
          />
        </div>
      </ModalBody>
      <ModalFooter>
        <Button
          onClick={closeModal}
          title="Отмена"
          className={cx("disabled", btnClassName)}
        />
        <Button
          onClick={addOwnHotelInfoAction}
          title="Применить"
          className={btnClassName}
        />
      </ModalFooter>
    </Modal>
  );
};

export default OwnHotelModal;
