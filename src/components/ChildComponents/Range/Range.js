import React from "react";
import { Range } from "react-range";

import "./style.css";

const RangeComponent = ({ onChangeRange, maxDistance, currentDistance }) => {
  return (
    <div className="range-container">
      <Range
        step={0.1}
        min={0}
        max={50}
        values={[currentDistance]}
        onChange={currentDistance => onChangeRange(currentDistance)}
        renderTrack={({ props, children }) => (
          <div {...props} className="range-container__line">
            {children}
          </div>
        )}
        renderThumb={() => <div className="range-container__button"></div>}
      />
      <output className="range-container__output" id="output">
        <div className="output__begin-distance">0 км</div>
        <div className="output__hint">{`${currentDistance} км`}</div>
      </output>
    </div>
  );
};

export default RangeComponent;
