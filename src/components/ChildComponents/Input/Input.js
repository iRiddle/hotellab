import React from "react";
import cx from "classnames";
import "./style.css";

const Input = ({
  id,
  type,
  placeholder,
  value,
  className,
  extraClass,
  initial,
  onChange,
  onKeyDown,
  label,
  lableTitle,
  name,
  min,
  max
}) => {
  return (
    <div className={cx(initial ? "initial" : "input", extraClass)}>
      {label && (
        <label for={id} className="label">
          {lableTitle}
        </label>
      )}
      <input
        id={id}
        type={type}
        placeholder={placeholder}
        value={value}
        className={className}
        onChange={onChange}
        name={name}
        min={min}
        max={max}
        onKeyDown={onKeyDown}
      ></input>
    </div>
  );
};

export default Input;
