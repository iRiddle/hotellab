import React, { Component } from "react";
import { Input } from "reactstrap";

import cx from "classnames";

class Select extends Component {
  render() {
    const { items, selectedItem, onClick, className, column } = this.props;
    return (
      <div
        className={cx("nds-select", { "form-control-column": column === true })}
      >
        <Input
          type="select"
          name="select"
          id="ndsSelect"
          onChange={e => onClick(e.target.value)}
          className={className}
        >
          {items.map(item => (
            <option
              key={item.id}
              className={cx("select-all", "dropdown-item", {
                active: item.id === selectedItem
              })}
              selected={item.id === selectedItem}
            >
              {item.title}
            </option>
          ))}
        </Input>
      </div>
    );
  }
}

export default Select;
