import React from "react";
import "./style.css";

const Star = ({ className, onClick }) => {
  return (
    <span className={className} onClick={onClick}>
      &#9733;
    </span>
  );
};

export default Star;
