import React from "react";
import cx from "classnames";

import Star from "../Star";

import "./style.css";

const HotelObject = ({
  id,
  title,
  stars,
  starsBuilder,
  rating,
  distance,
  className,
  selectHotel,
  selectedHotels
}) => {
  const isChecked = Object.keys(selectedHotels).some(h => h === id);
  return (
    <div className="hotel-object-container">
      <div className="hotel-object__title">
        <div>{title}</div>
        <div className="title__stars">
          {starsBuilder.slice(0, stars).map(star => (
            <Star key={star.id} className={cx(className, "star-gap")} />
          ))}
        </div>
      </div>
      <div className="hotel-object__distance">{`${distance.toFixed(
        1
      )} км. от вашего объекта`}</div>
      <div className="hotel-object__count-rooms">{`${rating} рейтинг`}</div>
      <button
        className={cx("hotel-object__check-btn", {
          "hotel-object__check-btn_checked": isChecked
        })}
        onClick={() => selectHotel(id)}
      >
        {isChecked ? "Выбрано" : "Выбрать"}
      </button>
    </div>
  );
};

export default HotelObject;
