import React, { Component } from "react";
import ReactEcharts from "echarts-for-react";
import cx from "classnames";
import { withTranslation } from "react-i18next";

import { Container, Row, Col, Table } from "reactstrap";

import Panel from "./Panel";

import {
  headerInfo,
  bookingFromSite,
  leadTable,
  brandPhrases,
  rateFutureDates
} from "../mocks/digitalMarketing";

import { formatMoney } from "../utils";

class DigitalMarketing extends Component {
  getSymbolForTitle = (data, id) => {
    switch (id) {
      case 1: {
        return `${formatMoney(data)}₽`;
      }
      case 2: {
        return data;
      }
      case 3: {
        return `${formatMoney(data)}₽`;
      }
      case 4: {
        return `${data}%`;
      }
      case 5: {
        return `${formatMoney(data)}₽`;
      }
      default: {
        return "";
      }
    }
  };

  exampleRender = () => {
    for (let i = 0; i++; i < 10) {
      this.setState({ count: i });
    }
  };

  getDigitalTraffOption = () => ({
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
      orient: "vertical",
      x: "left",
      y: "center",
      data: [
        this.props.t("constants.transitionsSearch"),
        this.props.t("constants.directCalls"),
        this.props.t("constants.internalTransitions"),
        this.props.t("constants.socialTransitions")
      ],
      left: 250,
      top: 50,
      itemGap: 30,
      icon: "circle"
    },
    series: [
      {
        name: this.props.t("constants.bookingTraffic"),
        type: "pie",
        radius: ["45%", "70%"],
        center: ["20%", "40%"],
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: false,
            position: "center"
          },
          emphasis: {
            show: true,
            textStyle: {
              fontSize: "12",
              fontWeight: "bold"
            }
          }
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        itemStyle: {
          normal: {
            color: "#009688"
          }
        },
        data: [
          {
            name: this.props.t("constants.transitionsSearch"),
            value: 25,
            itemStyle: { color: "#009688" }
          },
          {
            name: this.props.t("constants.directCalls"),
            value: 34,
            itemStyle: { color: "#A42525" }
          },
          {
            name: this.props.t("constants.internalTransitions"),
            value: 43,
            itemStyle: { color: "#55BFAD" }
          },
          {
            name: this.props.t("constants.socialTransitions"),
            value: 1,
            itemStyle: { color: "#FFC5C5" }
          }
        ]
      }
    ],
    graphic: [
      {
        type: "group", // to do fix this left side
        left: "center",
        bottom: 30,
        width: 100,
        bounding: "raw",
        children: [
          {
            type: "text",
            style: {
              fill: "#000000",
              text: "25%",
              font: 'bolder 14px "Microsoft YaHei", sans-serif'
            },
            left: 280,
            top: -216
          },
          {
            type: "text",
            style: {
              fill: "#000000",
              text: "34%",
              font: 'bolder 14px "Microsoft YaHei", sans-serif'
            },
            left: 280,
            top: -172
          },
          {
            type: "text",
            style: {
              fill: "#000000",
              text: "43%",
              font: 'bolder 14px "Microsoft YaHei", sans-serif'
            },
            left: 280,
            top: -128
          },
          {
            type: "text",
            style: {
              fill: "#000000",
              font: 'bolder 14px "Microsoft YaHei", sans-serif',
              text: "1%"
            },
            left: 280,
            top: -84
          }
        ]
      }
    ]
  });

  render() {
    const { t } = this.props;
    return (
      <Container fluid className="digital-marketing">
        <Row>
          <Col xs="7" sm="7" lg="7" xl="7">
            <div className="digital-marketing__left-side">
              <Panel style={{ marginBottom: "24px" }}>
                <div className="digital-marketing__header ">
                  {headerInfo.map(data => (
                    <div key={data.id} className="header-marketing">
                      <span className="header-marketing__title">
                        {t(`${data.title}`)}
                      </span>
                      <span
                        className={cx(
                          "header-marketing__count",
                          data.isDangerous ? "dangerous_color" : "safe_color"
                        )}
                      >
                        {this.getSymbolForTitle(data.count, data.id)}
                      </span>
                      <span className="header-marketing__statistics">
                        {data.statisticsCount > 0
                          ? `+${data.statisticsCount}%`
                          : `${data.statisticsCount}%`}{" "}
                        {t(data.statistics)}
                      </span>
                    </div>
                  ))}
                </div>
              </Panel>
              <Panel title={t("constants.leadSummaryTable")}>
                <Table className="digital-marketing__table">
                  <thead>
                    <tr className="table__tr">
                      <th></th>
                      <th>{t("constants.calls")}</th>
                      <th>{t("constants.chat")}</th>
                      <th>{t("constants.applications")}</th>
                      <th>{t("constants.totalLeads")}</th>
                      <th>{t("constants.spentRub")}</th>
                      <th>{t("constants.constOfLead")}</th>
                      <th>{t("constants.income")}</th>
                      <th>ADR</th>
                      <th>{t("constants.soldNumbers")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {leadTable.map(lead => (
                      <tr key={lead.id}>
                        <th scope="row">{t(lead.title)}</th>
                        <td>{lead.calls}</td>
                        <td>{lead.chats}</td>
                        <td>{lead.requests}</td>
                        <td>{lead.leads}</td>
                        <td>{formatMoney(lead.spentLeads)}</td>
                        <td>{lead.costLead}</td>
                        <td>{formatMoney(lead.earn)}</td>
                        <td>{formatMoney(lead.adr)}</td>
                        <td>{lead.soldNums}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Panel>
            </div>
          </Col>
          <Col xs="5" sm="5" lg="5" xl="5">
            <div className="digital-marketing__right-side">
              <Panel title={t("constants.bookingForFuture")}>
                <div className="digital-marketing__booking">
                  {bookingFromSite.map(book => (
                    <div
                      key={book.id}
                      className={cx("booking-container", book.color)}
                    >
                      <span className="booking-container__date">
                        {book.date}
                      </span>
                      <span className="booking-container__count">
                        {book.count}
                      </span>
                    </div>
                  ))}
                </div>
              </Panel>
            </div>
          </Col>
        </Row>
        <Row style={{ marginTop: "24px" }}>
          <Col xs="4" sm="4" lg="4" xl="4">
            <div className="digital-marketing__left-side">
              <Panel
                style={{ marginTop: "24px" }}
                title={t("constants.bookingTraffic")}
              >
                <ReactEcharts
                  option={this.getDigitalTraffOption()}
                ></ReactEcharts>
              </Panel>
            </div>
          </Col>
          <Col xs="3" sm="3" lg="3" xl="3">
            <div className="digital-marketing__left-side">
              <Panel title={t("constants.futureDateRating")} body="100%">
                <div className="digital-marketing__rate-future">
                  {rateFutureDates.map(rate => (
                    <div className="rate-future__item" key={rate.id}>
                      <span style={{ marginRight: "8px" }}>{rate.date}</span>
                      <div
                        style={{ width: `${rate.loaded}%`, marginRight: "8px" }}
                        className={cx("future-item__rectangle", rate.testColor)}
                      ></div>
                      <span style={{ color: rate.color }}>{rate.count}</span>
                    </div>
                  ))}
                </div>
              </Panel>
            </div>
          </Col>
          <Col xs="5" sm="5" lg="5" xl="5">
            <div className="digital-marketing__right-side">
              <Panel title={t("constants.nonBrandPhrase")}>
                <div className="digital-marketing__brand-rate">
                  {brandPhrases.map(phrase => (
                    <div className="brand-rate__data">
                      <div className="brand-rate__title">
                        <span
                          className={cx(
                            phrase.id === 0
                              ? "brand-rate__title-none-decoration"
                              : ""
                          )}
                        >
                          {t(phrase.title)}
                        </span>
                      </div>
                      <div className="brand-rate__visits">
                        <span>{t(phrase.visits)}</span>
                      </div>
                    </div>
                  ))}
                </div>
              </Panel>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(DigitalMarketing);
