import React from "react";
import { ButtonDropdown, DropdownToggle, DropdownMenu } from "reactstrap";

export default class MLCBDD extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    let currentOption = null;
    this.state = {
      title: this.props.title || "",
      dropdownOpen: false,
      currentOption,
      items: this.props.items || []
    };
    if (this.props.checked) {
      this.state.checked = this.props.checked;
    } else if (
      this.state.items.length > 0 &&
      Object.keys(this.state.items[0].items).length > 0
    ) {
      this.state.checked = Object.keys(this.state.items[0].items)[0];
    }
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  setCurrentOption(currentOption) {
    this.setState({
      currentOption: currentOption
    });
  }

  checkOption = checked => {
    this.setState({
      checked
    });
    this.props.onChange(checked);
  };

  render() {
    const { items } = this.props;
    return (
      <div className="custom-ml-dropdown">
        <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
          <DropdownToggle caret outline color="primary">
            {this.state.title}
          </DropdownToggle>
          <DropdownMenu>
            {items.map((o, n) => (
              <li
                key={n}
                className="dropdown-item"
                onMouseEnter={() => this.setCurrentOption(n)}
                onMouseLeave={() => this.setCurrentOption()}
                // onClick={()=>this.setCurrentOption(o)}
              >
                <span data-toggle="dropdown" className="dropdown-toggle">
                  {o.title}
                </span>
                {this.state.currentOption !== n ? (
                  ""
                ) : (
                  <ul className="dropdown-menu dropdown-submenu show">
                    {Object.keys(o.items).map(k => (
                      // <li key={k} className={"dropdown-item"+((this.state.checked.indexOf(k)===-1)?'':' active')} onClick={() => { this.toggleOption(k); }}>
                      <li
                        key={k}
                        className={
                          "dropdown-item" +
                          (this.state.checked.indexOf(k) === -1
                            ? ""
                            : " active")
                        }
                        onClick={() => {
                          this.checkOption(k);
                        }}
                      >
                        <span>{o.items[k]}</span>
                      </li>
                    ))}
                  </ul>
                )}
              </li>
            ))}
          </DropdownMenu>
        </ButtonDropdown>
      </div>
    );
  }
}
