import React, { Component } from "react";
import { Button, ButtonGroup, Row, Col } from "reactstrap";
import { withTranslation } from "react-i18next";
import ReactEcharts from "echarts-for-react";
import echarts from "echarts";
import Panel from "./Panel";
import { formatMoney } from "../utils";

const isEmpty = require("lodash/isEmpty");

class NearestSalesPanel extends Component {
  constructor(props) {
    super(props);
    this.state = { rSelected: 0 };
  }

  onRadioBtnClick = rSelected => {
    this.setState({ rSelected });
  };

  render() {
    const { rSelected } = this.state;
    const { t, currencySymbol } = this.props;
    let options = {
      xAxis: {
        type: "category",
        data:
          !isEmpty(this.props.data) && this.props.data[this.state.rSelected].x
      },
      yAxis: {
        type: "value",
        axisLabel: {
          formatter: function(value) {
            return rSelected === 1
              ? `${formatMoney(value)} %`
              : `${formatMoney(value)} ${currencySymbol}`;
          }
        }
      },
      tooltip: {
        trigger: "axis",
        formatter: function(a) {
          let o = a[0];
          return rSelected === 1
            ? `${o.name}: ${formatMoney(o.value)} %`
            : `${o.name}: ${formatMoney(o.value)} ${currencySymbol}`;
        }
      },
      series: [
        {
          data:
            !isEmpty(this.props.data) &&
            this.props.data[this.state.rSelected].y,
          type: "line",
          label: {
            normal: {
              show: true,
              color: "#333333",
              offset: [0, -10],
              position: "top",
              formatter: function(o) {
                return rSelected === 1
                  ? `${formatMoney(o.value)} %`
                  : `${formatMoney(o.value)} ${currencySymbol}`;
              }
            }
          },
          lineStyle: {
            normal: {
              width: 4,
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                { offset: 0, color: "#008800" },
                { offset: 1, color: "#ff0000" }
              ])
            }
          }
        }
      ]
    };

    return (
      <Panel
        id="2-1"
        title={t("salesNext7Days")}
        description="Текст подсказки 2.1"
      >
        <Row className="text-center">
          <Col>
            <ButtonGroup>
              {this.props.data.map((v, i) => {
                return (
                  <Button
                    key={i}
                    outline
                    onClick={() => this.onRadioBtnClick(i)}
                    active={this.state.rSelected === i}
                    color="info"
                  >
                    {v.title}
                  </Button>
                );
              })}
            </ButtonGroup>
          </Col>
        </Row>
        <ReactEcharts option={options} theme="grt" />
      </Panel>
    );
  }
}
export default withTranslation()(NearestSalesPanel);
