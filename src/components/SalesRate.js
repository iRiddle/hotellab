import React, { Component } from "react";
import { Container, Row, Col, ButtonGroup, Button, Table } from "reactstrap";
import cx from "classnames";
import { withTranslation } from "react-i18next";

import Wrapper from "../hocs/salesRateHoc";

import CustomSelect from "./CustomSelect";
import CheckboxDropdown from "./CheckboxDropdown";
import DateRangePicker from "./DateRangePicker";
import Select from "./ChildComponents/Select";

import { formatMoney } from "../utils";

const isEmpty = require("lodash/isEmpty");

class SalesRate extends Component {
  render() {
    const {
      parseDifference,
      dataFilter,
      fact,
      dyn,
      isEqualWithLastYear,
      stepPeriodArray,
      stepSalesArray,
      period,
      updateDate,
      daysWeek,
      formatFilterDate,
      formatDay,
      showByPeriod,
      exportToExcel,
      filterCategory,
      filterCategoryStock,
      filterSourcesStock,
      filterSources,
      handlePenultimateYear,
      currentNds,
      currentBreakfast,
      ndsArray,
      breakfastArray,
      selectNds,
      selectBreakfast,
      t,
      currencySymbol,
      setFilterOption,

      filterActiveCategory,
      filterActiveSources
    } = this.props;
    return (
      <Container fluid className="artboadr">
        <Row className="filter">
          <Col>
            <div>
              <label>
                {t("datesOfResidence")}
                <DateRangePicker
                  typeDate={"SET_SALES_RATE_DATE_RS"}
                  getPage={"SalesRate"}
                  dateStart={formatFilterDate(
                    dataFilter.filterDateStock.rs.dateStart
                  )}
                  dateEnd={formatFilterDate(
                    dataFilter.filterDateStock.rs.dateEnd
                  )}
                  showByPeriod={showByPeriod}
                  period={period}
                />
              </label>
              <label>
                {t("bookingDates")}:
                <DateRangePicker
                  typeDate={"SET_SALES_RATE_DATE_RV"}
                  getPage={"SalesRate"}
                  dateStart={formatFilterDate(
                    dataFilter.filterDateStock.rv.dateStart
                  )}
                  dateEnd={formatFilterDate(
                    dataFilter.filterDateStock.rv.dateEnd
                  )}
                  showByPeriod={showByPeriod}
                  period={period}
                />
              </label>
              <CheckboxDropdown
                onChange={v => setFilterOption("roomCategory", v)}
                title={`${t("roomCategory")}:`}
                checked={filterActiveCategory}
                items={dataFilter.category}
              />
              <CheckboxDropdown
                onChange={v => setFilterOption("source", v)}
                title={`${t("source")}`}
                checked={filterActiveSources}
                items={dataFilter.sources}
              />
            </div>
          </Col>
        </Row>
        <Row className="body">
          <Col>
            <Row className="export">
              <Col xs="4" sm="4" lg="4" xl="4">
                <ButtonGroup>
                  <span className="labelPeriod">{t("showBy")}:</span>
                  <CustomSelect
                    onChange={v => showByPeriod(v)}
                    items={{
                      day: t("showByDay"),
                      week: t("showByWeek"),
                      month: t("showByMonth"),
                      dayWeek: t("showByDayWeek")
                    }}
                    id="periodSelect"
                  />
                  <Button className="stableHeight" onClick={exportToExcel}>
                    Exсel
                  </Button>
                </ButtonGroup>
              </Col>
              <Col xs="8" sm="8" lg="8" xl="8">
                <div className="sales-depth__filters">
                  <div className="custom-ml-dropdown">
                    <Select
                      items={ndsArray}
                      selectedItem={currentNds}
                      onClick={selectNds}
                    ></Select>
                  </div>
                  <div className="custom-ml-dropdown">
                    <Select
                      items={breakfastArray}
                      selectedItem={currentBreakfast}
                      onClick={selectBreakfast}
                    ></Select>
                  </div>
                </div>
              </Col>
            </Row>
            <Row className="tables">
              <Col sm="8">
                <div className="temps-sales-header">
                  <h3 style={{ padding: "7.5px 0" }}>{t("fact")}</h3>
                </div>
                <Table striped id="table1">
                  <thead>
                    <tr>
                      <th>{t("date")}</th>
                      {period === "day" && <th>{t("dWeeks")}</th>}
                      <th>{t("soldNumbers")}</th>
                      <th>ADR</th>
                      <th>{t("income")}</th>
                      <th>{t("loading")}</th>
                      <th>RevPAR</th>
                      <th>{t("numbersForSale")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {t("total")}
                      </th>
                      {period === "day" && (
                        <th
                          style={{
                            backgroundColor: "#55bfad",
                            color: "#ffffff"
                          }}
                        />
                      )}
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {fact.dataSum.sales}
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {formatMoney(fact.dataSum.ADR)} {currencySymbol}
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {formatMoney(fact.dataSum.profit)} {currencySymbol}
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {fact.dataSum.load} %
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {fact.dataSum.RevPAR} {currencySymbol}
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {fact.dataSum.rooms}
                      </td>
                    </tr>
                    {stepPeriodArray !== undefined && stepPeriodArray.length > 0
                      ? stepPeriodArray.map((v, n) =>
                          period === "dayWeek" ? (
                            !isNaN(v.ADR) && (
                              <tr key={n}>
                                <th scope="row">{daysWeek[n]}</th>
                                {period === "day" && (
                                  <td>{formatDay(v.date)}</td>
                                )}
                                <td>{v.sales || 0}</td>
                                <td>
                                  {!isNaN(v.ADR) ? formatMoney(v.ADR) : 0}{" "}
                                  {currencySymbol}
                                </td>
                                <td>
                                  {formatMoney(v.profit) || 0} {currencySymbol}
                                </td>
                                <td>{v.load || 0}%</td>
                                <td>
                                  {parseInt(v.RevPAR) || 0} {currencySymbol}
                                </td>
                                <td>{v.rooms || 0}</td>
                              </tr>
                            )
                          ) : (
                            <tr key={n}>
                              <th scope="row">{v.date}</th>
                              {period === "day" && <td>{formatDay(v.date)}</td>}
                              <td>{v.sales || 0}</td>
                              <td>
                                {!isNaN(v.ADR) ? formatMoney(v.ADR) : 0}{" "}
                                {currencySymbol}
                              </td>
                              <td>
                                {formatMoney(v.profit) || 0} {currencySymbol}
                              </td>
                              <td>{v.load || 0}%</td>
                              <td>
                                {parseInt(v.RevPAR) || 0} {currencySymbol}
                              </td>
                              <td>{v.rooms || 0}</td>
                            </tr>
                          )
                        )
                      : fact.data.map((v, n) => (
                          <tr key={n}>
                            <th scope="row">{v.date}</th>
                            <td>{formatDay(v.date)}</td>
                            <td>{v.sales}</td>
                            <td>
                              {formatMoney(v.ADR)} {currencySymbol}
                            </td>
                            <td>
                              {formatMoney(v.profit)} {currencySymbol}
                            </td>
                            <td>{v.load} %</td>
                            <td>
                              {parseInt(v.RevPAR)} {currencySymbol}
                            </td>
                            <td>{v.rooms}</td>
                          </tr>
                        ))}
                  </tbody>
                </Table>
              </Col>
              <Col>
                <div className="temps-sales-header">
                  <h3 style={{ padding: "7.5px 0" }}>{t("salesPace")}</h3>
                  <ButtonGroup>
                    <button
                      className={cx("stateActiveBtnYearAgo btn btn-secondary", {
                        active: isEqualWithLastYear
                      })}
                      onClick={handlePenultimateYear}
                    >
                      {t("compareWithLastYear")}
                    </button>
                  </ButtonGroup>
                </div>
                <Table striped className="table-dif" id="table2">
                  <thead>
                    <tr>
                      <th>{t("r/n")}</th>
                      <th>ADR</th>
                      <th>{t("income")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {formatMoney(dyn.dataSum.kn)}
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {formatMoney(dyn.dataSum.ADR)} {currencySymbol}
                      </td>
                      <td
                        style={{ backgroundColor: "#55bfad", color: "#ffffff" }}
                      >
                        {formatMoney(dyn.dataSum.profit)} {currencySymbol}
                      </td>
                    </tr>
                    {!isEmpty(stepSalesArray) && isEqualWithLastYear !== true
                      ? stepSalesArray.map((v, n) =>
                          period === "dayWeek" ? (
                            v.isNanObj && (
                              <tr key={n}>
                                <td>{v.kn || 0}</td>
                                <td>
                                  {!isNaN(v.ADR) ? formatMoney(v.ADR) : 0}{" "}
                                  {currencySymbol}
                                </td>
                                <td>
                                  {formatMoney(v.profit) || 0} {currencySymbol}
                                </td>
                              </tr>
                            )
                          ) : (
                            <tr key={n}>
                              <td>{v.kn || 0}</td>
                              <td>
                                {!isNaN(v.ADR) ? formatMoney(v.ADR) : 0}{" "}
                                {currencySymbol}
                              </td>
                              <td>
                                {formatMoney(v.profit) || 0} {currencySymbol}
                              </td>
                            </tr>
                          )
                        )
                      : isEqualWithLastYear
                      ? parseDifference.map((v, n) => (
                          <tr key={n}>
                            <td
                              style={{
                                color:
                                  v.kn > 0 && v.isDifference
                                    ? "#50c0ad"
                                    : v.kn <= 0 && v.isDifference
                                    ? "#ef385f"
                                    : `-`
                              }}
                            >
                              <div className="difference-data-container">
                                {v.isDifference && (
                                  <>
                                    <div className="difference-data-container__field">
                                      {formatMoney(v.knSum)}
                                    </div>
                                    <div className="difference-data-container__field">
                                      <span
                                        className={cx(
                                          v.kn > 0 && v.isDifference
                                            ? "moreColor"
                                            : v.kn <= 0 && v.isDifference
                                            ? "lessColor"
                                            : `-`
                                        )}
                                      >
                                        {v.kn > 0 && v.isDifference
                                          ? "↗"
                                          : v.kn <= 0 && v.isDifference
                                          ? "↘"
                                          : `-`}
                                      </span>
                                    </div>
                                  </>
                                )}

                                <div className="difference-data-container__field">
                                  {v.isDifference
                                    ? `${formatMoney(v.kn)} %`
                                    : formatMoney(v.kn)}
                                </div>
                              </div>
                            </td>
                            <td
                              style={{
                                color:
                                  v.ADR > 0 && v.isDifference
                                    ? "#50c0ad"
                                    : v.ADR <= 0 && v.isDifference
                                    ? "#ef385f"
                                    : `-`
                              }}
                            >
                              <div className="difference-data-container">
                                {v.isDifference && (
                                  <>
                                    <div className="difference-data-container__field">
                                      {formatMoney(v.ADRSum)} {currencySymbol}
                                    </div>
                                    <div className="difference-data-container__field">
                                      <span
                                        className={cx(
                                          v.ADR > 0 && v.isDifference
                                            ? "moreColor"
                                            : v.ADR <= 0 && v.isDifference
                                            ? "lessColor"
                                            : `-`
                                        )}
                                      >
                                        {v.ADR > 0 && v.isDifference
                                          ? "↗"
                                          : v.ADR <= 0 && v.isDifference
                                          ? "↘"
                                          : `-`}
                                      </span>
                                    </div>
                                  </>
                                )}

                                <div className="difference-data-container__field">
                                  {v.isDifference
                                    ? `${formatMoney(v.ADR)} %`
                                    : formatMoney(v.ADR)}
                                </div>
                              </div>
                            </td>
                            <td
                              style={{
                                color:
                                  v.profit > 0 && v.isDifference
                                    ? "#50c0ad"
                                    : v.profit <= 0 && v.isDifference
                                    ? "#ef385f"
                                    : `-`
                              }}
                            >
                              <div className="difference-data-container">
                                {v.isDifference && (
                                  <>
                                    <div className="difference-data-container__field">
                                      {formatMoney(v.profitSum)}{" "}
                                      {currencySymbol}
                                    </div>
                                    <div className="difference-data-container__field">
                                      <span
                                        className={cx(
                                          v.profit > 0 && v.isDifference
                                            ? "moreColor"
                                            : v.profit <= 0 && v.isDifference
                                            ? "lessColor"
                                            : `-`
                                        )}
                                      >
                                        {v.profit > 0 && v.isDifference
                                          ? "↗"
                                          : v.profit <= 0 && v.isDifference
                                          ? "↘"
                                          : `-`}
                                      </span>
                                    </div>
                                  </>
                                )}

                                <div className="difference-data-container__field">
                                  {v.isDifference
                                    ? `${formatMoney(v.profit)} %`
                                    : formatMoney(v.profit)}
                                </div>
                              </div>
                            </td>
                          </tr>
                        ))
                      : dyn.data.map((v, n) => (
                          <tr key={n}>
                            <td>{v.kn}</td>
                            <td>
                              {v.ADR} {currencySymbol}
                            </td>
                            <td>
                              {formatMoney(v.profit)} {currencySymbol}
                            </td>
                          </tr>
                        ))}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="footer">
          <Col>
            <p className="updated">
              <span className="text">Последнее обновление</span>
              <span className="date">{updateDate}</span>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Wrapper(SalesRate));
