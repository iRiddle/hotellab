import React, { Component } from "react";
import { UncontrolledTooltip } from "reactstrap";

class Panel extends Component {
  render() {
    return (
      <div className="panel">
        <div className="head" style={this.props.style}>
          {this.props.preTitle ? (
            <div className="head__constructor">
              <h2 className="h5 text-center" style = {{marginRight: '12px'}}>{`${this.props.preTitle}`}</h2>
              <h2 className="h5 text-center">{this.props.title}</h2>
            </div>
          ) : (
            <h2 className="h5 text-center">{this.props.title}</h2>
          )}
          <span className="tooltip_icon" id={"tooltip_" + this.props.id}></span>
          <UncontrolledTooltip
            placement="bottom"
            target={"tooltip_" + this.props.id}
          >
            {this.props.description}
          </UncontrolledTooltip>
        </div>
        <div className="body">{this.props.children}</div>
      </div>
    );
  }
}
export default Panel;
