import React from "react";
import { ButtonDropdown, DropdownToggle, DropdownMenu } from "reactstrap";
import { withTranslation } from "react-i18next";

class CheckboxDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleOption = this.toggleOption.bind(this);
    this.selectAll = this.selectAll.bind(this);

    let items = this.props.items || {};
    let checked = this.props.checked || [];
    let options = Object.keys(items);

    this.state = {
      dropdownOpen: false,
      options,
      items,
      checked,
      title: this.props.title || ""
    };
  }

  componentWillReceiveProps(nextProps) {
    let items = nextProps.items || {};
    let checked = nextProps.checked || [];
    let options = Object.keys(items);
    let title = nextProps.title;
    this.setState({
      items,
      checked,
      options,
      title
    });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  toggleOption(option) {
    let checked = [];
    if (this.state.checked.indexOf(option) !== -1)
      checked = this.state.checked.filter(oid => oid !== option);
    else checked = [...this.state.checked, option];

    this.setState({
      checked
    });
    this.props.onChange(checked);
  }

  selectAll() {
    let checked = [];
    if (this.state.checked.length !== this.state.options.length) {
      checked = Object.keys(this.state.items);
    }
    this.setState({
      checked
    });
    this.props.onChange(checked);
  }

  render() {
    const { t } = this.props;
    return (
      <ButtonDropdown
        className="checkbox-dropdown"
        isOpen={this.state.dropdownOpen}
        toggle={this.toggle}
      >
        <DropdownToggle caret outline color="primary">
          {this.state.title +
            " " +
            this.state.checked.length +
            ` ${t("from")} ` +
            this.state.options.length}
        </DropdownToggle>
        <DropdownMenu>
          <button
            type="button"
            role="menuitem"
            key="selectAll"
            className={
              "select-all dropdown-item" +
              (this.state.checked.length === this.state.options.length
                ? " active"
                : "")
            }
            onClick={() => this.selectAll()}
          >
            {t("selectAll")}
          </button>
          <div tabIndex="-1" className="dropdown-divider"></div>
          {this.state.options.map(o => (
            <button
              type="button"
              role="menuitem"
              className={
                "select-all dropdown-item" +
                (this.state.checked.indexOf(o) !== -1 ? " active" : "")
              }
              key={o}
              onClick={e => {
                this.toggleOption(o);
                e.preventDefault();
              }}
            >
              {this.state.items[o]}
            </button>
          ))}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

export default withTranslation()(CheckboxDropdown);
