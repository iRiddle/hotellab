import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import ReactEcharts from "echarts-for-react";

import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import { getBookingDepth } from "../redux/actions/getBookingDepth";

import {
  getCurrentHotel,
  getCurrentDate,
  getCurrencySymbolSelector
} from "../redux/selectors";
import {
  getBookingDepthChartSelector,
  getMainMetricsSelector,
  getFiltersSelector
} from "../redux/selectors/bookingDepth";

import Panel from "./Panel";
import CheckboxDropdown from "./CheckboxDropdown";
import DateRangePicker from "./DateRangePicker";
import Select from "./ChildComponents/Select";

import { formatMoney, copyToClipboard, formatFilterDate } from "../utils";

const isEmpty = require("lodash/isEmpty");

class SalesDepth extends Component {
  state = {
    data: [],
    sources: [],
    roomTypes: [],
    ndsArray: [
      { id: 0, title: "С НДС" },
      { id: 1, title: "Без НДС" }
    ],
    breakfastArray: [
      { id: 0, title: "С завтраком" },
      { id: 1, title: "Без завтрака" }
    ],
    currentNds: 0,
    currentBreakfast: 0
  };

  componentDidMount() {
    document.title = "hotellab - Глубина продаж";
    const { sources, roomTypes, currentNds, currentBreakfast } = this.state;
    this.props.getBookingDepthData(
      this.props.currentHotel,
      sources,
      roomTypes,
      currentNds,
      currentBreakfast,
      this.props.filterDateStock.dateStart,
      this.props.filterDateStock.dateEnd
    );
  }

  // TODO: add to static breakfast translation array

  static getDerivedStateFromProps(nextProps, prevState) {
    if (localStorage.getItem("i18nextLng") !== nextProps.currentLanguage) {
      return {
        ndsArray: [
          { id: 0, title: nextProps.t("vatIncluded") },
          { id: 1, title: nextProps.t("vatWithout") }
        ]
      };
    }
    return;
  }

  componentDidUpdate(prevProps, prevState) {
    const { sources, roomTypes, currentNds, currentBreakfast } = this.state;

    if (prevProps.currentHotel !== this.props.currentHotel) {
      this.props.getBookingDepthData(
        this.props.currentHotel,
        sources,
        roomTypes,
        currentNds,
        currentBreakfast,
        this.props.filterDateStock.dateStart,
        this.props.filterDateStock.dateEnd
      );
      this.setState({
        sources: [],
        roomTypes: []
      });
    }

    if (
      prevState.sources !== sources ||
      prevState.roomTypes !== roomTypes ||
      prevState.currentNds !== currentNds ||
      prevState.currentBreakfast !== currentBreakfast ||
      prevProps.filterDateStock.dateStart !==
        this.props.filterDateStock.dateStart ||
      prevProps.filterDateStock.dateEnd !== this.props.filterDateStock.dateEnd
    ) {
      this.props.getBookingDepthData(
        this.props.currentHotel,
        sources,
        roomTypes,
        currentNds,
        currentBreakfast,
        this.props.filterDateStock.dateStart,
        this.props.filterDateStock.dateEnd
      );
    }
  }

  toggle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  setFilterOption = (filter, data) => {
    if (filter === "sources") {
      this.setState({
        sources: data
      });
    } else {
      this.setState({
        roomTypes: data
      });
    }
  };

  copyCSV = () => {
    let text = "";
    for (var i in this.state.data) {
      var row = "";
      for (var j in this.state.data[i]) {
        row += `"${this.state.data[i][j]}",`;
      }
      text += row + "\n";
    }
    copyToClipboard(text);
  };
  copyEXCEL = () => {
    let text = "";
    for (var i in this.state.data) {
      var row = "";
      for (var j in this.state.data[i]) {
        row += this.state.data[i][j] + "\t";
      }
      text += row + "\n";
    }
    copyToClipboard(text);
  };

  getTitle = key => {
    const { t } = this.props;
    switch (key) {
      case "residence":
        return t("averageLength");
      case "cancel":
        return t("cancellations");
      case "commition":
        return t("commissions");
      case "bookingdepth":
        return t("averageBooking");
      default:
        return "";
    }
  };

  selectNds = id => {
    id === "С НДС" || id === "VAT included"
      ? this.setState({
          currentNds: 0
        })
      : this.setState({
          currentNds: 1
        });
  };

  selectBreakfast = id => {
    id === "С завтраком" || id === "With breakfast"
      ? this.setState({
          currentBreakfast: 0
        })
      : this.setState({
          currentBreakfast: 1
        });
  };

  getSalesDepthOptions = () => {
    const { bookingDepthChart } = this.props;
    const option = {
      tooltip: {
        trigger: "axis",
        formatter: itemYaxis => {
          return `Глубина бронирования - ${itemYaxis[0].value} %`;
        },
        axisPointer: {
          type: "shadow"
        }
      },
      xAxis: {
        data: Object.keys(bookingDepthChart)
      },
      yAxis: [
        {
          axisLabel: {
            formatter: value => {
              return `${value} %`;
            }
          },
          min: 0,
          max: 100,
          position: "left"
        },
        {
          axisLabel: {
            formatter: value => {
              return `${value} %`;
            }
          },
          min: 0,
          max: 100,
          position: "right"
        }
      ],
      series: [
        {
          name: "Глубина бронирования",
          type: "bar",
          itemStyle: {
            normal: {
              barBorderColor: "#54bfad",
              color: "#54bfad"
            }
          },
          barCategoryGap: "1.5%",
          data: Object.values(bookingDepthChart)
        }
      ]
    };
    return option;
  };

  render() {
    const {
      bookingDepthChart,
      mainMetrics,
      filters,
      t,
      currencySymbol
    } = this.props;
    const {
      sources,
      roomTypes,
      ndsArray,
      currentNds,
      breakfastArray,
      currentBreakfast
    } = this.state;
    return (
      <Container fluid className="sales-depth">
        <>
          <Row className="filter panel">
            <Col xs="4" sm="4" lg="4" xl="4" className="col">
              {!isEmpty(filters) && (
                <div>
                  <label>
                    {t("datesOfResidence")}
                    <DateRangePicker
                      typeDate="SET_SALES_DEPTH_DATE_RS"
                      getPage="SalesDepth"
                      dateStart={formatFilterDate(filters.start_date)}
                      dateEnd={formatFilterDate(filters.end_date)}
                    />
                  </label>
                </div>
              )}
            </Col>

            <Col xs="8" sm="8" lg="8" xl="8">
              <div className="sales-depth__filters">
                <CheckboxDropdown
                  onChange={v => this.setFilterOption("sources", v)}
                  title={t("source")}
                  items={filters.sources}
                  checked={sources}
                />
                <CheckboxDropdown
                  onChange={v => this.setFilterOption("code", v)}
                  title={t("roomTypes")}
                  items={filters.room_types}
                  checked={roomTypes}
                />
                <div className="custom-ml-dropdown">
                  <Select
                    items={ndsArray}
                    selectedItem={currentNds}
                    onClick={this.selectNds}
                  ></Select>
                </div>
                <div className="custom-ml-dropdown">
                  <Select
                    items={breakfastArray}
                    selectedItem={currentBreakfast}
                    onClick={this.selectBreakfast}
                  ></Select>
                </div>
              </div>
            </Col>
          </Row>

          <Row className="title" style={{ margin: "16px 0 8px 0" }}>
            <Col>
              <h3>{t("depthOfReservation")}</h3>
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="12" lg="12" xl="8">
              <Panel
                id="2-3"
                title={t("depthOfReservation")}
                description="Текст подсказки 2.3"
              >
                {!isEmpty(bookingDepthChart) && (
                  <ReactEcharts option={this.getSalesDepthOptions()} />
                )}
              </Panel>
            </Col>
            <Col xs="12" sm="12" lg="12" xl="4">
              <Panel
                id="1-3"
                title={t("keyMetrics")}
                description="Текст подсказки 1.3"
              >
                <Row className="key-metrics">
                  {Object.keys(mainMetrics).map((metric, i) => (
                    <Col
                      sm="6"
                      xs="12"
                      key={i}
                      className={
                        "metric " + (mainMetrics[metric].good ? "good" : "bad")
                      }
                    >
                      <h6 className="title">{this.getTitle(metric)}</h6>
                      <h2 className="val">
                        {metric === "commition"
                          ? `${formatMoney(
                              mainMetrics[metric].val
                            )} ${currencySymbol}`
                          : mainMetrics[metric].val}
                      </h2>
                      <span className="diff">
                        {mainMetrics[metric].diff > 0 ? "↗ +" : "↘ -"}
                        {`${mainMetrics[metric].diff} % ${t("lastYear")}`}
                      </span>
                    </Col>
                  ))}
                </Row>
              </Panel>
            </Col>
          </Row>
        </>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  currentHotel: getCurrentHotel(state),
  bookingDepthChart: getBookingDepthChartSelector(state),
  mainMetrics: getMainMetricsSelector(state),
  filters: getFiltersSelector(state),
  filterDateStock: getCurrentDate(state),
  currencySymbol: getCurrencySymbolSelector(state)
});

const mapDispatchToProps = dispatch => ({
  getBookingDepthData: (
    hotelId,
    sources,
    roomTypes,
    nds,
    breakfast,
    startData,
    endDate
  ) => {
    dispatch(
      getBookingDepth(
        hotelId,
        sources,
        roomTypes,
        nds,
        breakfast,
        startData,
        endDate
      )
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(SalesDepth));
