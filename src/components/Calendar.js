import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Table,
  Container,
  Row,
  Col,
  Button,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  ButtonGroup,
  TabContent,
  TabPane,
  Progress,
  UncontrolledTooltip,
  CustomInput
} from "reactstrap";
import classnames from "classnames";
import ReactEcharts from "echarts-for-react";
import { withTranslation } from "react-i18next";
import CheckboxDropdown from "./CheckboxDropdown";
import {
  getCalendarFilters,
  getCalendarData,
  getCurrencySymbolSelector
} from "../redux/selectors";

import { getMarketAllStars, getStarsCount } from "../redux/selectors/market";

import {
  fetchInitialState,
  setCalendarFilterSelectedOptions,
  fetchCalendarData,
  fetchSelectedDateDetails,
  setCalendarSelectedDate
} from "../redux/actions";
import { MONTHLABELS } from "../constants";

import { formatMoney } from "../utils";
import CustomSelect from "./CustomSelect";
import MLCBDD from "./MLCBDD";
import InDev from "./InDev";

class Calendar extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.filters.recommendations !== prevState.recommendations) {
      return {
        recommendations: nextProps.filters.recommendations
      };
    }
    if (localStorage.getItem("i18nextLng") !== nextProps.currentLanguage) {
      return {
        // todo translate stars
      };
    }
  }

  constructor(props) {
    super(props);
    let d = new Date();
    d.setDate(1);
    let tableConfig = this.getTableConfig(d.getTime());
    this.state = {
      minDate: new Date(),
      d: d,
      sidebarCollapse: false,
      sidebarCollapsed: false,
      activeTab: "competitors",
      activeHotelGraph: "salesDepth",
      tableConfig: tableConfig,
      gap: 0,
      showOn: "%",
      recommendations: this.props.filters.recommendations,
      activeStars: ["star_none", "star_2", "star_3", "star_4", "star_5"],
      starsData: { abscissaArray: [], filteredArray: [] }
    };
  }

  componentDidMount() {
    document.title = "Календарь спроса";
    this.props.setFilterSelectedOptions("recommendations");

    this.collapseStarsByFilter(
      this.props.marketAllStars,
      this.state.activeStars
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.activeStars !== this.state.activeStars ||
      prevProps.marketAllStars !== this.props.marketAllStars
    ) {
      this.collapseStarsByFilter(
        this.props.marketAllStars,
        this.state.activeStars
      );
    }
  }

  toggleSidebar = () => {
    this.setState({ sidebarCollapse: !this.state.sidebarCollapse });
  };
  onSidebarExited = () => {
    this.setState({ sidebarCollapsed: false });
  };
  onSidebarEntering = () => {
    this.setState({ sidebarCollapsed: true });
  };
  toggleTab = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        sidebarCollapse: true
      });
    } else {
      this.toggleSidebar();
    }
  };
  toggleHotelGraph = graph => {
    if (this.state.activeHotelGraph !== graph) {
      this.setState({
        activeHotelGraph: graph
      });
    }
  };
  nextMonth = () => {
    let newDate = new Date(
      this.state.d.getFullYear(),
      this.state.d.getMonth() + 1
    );
    let tableConfig = this.getTableConfig(newDate.getTime());
    this.setState({
      d: newDate,
      tableConfig: tableConfig
    });
  };
  prevMonth = () => {
    let newDate = new Date(
      this.state.d.getFullYear(),
      this.state.d.getMonth() - 1
    );
    if (this.isDateLesThanMD(newDate)) return;

    let tableConfig = this.getTableConfig(newDate.getTime());
    this.setState({
      d: newDate,
      tableConfig: tableConfig
    });
  };

  isDateLesThanMD(date) {
    return (
      date.getFullYear() < this.state.minDate.getFullYear() ||
      (date.getFullYear() === this.state.minDate.getFullYear() &&
        date.getMonth() < this.state.minDate.getMonth())
    );
  }

  getTableConfig(time) {
    let c = {};
    let d = new Date(time);
    c.from = d.getDay();

    d.setDate(d.getDate() - 1); // последнее число предидущего месяца
    c.pmc = d.getDate();
    d.setDate(d.getDate() + 1);
    d.setMonth(d.getMonth() + 1); // 1ое число следующего месяца
    d.setDate(d.getDate() - 1); // последнее число нужного месяца
    c.count = d.getDate();
    c.rows = (c.from + c.count - 1) / 7;
    if (c.from == 0) c.rows += 1;

    return c;
  }

  getTableDataByDate(y, _m, d) {
    let data = {};
    let m = _m + 1;

    if (
      !this.props.tableData.hasOwnProperty(y) ||
      !this.props.tableData[y].hasOwnProperty(m) ||
      !this.props.tableData[y][m].hasOwnProperty(d)
    )
      return data;
    data = this.props.tableData[y][m][d];
    return data;
  }

  onClickOnDate(y, m, d, currentMonth) {
    if (!currentMonth) return;
    if (
      !this.props.tableData.hasOwnProperty(y) ||
      !this.props.tableData[y].hasOwnProperty(m + 1) ||
      !this.props.tableData[y][m + 1].hasOwnProperty(d)
    )
      return;
    var data = this.getTableDataByDate(y, m, d);
    this.props.setCalendarSelectedDate(y, m, d);
    this.props.fetchSelectedDateDetails(); // TODO need refactoring
    var now = Date.now();
    var date = new Date(y, m, d);
    var msPerDay = 24 * 60 * 60 * 1000;
    var gap = Math.round((date - now) / msPerDay);

    if (data) {
      this.setState({
        // selectedDate: {y,m,d},
        gap
      });
    }
  }

  swShowOn = showOn => {
    this.setState({
      showOn
    });
  };

  getStarLabel = star => {
    const { t } = this.props;
    switch (star) {
      case "star_none":
        return t("noStars");
      case "star_2":
        return t("2stars");
      case "star_3":
        return t("3stars");
      case "star_4":
        return t("4stars");
      case "star_5":
        return t("5stars");
    }
  };

  handleStarChange = star => {
    const { activeStars } = this.state;
    if (activeStars.some(astar => astar === star)) {
      this.setState({
        activeStars: activeStars.filter(astar => astar !== star)
      });
    } else {
      this.setState({
        activeStars: [...activeStars, star]
      });
    }
  };

  collapseStarsByFilter = (marketAllStars, activeStars) => {
    const abscissaArray = marketAllStars.map(item => item[0]);
    const ordinateArray = marketAllStars.map(item => item[1]);

    const filteredArray = ordinateArray.map(star => {
      let totalStars = 0;

      if (star === 0) {
        return totalStars;
      }

      if (activeStars.some(item => item === "star_none")) {
        totalStars += star.star_none;
      }
      if (activeStars.some(item => item === "star_2")) {
        totalStars += star.star_2;
      }
      if (activeStars.some(item => item === "star_3")) {
        totalStars += star.star_3;
      }
      if (activeStars.some(item => item === "star_4")) {
        totalStars += star.star_4;
      }
      if (activeStars.some(item => item === "star_5")) {
        totalStars += star.star_5;
      }
      return totalStars;
    });

    this.setState({
      starsData: { abscissaArray, filteredArray }
    });
  };

  getGraphMarketOption = () => ({
    xAxis: {
      type: "category",
      name: this.props.t("daysBeforeArrival"),
      nameLocation: "center",
      nameGap: 30,
      data: this.state.starsData.abscissaArray,
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      splitArea: {
        show: false
      }
    },
    yAxis: {
      type: "value",
      // name: graphConf1.yTitle,
      nameLocation: "center",
      nameGap: 30,
      axisTick: {
        show: false
      }
    },
    grid: {
      left: 60
    },
    tooltip: {
      trigger: "axis",
      formatter: function(a) {
        let o = a[0];
        return o.value;
      }
    },
    series: [
      {
        lineStyle: {
          color: "#51beaa"
        },
        data: this.state.starsData.filteredArray,
        type: "line"
      }
    ]
  });

  render() {
    const { t, currencySymbol, starsCount } = this.props;
    const { recommendations, activeStars } = this.state;
    var updateDate = this.props.updateDate; // TODO

    const HOTELGRAPHS = {
      salesDepth: "salesDepth",
      averageFare: "averageFare",
      profit: "profit"
    };
    var hotelGrapConf = {};
    hotelGrapConf[HOTELGRAPHS.salesDepth] = {
      yTitle: t("soldNumbers")
    };
    hotelGrapConf[HOTELGRAPHS.averageFare] = {
      yTitle: t("averageRate")
    };
    hotelGrapConf[HOTELGRAPHS.profit] = {
      yTitle: t("income")
    };

    let c = this.state.tableConfig;
    let filters = this.props.filters;

    // TODO need refactoring
    let setFilterSelectedOptions = (k, v) => {
      this.props.setFilterSelectedOptions(
        k,
        v,
        this.props.selectedDate.y,
        this.props.selectedDate.m,
        this.props.selectedDate.d
      );
    };

    let tableRows = [];

    let cd = -c.from + 1;
    if (cd == 1) cd = -6;
    let cy = this.state.d.getFullYear();
    let cm = this.state.d.getMonth();
    for (let i = 0; i < c.rows; i++) {
      var dates = [];
      for (let j = 0; j < 7; j++) {
        cd++;
        var date = "";
        var Rev = "";
        var BAR = "";
        var Act = "";
        var Load = "";
        var LoadPY = "";
        var Par = "";
        var cn = "";
        var sign = "";
        var currentMonth = false;
        if (cd > 0 && cd <= c.count) {
          date = cd;
          var data = this.getTableDataByDate(cy, cm, date);
          currentMonth = true;
          cn = "current-month";
          if (Object.keys(data).length >= 4) {
            Rev = data.Rev;
            BAR = data.BAR;
            Act = data.Act;
            Load = data.Load || "";
            LoadPY = data.LoadPY || "-";
            Par = data.Par;
            if (data.hasOwnProperty("prec")) {
              if (data.prec === "eq") {
                cn += " eq";
                sign = "eq";
              } else if (data.prec === "up") {
                cn += " green";
                sign = "up";
              } else {
                cn += " red";
                sign = "down";
              }
            } else {
              if (Rev === BAR) {
                cn += " eq";
                sign = "eq";
              } else if (Rev > BAR) {
                cn += " green";
                sign = "up";
              } else {
                cn += " red";
                sign = "down";
              }
            }
          }
          if (
            this.props.selectedDate.y === cy &&
            this.props.selectedDate.m === cm &&
            this.props.selectedDate.d === date
          )
            cn += " selected";
        } else if (cd <= 0) {
          date = c.pmc + cd;
          cn = "another-month";
        } else {
          date = cd - c.count;
          cn = "another-month";
        }

        dates.push(
          <td
            className={cn}
            key={j}
            onClick={this.onClickOnDate.bind(this, cy, cm, date, currentMonth)}
          >
            <Row className="date">
              <Col>{date}</Col>
            </Row>
            {currentMonth ? (
              <Row className="bar-rev">
                {Rev !== "" ? (
                  <Col className="rev">
                    <label>Rev</label>
                    <span className="value">{Rev}</span>
                    <span className={"sign " + sign}>
                      {sign === "eq" ? "" : sign === "up" ? "↗" : "↘"}
                    </span>
                  </Col>
                ) : (
                  ""
                )}
                {BAR !== "" ? (
                  <Col className="bar">
                    <label>BAR</label>
                    <span className="value">{BAR}</span>
                  </Col>
                ) : (
                  ""
                )}
              </Row>
            ) : (
              ""
            )}
            {currentMonth ? (
              <Row className="act-par">
                {Act !== "" ? (
                  <Col className="act">
                    <label>{t("constants.f")}</label>
                    <span className="value">
                      {this.state.showOn === "F" ? Act : Load}
                    </span>
                    <label>{this.state.showOn === "F" ? "" : "%"}</label>
                  </Col>
                ) : (
                  ""
                )}
                {Par !== "" ? (
                  <Col className="par">
                    <label>{t("constants.ly")}</label>
                    {/* <span className="value">{Par}</span> */}
                    <span className="value">
                      {this.state.showOn === "F" ? Par : LoadPY}
                    </span>
                    <label>{this.state.showOn === "F" ? "" : "%"}</label>
                  </Col>
                ) : (
                  ""
                )}
              </Row>
            ) : (
              ""
            )}
          </td>
        );
      }
      tableRows.push(<tr key={i}>{dates}</tr>);
    }

    let mn = this.state.d.getMonth();
    let _dev = true;
    let filterDD2 = filters["roomTypes"] || {};
    let filterDD1 = filters["recommendations"] || {};
    let _CD = this.getTableDataByDate(
      this.props.selectedDate.y,
      this.props.selectedDate.m,
      this.props.selectedDate.d
    );
    let currentPrice = parseFloat(_CD.BAR) || 0;

    let competitorsTariffs = [];
    let details = this.props.details; // TODO

    if (details && details.competitors && details.competitors.tariffs)
      competitorsTariffs = details.competitors.tariffs
        .slice(0)
        .sort((v1, v2) => v2.price - v1.price);

    let graphData = [];
    let graphData1 = [];
    let graphConf1 = {};
    let roomsForSale = [];

    if (details) {
      if (details.competitors) {
        graphData = details.competitors.medianPrice;
      }
      if (details.hotel) {
        roomsForSale = details.hotel.roomsForSale || [];
        switch (this.state.activeHotelGraph) {
          case HOTELGRAPHS.salesDepth:
            graphData1 = details.hotel.salesDepth;
            break;
          case HOTELGRAPHS.averageFare:
            graphData1 = details.hotel.averageFare;
            break;
          case HOTELGRAPHS.profit:
            graphData1 = details.hotel.profit;
            break;
          default:
            break;
        }
        graphConf1 = hotelGrapConf[this.state.activeHotelGraph] || {};
      }
    }

    var graphDataLabels1 = graphData1.map(d => d[0]);
    var graphDataLabels = graphData.map(d => d[0]);
    var gap = this.state.gap || 0;
    var graphData1data = graphData1;
    graphData1data = graphData1.filter((d, n) => n > gap);
    graphData = graphData.filter((d, n) => n > gap);
    let graphConfig = {};
    graphConfig["default"] = {
      xAxis: {
        type: "category",
        name: t("daysBeforeArrival"),
        nameLocation: "center",
        nameGap: 30,
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        splitArea: {
          show: false
        },
        axisLabel: {
          interval: function(num, name) {
            return num % 10 === 0 || num === 0 || num === graphData1.length - 1;
          }
        },

        data: graphDataLabels1
      },
      yAxis: {
        type: "value",
        name: graphConf1.yTitle,
        nameLocation: "center",
        nameGap: 30,

        axisTick: {
          show: false
        },
        axisLabel: {
          formatter: function(value, index) {
            if (value >= 1000) return Math.floor(value / 1000) + "к";
            else return value;
          }
        }
      },
      grid: {
        left: 60
      },
      tooltip: {
        trigger: "axis",
        formatter: function(a) {
          let o = a[0];
          return o.axisValue + ": " + o.value[1];
        }
      },
      series: [
        {
          lineStyle: {
            color: "#51beaa"
          },
          data: graphData1data,
          type: "line"
        }
      ]
    };

    graphConfig["competitors"] = {
      xAxis: {
        type: "category",
        name: t("daysBeforeArrival"),
        nameLocation: "center",
        nameGap: 30,
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        axisLabel: {
          interval: function(num, name) {
            return num % 10 === 0 || num === 0 || num === graphData.length - 1;
          }
        },
        data: graphDataLabels
      },
      yAxis: {
        type: "value",
        name: t("medianMarketPrice"),
        nameLocation: "center",
        nameGap: 30,
        axisTick: {
          show: false
        },
        axisLabel: {
          formatter: value => {
            if (value >= 1000) return (value / 1000).toFixed(1) + "к";
            else return value;
          }
        }
      },
      grid: {
        left: 60
      },
      tooltip: {
        trigger: "axis",
        formatter: function(a) {
          let o = a[0];
          return o.axisValue + ": " + o.value[1];
        }
      },
      series: [
        {
          lineStyle: {
            color: "#51beaa"
          },
          data: graphData, // !!! TODO
          type: "line"
        }
      ]
    };

    return (
      <Container fluid className="table-cont calendar">
        <Row className="head">
          <Col sm={_dev ? "8" : "12"} className="calendar-filters">
            <div className="panel">
              <Row className="text-center">
                <Col>
                  <ButtonGroup>
                    <MLCBDD
                      onChange={v =>
                        setFilterSelectedOptions("recommendations", v)
                      }
                      title={t("selectionByRecommendation")}
                      items={recommendations.list}
                      checked={recommendations.selected}
                    />
                    <CheckboxDropdown
                      onChange={v => setFilterSelectedOptions("roomTypes", v)}
                      title={t("numberTypeFilter")}
                      checked={filterDD2.selected}
                      items={filterDD2.list}
                    />
                    <CustomSelect
                      onChange={v => this.swShowOn(v)}
                      items={{
                        "%": t("showByDownload"),
                        F: t("showByNumbers")
                      }}
                    />
                  </ButtonGroup>
                </Col>
              </Row>
            </div>
          </Col>
          {_dev ? (
            <Col sm="4" className="calendar-details">
              <div
                className={
                  (this.state.sidebarCollapsed ? "collapsed " : "") +
                  "graph-panel panel"
                }
              >
                <span className="tooltip_icon" id="tooltip_graph-panel" />
                <UncontrolledTooltip
                  placement="bottom"
                  target="tooltip_graph-panel"
                  dangerouslySetInnerHTML={{ __html: t("tips.calendarHeader") }}
                ></UncontrolledTooltip>
                <Button
                  className="toggle"
                  color="primary"
                  onClick={this.toggleSidebar}
                />
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "hotel"
                      })}
                      onClick={() => {
                        this.toggleTab("hotel");
                      }}
                    >
                      {t("hotel")}
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "competitors"
                      })}
                      onClick={() => {
                        this.toggleTab("competitors");
                      }}
                    >
                      {t("competitors")}
                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink
                      className={classnames({
                        active: this.state.activeTab === "market"
                      })}
                      onClick={() => {
                        this.toggleTab("market");
                      }}
                    >
                      {t("market")}
                    </NavLink>
                  </NavItem>
                  <NavItem></NavItem>
                </Nav>

                <Collapse
                  onEntering={this.onSidebarEntering}
                  onExited={this.onSidebarExited}
                  isOpen={this.state.sidebarCollapse}
                >
                  <div className="tab-content">
                    {this.props.detailsLoading ? (
                      <div className="overlay">
                        <span>Loading...</span>
                      </div>
                    ) : (
                      ""
                    )}
                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="competitors" className="competitors">
                        <Row className="graph">
                          <Col sm="12">
                            <ReactEcharts
                              option={graphConfig["competitors"]}
                              style={{ height: "400px" }}
                            />
                          </Col>
                        </Row>
                        <Row className="table">
                          <Col>
                            <h4 className="main-title">
                              {t("tariffsGroupCompetitors")}
                            </h4>
                            <Table borderless>
                              <thead>
                                <tr>
                                  <th>{t("title")}</th>
                                  <th>{t("price")}</th>
                                  <th>{t("difference")}</th>
                                </tr>
                              </thead>
                              <tbody>
                                {competitorsTariffs.map((r, n) => (
                                  <tr key={n}>
                                    <td>{r.name}</td>
                                    <td>
                                      {r.closed ? "-" : formatMoney(r.price)}{" "}
                                      {currencySymbol}
                                    </td>
                                    <td>
                                      {r.closed || !currentPrice
                                        ? "-"
                                        : formatMoney(
                                            parseFloat(r.price) - currentPrice
                                          )}{" "}
                                      {currencySymbol}
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tabId="hotel" className="hotel">
                        <Row className="graphs">
                          <Col>
                            <Nav tabs>
                              <NavItem>
                                <NavLink
                                  className={classnames({
                                    active:
                                      this.state.activeHotelGraph ===
                                      HOTELGRAPHS.salesDepth
                                  })}
                                  onClick={() => {
                                    this.toggleHotelGraph(
                                      HOTELGRAPHS.salesDepth
                                    );
                                  }}
                                >
                                  {t("loading")}
                                </NavLink>
                              </NavItem>
                              <NavItem>
                                <NavLink
                                  className={classnames({
                                    active:
                                      this.state.activeHotelGraph ===
                                      HOTELGRAPHS.averageFare
                                  })}
                                  onClick={() => {
                                    this.toggleHotelGraph(
                                      HOTELGRAPHS.averageFare
                                    );
                                  }}
                                >
                                  {t("averageRate")}
                                </NavLink>
                              </NavItem>
                              <NavItem>
                                <NavLink
                                  className={classnames({
                                    active:
                                      this.state.activeHotelGraph === "profit"
                                  })}
                                  onClick={() => {
                                    this.toggleHotelGraph("profit");
                                  }}
                                >
                                  {t("income")}
                                </NavLink>
                              </NavItem>
                            </Nav>
                            <Row className="graph">
                              <Col>
                                <ReactEcharts
                                  option={graphConfig["default"]}
                                  style={{ height: "400px" }}
                                />
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                        <Row className="rooms_for_sale">
                          <Col>
                            <h4>{t("numbersForSale")}</h4>
                            <div className="bars">
                              {roomsForSale.map((v, n) => (
                                <div key={n} className="bar">
                                  <label>{v.title}</label>
                                  <span className="amount">{v.max - v.v}</span>
                                  <Progress value={(v.v / v.max) * 100} />
                                </div>
                              ))}
                            </div>
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tabId="market" className="market">
                        <>
                          <Row className="graph">
                            <Col>
                              <ReactEcharts
                                option={this.getGraphMarketOption()}
                                style={{ height: "400px" }}
                              />
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <div className="market-stars">
                                {Object.keys(starsCount).map(star => (
                                  <div
                                    className="market-stars__body"
                                    key={star}
                                  >
                                    <CustomInput
                                      type="checkbox"
                                      id={star}
                                      label={this.getStarLabel(star)}
                                      className="market-star"
                                      checked={activeStars.some(
                                        astar => astar === star
                                      )}
                                      onChange={() =>
                                        this.handleStarChange(star)
                                      }
                                    />
                                    <span>{starsCount[star]}</span>
                                  </div>
                                ))}
                              </div>
                            </Col>
                          </Row>
                        </>
                      </TabPane>
                    </TabContent>
                  </div>
                </Collapse>
              </div>
            </Col>
          ) : (
            ""
          )}
        </Row>
        <Row className="body">
          <Col
            sm={this.state.sidebarCollapsed ? "8" : "12"}
            className="calendar-calendar panel"
          >
            <span className="tooltip_icon" id="tooltip_calendar-panel" />
            <UncontrolledTooltip
              placement="bottom"
              target="tooltip_calendar-panel"
            >
              <p>{t("tips.calendarP1")}</p>
              <p>{t("tips.calendarP2")}</p>
              <p>{t("tips.calendarP3")}</p>
              <p>{t("tips.calendarP4")}</p>
            </UncontrolledTooltip>
            <Row className="controls">
              <Col className="text-center">
                {this.isDateLesThanMD(
                  new Date(
                    this.state.d.getFullYear(),
                    this.state.d.getMonth() - 1
                  )
                ) ? (
                  ""
                ) : (
                  <button onClick={this.prevMonth} className="prev">
                    &lt;-
                  </button>
                )}
                <span className="year">{this.state.d.getFullYear()}</span>
                <span
                  className="month"
                  style={{ minWidth: "100px", display: "inline-block" }}
                >
                  {t(MONTHLABELS[mn])}
                </span>
                <button onClick={this.nextMonth} className="next">
                  -&gt;
                </button>
              </Col>
            </Row>
            <Row
              className={
                "table-wrap " +
                (this.state.sidebarCollapsed ? "simple" : "full")
              }
            >
              <Col>
                <Table bordered>
                  <thead>
                    <tr>
                      <th>{t("daysCalendar.mon")}</th>
                      <th>{t("daysCalendar.tue")}</th>
                      <th>{t("daysCalendar.wed")}</th>
                      <th>{t("daysCalendar.th")}</th>
                      <th>{t("daysCalendar.fri")}</th>
                      <th>{t("daysCalendar.sat")}</th>
                      <th>{t("daysCalendar.sun")}</th>
                    </tr>
                  </thead>
                  <tbody>{tableRows}</tbody>
                </Table>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="footer">
          <Col>
            <p className="updated">
              <span className="text">{t("lastUpdate")}:</span>
              <span className="date">{updateDate}</span>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  const tableData = getCalendarData(state);
  const filters = getCalendarFilters(state);
  const updateDate = state.calendar.updated;
  const loading = state.calendar.loading;
  const details = state.calendar.selectedDateDetails;
  const selectedDate = state.calendar.selectedDate;
  const detailsLoading = state.calendar.selectedDateDetails.loading;
  const currencySymbol = getCurrencySymbolSelector(state);
  const marketAllStars = getMarketAllStars(state);
  const starsCount = getStarsCount(state);
  return {
    tableData,
    filters,
    updateDate,
    loading,
    details,
    selectedDate,
    detailsLoading,
    currencySymbol,
    marketAllStars,
    starsCount
  };
};

const mapDispatchToProps = dispatch => ({
  fetchInitialState: () => {
    dispatch(fetchInitialState());
  },
  setFilterSelectedOptions: (k, v) => {
    dispatch(setCalendarFilterSelectedOptions(k, v));
    dispatch(fetchCalendarData());
    dispatch(fetchSelectedDateDetails());
  },
  fetchSelectedDateDetails: () => {
    dispatch(fetchSelectedDateDetails());
  },
  setCalendarSelectedDate: (y, m, d) => {
    dispatch(setCalendarSelectedDate(y, m, d));
  }
});

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(Calendar)
);
