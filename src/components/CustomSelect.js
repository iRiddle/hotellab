import React from "react";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

export default class CustomSelect extends React.Component {
  constructor(props) {
    super(props);
    // this.emitChange = this.emitChange.bind(this);
    let options = Object.keys(this.props.items || []);
    let currentOption = this.props.currentOption || options[0];

    this.state = {
      dropdownOpen: false,
      currentOption,
      options,
      items: this.props.items || {}
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.items !== prevState.items) {
      return {
        items: nextProps.items
      };
    }
    return;
  }

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  };
  setCurrentOption(currentOption) {
    this.setState({
      currentOption: currentOption
    });
    if (this.props.onChange) this.props.onChange(currentOption);
  }

  render() {
    const { options, items, currentOption, dropdownOpen } = this.state;
    return (
      <ButtonDropdown isOpen={dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret outline color="primary">
          {items[currentOption]}
        </DropdownToggle>
        <DropdownMenu>
          {options.map(o => (
            <DropdownItem
              active={currentOption === o}
              key={o}
              onClick={() => this.setCurrentOption(o)}
            >
              {items[o]}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}
