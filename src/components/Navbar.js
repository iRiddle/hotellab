import React, { Component } from "react";
import { NavLink as RRNavLink } from "react-router-dom";
import { matchPath, withRouter } from "react-router";

import ReactFlagsSelect from "react-flags-select";
import "react-flags-select/css/react-flags-select.css";

import { connect } from "react-redux";

import { withTranslation } from "react-i18next";

import {
  selectHotel,
  fetchCalendarData,
  fetchSelectedDateDetails,
  fetchInitialStateSalesRate,
  fetchInitialStateArtboard
} from "../redux/actions";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import { getHotels, getHotelsIds } from "../redux/selectors";

class NB extends Component {
  state = {
    isOpen: false,
    routes: {
      "/calendar": "Календарь",
      "/sales_rate": "Темпы продаж"
      // "/sales_depth": "Глубина продаж"
    },
    languagesItems: [
      { id: "ru", title: "RU", img: "russia" },
      { id: "en", title: "EN", img: "america" }
    ],
    currentLanguage: localStorage.getItem("i18nextLng")
  };

  static getDerivedStateFromProps(props, state) {
    if (localStorage.getItem("i18nextLng") === state.currentLanguage) {
      return {
        routes: {
          "/calendar": props.t("header.calendar"),
          "/sales_rate": props.t("header.salesPace")
        }
      };
    }
    return;
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  selectLanguage = id => {
    const { i18n } = this.props;
    if (id === "RU") {
      this.setState(
        {
          currentLanguage: "ru"
        },
        () => i18n.changeLanguage("ru")
      );
    } else {
      this.setState(
        {
          currentLanguage: "en"
        },
        () => i18n.changeLanguage("en")
      );
    }
  };

  render() {
    let {
      selectHotel,
      hotels,
      hotelsIds,
      currentHotel,
      userImg,
      userName,
      t
    } = this.props;
    const { routes, currentLanguage } = this.state;
    let paths = Object.keys(routes);
    return (
      <div>
        <Navbar color="light" light expand="md">
          <RRNavLink
            exact={true}
            activeClassName="active"
            className="navbar-brand"
            to="/"
          ></RRNavLink>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav navbar>
              <UncontrolledDropdown nav inNavbar setActiveFromChild>
                <DropdownToggle nav caret className="income-control">
                  {t("header.revenueManagement")}
                </DropdownToggle>
                <DropdownMenu right>
                  {paths.map((p, n) => (
                    <DropdownItem
                      key={n}
                      active={
                        (
                          matchPath(this.props.location.pathname, {
                            path: p,
                            exact: true,
                            strict: false
                          }) || {}
                        ).isExact
                      }
                    >
                      <RRNavLink
                        exact={true}
                        activeClassName="active"
                        className="nav-link"
                        to={p}
                      >
                        {routes[p]}
                      </RRNavLink>
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </UncontrolledDropdown>

              <NavItem>
                <RRNavLink
                  exact={true}
                  activeClassName="active"
                  className="nav-link nav-link-marketing"
                  to="/digital_marketing"
                >
                  {t("header.digitalMarketing")}
                </RRNavLink>
              </NavItem>
              <NavItem>
                <RRNavLink
                  exact={true}
                  activeClassName="active"
                  className="nav-link nav-link-analytics"
                  to="/dashboard"
                >
                  {t("header.analytics")}
                </RRNavLink>
              </NavItem>
              <NavItem>
                <RRNavLink
                  exact={true}
                  activeClassName="active"
                  className="nav-link nav-link-settings"
                  to="/settings"
                >
                  {t("header.settings")}
                </RRNavLink>
              </NavItem>
            </Nav>
            <Nav className="ml-auto" navbar>
              {hotelsIds.length > 1 ? (
                <UncontrolledDropdown nav inNavbar className="hotel-selector">
                  <DropdownToggle nav caret>
                    {hotels[currentHotel]}
                  </DropdownToggle>
                  <DropdownMenu right>
                    {hotelsIds.map((hid, n) => (
                      <DropdownItem
                        key={n}
                        active={currentHotel === hid}
                        onClick={() => selectHotel(hid)}
                      >
                        {hotels[hid]}
                      </DropdownItem>
                    ))}
                  </DropdownMenu>
                </UncontrolledDropdown>
              ) : (
                ""
              )}
              <ReactFlagsSelect
                countries={["RU", "US"]}
                onSelect={this.selectLanguage}
                customLabels={{ US: "EN", RU: "RU" }}
                defaultCountry={
                  currentLanguage === "en"
                    ? "US"
                    : currentLanguage.toUpperCase()
                }
                className="nav-language"
              />
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  <div className="user-image">
                    <img src={userImg} alt="user" />
                  </div>
                  {userName}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem href="/signout" tag="a">
                    log out
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  // currentHotel
  const user = state.user || {};
  const userImg = user.img;
  const userName = user.name;
  const hotels = getHotels(state);
  const hotelsIds = getHotelsIds(state);
  const currentHotel = state.hotels.currentHotel;

  return { hotels, hotelsIds, currentHotel, userImg, userName };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  // setLanguage: lang => dispatch(changeLanguage(lang))
  selectHotel: hid => {
    dispatch(selectHotel(hid));
    dispatch(fetchCalendarData());
    dispatch(fetchSelectedDateDetails());
    dispatch(fetchInitialStateSalesRate());
    dispatch(fetchInitialStateArtboard());
  }
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withTranslation()(NB))
);
