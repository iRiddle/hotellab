import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

class InDev extends Component {
  componentDidMount(){
    document.title = "hotellab ⚠"
  }
  render() {
    return (
      <Container fluid>
      
        <Row className='container-row'>
          <Col className='text-center'>
            <h1>Данный раздел находится в разработке</h1>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default InDev;
