import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import Settings from "../containers/Settings";

import InDev from "./InDev";
import Calendar from "./Calendar";
import SalesRate from "./SalesRate";
import Artboard from "./Artboard";
import SalesDepth from "./SalesDepth";
import DigitalMarketing from "./DigitalMarketing";

class Main extends Component {
  render() {
    return (
      <main>
        <Switch>
          <Route path="/calendar" component={Calendar} />
          <Route path="/sales_rate" component={SalesRate} />
          <Route path="/home" component={InDev} />
          <Route path="/home1" component={InDev} />

          <Route path="/in_development" component={InDev} />
          <Route exact path="/" component={Artboard} />

          {/* <Route path="/sales_depth" component={SalesDepth} /> */}
          <Route path="/dashboard" component={SalesDepth} />
          <Route path="/digital_marketing" component={DigitalMarketing} />
          <Route path="/settings" component={Settings}></Route>
        </Switch>
      </main>
    );
  }
}
export default Main;
