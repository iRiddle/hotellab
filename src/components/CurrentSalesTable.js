import React, { Component } from "react";
import { Table, Row, Col } from "reactstrap";
import { withTranslation } from "react-i18next";

import { formatMoney } from "../utils";

const isEmpty = require("lodash/isEmpty");
const numeral = require("numeral");

class CurrentSalesTable extends Component {
  parseCountNamed = num => {
    if (num >= 1000000) return numeral(num).format("0.0 a");
    return formatMoney(num);
  };

  render() {
    const { data, currencySymbol } = this.props;
    let rows = [];
    for (let title in this.props.data.data) {
      let vals = this.props.data.data[title];
      rows.push(
        <tr key={title}>
          <th scope="row table-row-title">{title}</th>
          {vals.map((val, i) => {
            return (
              <td key={i} className={val[1] ? "growth" : "reduction"}>
                <Row className="text-right">
                  <Col
                    xs="6"
                    sm="5"
                    lg="5"
                    title={`${formatMoney(val[0])} ${currencySymbol}`}
                  >
                    {title === "Загрузка"
                      ? `${val[0]} %`
                      : `${this.parseCountNamed(val[0])} ${currencySymbol}`}
                  </Col>
                  <Col xs="2" sm="2" lg="2" className="text-center">
                    <span className="icon">{val[1] ? "↗" : "↘"}</span>
                  </Col>
                  <Col
                    xs="2"
                    sm="5"
                    lg="5"
                    className="text-center"
                  >{`${val[2]} %`}</Col>
                </Row>
              </td>
            );
          })}
        </tr>
      );
    }
    return (
      <Table hover borderless className="sales-table">
        <thead>
          <tr>
            <th />

            {!isEmpty(data) &&
              data.head.map((text, i) => {
                return (
                  <th className="text-right" key={i}>
                    {text}
                  </th>
                );
              })}
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    );
  }
}
export default withTranslation()(CurrentSalesTable);
