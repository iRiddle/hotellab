import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import ReactEcharts from 'echarts-for-react';

import Panel from '../Panel'
import CurrentSalesTable from '../CurrentSalesTable'
import NearestSalesPanel from '../NearestSalesPanel'

class CalendarGraph extends Component {
  render() {

    let sales = {
      head: ['Октябрь', 'Ноябрь', 'Декабрь', 'Итого'],
      data: {
        'Загрузка': [['60%', true, '6%'], ['10%', true, '6%'], ['40%', true, '6%'], ['60%', true, '6%']],
        'ADR': [['4 000 ₽', false, '-2%'], ['7 000 ₽', false, '-2%'], ['2 500 ₽', false, '-2%'], ['4 000 ₽', false, '-2%']],
        'RevPAR': [['2 500 ₽', true, '4%'], ['6 400 ₽', true, '4%'], ['1 600 ₽', true, '4%'], ['2 500 ₽', true, '4%']],
        'Доход': [['547 000 ₽', true, '3%'], ['47 000 ₽', true, '3%'], ['23 000 ₽', true, '3%'], ['547 000 ₽', true, '3%']]
      }
    }

    let nsales = [
      {
        title: 'Доходы',
        x: ['19.11', '20.11', '21.11', '22.11', '23.11', '24.11', '25.11'],
        y: [201560, 190540, 400500, 400390, 200560, 220439, 185670]
      },
      {
        title: 'Загрузка',
        x: ['19.11', '20.11', '21.11', '22.11', '23.11', '24.11', '25.11'],
        y: [101560, 190540, 100500, 100390, 100560, 120439, 185670]
      },
      {
        title: 'ADR',
        x: ['19.11', '20.11', '21.11', '22.11', '23.11', '24.11', '25.11'],
        y: [201560, 290540, 200500, 200390, 200560, 220439, 285670]
      },
      {
        title: 'RevPAR',
        x: ['19.11', '20.11', '21.11', '22.11', '23.11', '24.11', '25.11'],
        y: [301560, 390540, 300500, 300390, 300560, 320439, 385670]
      }
    ]

    let keyMetr = [
      {
        title: "... проживания",
        val: "1,87",
        good: true,
        diff: 37
      },
      {
        title: "Отмены",
        val: "12,8%",
        good: true,
        diff: 12
      },
      {
        title: "Комиссии",
        val: "33 456₽",
        good: true,
        diff: -17
      },
      {
        title: "Средняя глубина бронирования",
        val: "3,2",
        good: false,
        diff: 3
      },
    ]

    let rating = [
      [54650, '13.11'],
      [46750, '9.11'],
      [38300, '12.11'],
      [65745, '10.11'],
      [32780, '11.11'],
      [53640, '8.11'],
      [70546, '7.11'],
    ]

    rating.sort((v1, v2) => v1[0] - v2[0])

    var ratingOptions = {
      dataset: {
        source: rating
      },
      grid: { containLabel: true },
      xAxis: { 
        // name: 'amount' 
        // axisLabel: {
        //   show: false
        // },
        show: false
      },
      yAxis: { 
        type: 'category',
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        axisLabel: {
          show: true,
          fontSize: 18
        }
      },
      visualMap: {
        show: false,
        // orient: 'horizontal',
        // left: 'center',
        min: rating[0][0],
        max: rating[rating.length-1][0],
        // text: ['High Score', 'Low Score'],
        // Map the score column to color
        dimension: 0,
        inRange: {
          color: ['red', 'green'],
        }
      },
      series: [
        {
          type: 'bar',
          barWidth: 5,
          label: {
            normal: {
              show: true,
              // color: '#333333',
              // offset: [0, -10],
              position: 'right',
              fontSize: 16,
              // formatter: function (o) {
              //   return o.value.toFixed().replace(/\d(?=(\d{3})+$)/g, '$& ') + ' ₽'
              // }
            }
          },
          encode: {
            // Map the "amount" column to X axis.
            x: 'amount',
            // Map the "product" column to Y axis
            y: 'product'
          }
        }
      ]
    };


    return (
      <Container fluid>
        <Row className='container-row'>
          <Col sm='8' xs='12'>
            <Panel id="1-1" title="Текущие продажи за 3 месяца по сравнению с прошлым годом на эту дату" description="Текст подсказки 1.1">
              <CurrentSalesTable data={sales} />
            </Panel>
          </Col>
          <Col sm='4' xs='12'>
            <Panel id="1-2" title="Ключевые метрики текущего месяца" description="Текст подсказки 1.2">
              <Row className="key-metrics">
                {keyMetr.map((v, i) => {
                  return <Col sm="6" xs="12" key={i} className={'metric ' + (v.good ? 'good' : 'bad')}>
                    <h6 className="title">{v.title}</h6>
                    <h2 className="val">{v.val}</h2>
                    <span className="diff">{(v.diff > 0) ? '↗ +' : '↘ -'}{v.diff}% к прошлому году</span>
                  </Col>
                })}
              </Row>
            </Panel>
          </Col>
        </Row>

        <Row className='container-row'>
          <Col sm='8' xs='12'>
            <NearestSalesPanel data={nsales} />
          </Col>
          <Col sm='4' xs='12'>
            <Panel id="2-2" title="Рейтинг дат за последние 7 дней" description="Текст подсказки 2.2">
              <ReactEcharts
                style={{ height: '350px' }}
                option={ratingOptions}
              />
            </Panel>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default CalendarGraph;
