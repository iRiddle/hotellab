import React from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class CheckboxDropdown extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleOption = this.toggleOption.bind(this);
    this.selectAll = this.selectAll.bind(this);
    // this.emitChange = this.emitChange.bind(this);
    let _items = this.props.items||{}
    let _checked = this.props.checked||[]
    let items = {}
    let options = Object.keys(_items)
    for(var iid in _items){
      items[iid] = {
        title: _items[iid],
        checked: (_checked.indexOf(iid)!==-1)
      }
    }
    // console.log(_checked, items);
    

    this.state = {
      dropdownOpen: false,
      options,
      items,
      title: this.props.title||''
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  toggleOption(option) {
    // console.log(currentOption, this.state.items[currentOption], this.state.items);
    let items = {...this.state.items}
    items[option].checked = !this.state.items[option].checked
    this.setState({
      items: items
    })
    let checked = []
    for(var i in items){
      if(items[i].checked) checked.push(i)
    }
    // console.log(checked);
    
    this.props.onChange(checked);
  }

  selectAll() {
    let items = {...this.state.items}
    let checked = []
    for(var i in items){
      items[i].checked = true;
      checked.push(i)
    }
    this.setState({
      items: items
    })
    
    
    this.props.onChange(checked);
  }



  render() {
    let checked = 0
    for(var i in this.state.items){
      if(this.state.items[i].checked) checked++
    }
    return (
      <ButtonDropdown className="checkbox-dropdown" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret outline color="primary">
          {this.state.title+' '+checked+' из '+this.state.options.length}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={() => this.selectAll()} className="select-all">Выделить все</DropdownItem>
          <DropdownItem divider />
          {this.state.options.map(o => 
            <DropdownItem active={this.state.items[o].checked} key={o} onClick={() => this.toggleOption(o)}>
              {/* <span>{this.state.items[o].checked?'V':'O'}</span> */}
              {this.state.items[o].title}
            </DropdownItem>)}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}