import React, { Component } from 'react';
import { Button, ButtonGroup, Row, Col } from 'reactstrap';
import ReactEcharts from 'echarts-for-react';
import echarts from 'echarts';
import Panel from '../Panel'

// import Octicon, { Mail } from '@githubprimer/octicons-react'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class CalendarGraph extends Component {

  constructor (props) {
    super(props);
    this.state = { rSelected: 0 };
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
  }

  onRadioBtnClick(rSelected) {
    this.setState({ rSelected });
  }

  render() {
    let options = {
      // visualMap: [{
      //     show: false,
      //     type: 'continuous',
      //     seriesIndex: 0,
      //     min: 600,
      //     max: 1500
      // }],
      xAxis: {
        type: 'category',
        // data: ['19.11', '20.11', '21.11', '22.11', '23.11', '24.11', '25.11']
        data: this.props.data[this.state.rSelected].x
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: function (value, index) {
            return value.toFixed().replace(/\d(?=(\d{3})+$)/g, '$& ') + ' ₽'
          }
        }
      },
      tooltip: {
        trigger: 'axis',
        // formatter: "text<br/>{b}: {c}"
        formatter: function (a) {
          let o = a[0];
          // console.log(o);
          return o.name + ': ' + o.value.toFixed().replace(/\d(?=(\d{3})+$)/g, '$& ') + ' ₽'
        }
      },
      series: [{
        // data: [201560, 190540, 400500, 400390, 200560, 220439, 185670],
        data: this.props.data[this.state.rSelected].y,
        type: 'line',
        // showSymbol: false,
        label: {
          normal: {
            show: true,
            color: '#333333',
            offset: [0, -10],
            // position: 'bottom',
            position: 'top',
            // offset: function(o){
            //   console.log(o);
            //   return [10,10]
            // },
            formatter: function (o) {
              // console.log(o); // dataIndex: 5
              return o.value.toFixed().replace(/\d(?=(\d{3})+$)/g, '$& ') + ' ₽'
            }
          }
        },
        lineStyle: {
          normal: {
            width: 4,
            color: new echarts.graphic.LinearGradient(
              0, 0, 0, 1,
              [
                { offset: 0, color: '#008800' },
                // {offset: 0.5, color: '#ffffff'},
                { offset: 1, color: '#ff0000' }
              ]
            )
          }
        },
      }]
    };

    // echarts.registerTheme('grt', {
    //   // backgroundColor: '#f4cccc'
    // });

    return (
      <Panel id="2-1" title="Продажи ближайшие 7 дней" description="Текст подсказки 2.1">
        <Row className="text-center">

        </Row>
        <ReactEcharts
          option={options}
          theme="grt"
        />
      </Panel>
    )
  }
}
export default CalendarGraph;
