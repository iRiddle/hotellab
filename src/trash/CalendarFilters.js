import React from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class CustomSelect extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    // this.emitChange = this.emitChange.bind(this);
    let options = Object.keys(this.props.items||[])
    let currentOption = this.props.currentOption||options[0]

    this.state = {
      dropdownOpen: false,
      currentOption,
      options,
      items: this.props.items||{}
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  setCurrentOption(currentOption) {
    
    this.setState({
      currentOption: currentOption
    })
    this.props.onChange(currentOption);
  }

  render() {    
    return (
      <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret outline color="primary">
          {this.state.items[this.state.currentOption]}
        </DropdownToggle>
        <DropdownMenu>
          {this.state.options.map(o => 
            <DropdownItem className={this.state.currentOption===o?'active':''} active={this.state.currentOption===o} key={o} onClick={() => this.setCurrentOption(o)}>
              {this.state.items[o]}
            </DropdownItem>)}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}