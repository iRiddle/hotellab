import React, { Component } from 'react';
import { Table, Container, Row, Col, Button, Collapse, Nav, NavItem, NavLink, ButtonGroup } from 'reactstrap';
import classnames from 'classnames';
// import CalendarGraph from './CalendarGraph';
import ReactEcharts from 'echarts-for-react';
import CustomSelect from '../CustomSelect';


let MONTHLABELS = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']

class Calendar extends Component {
  constructor(props) {
    super(props);


    let competitorsTariffs = [
      {name: 'Ареал Конгресс отель', price: '8 100 ₽', diff: '+1 100 ₽' },
      {name: 'Яхонты Таруса', price: '7 900 ₽', diff: '+1 000 ₽' },
      {name: 'Hilton Garden', price: '7 850 ₽', diff: '+800 ₽' },
      {name: 'Tulip Inn', price: '7 600 ₽', diff: '+700 ₽' },
      {name: 'Heliopark', price: '7 230 ₽', diff: '+450 ₽' },
      {name: 'Radisson', price: '6 990 ₽', diff: '+300 ₽' },
      {name: 'Мой отель', price: '6 800 ₽', diff: '0 ₽' },
    ]
    this.nextMonth = this.nextMonth.bind(this);
    this.prevMonth = this.prevMonth.bind(this);

    this.onEntering = this.onEntering.bind(this);
    this.onExited = this.onExited.bind(this);
    this.toggle = this.toggle.bind(this);
    
    this.setFilterOption = this.setFilterOption.bind(this);
    

    let d = new Date()
    d.setDate(1)
    let tableConfig = this.getTableConfig(d.getTime())


    let graphData = {
      hotel: [],
      competitors: [],
      market: []
    }

    for(var gn in graphData){
      var bv = 50
      for(var i=0;i<=60;i+=5){
        bv -= Math.random()*5
        graphData[gn].push([i, bv])
      }
    }

    this.state = {
      d: d,
      collapse: true,
      collapsed: true,
      activeGraph: 'competitors',
      // calendarData: data,
      tableConfig: tableConfig,
      tableData: this.getTableData(tableConfig.count),
      graphData: graphData,
      competitorsTariffs: competitorsTariffs,
      filter: props.filter||{}
    };
  }

  toggle() {
    // this.setState({ ...this.state, collapse: !this.state.collapse });
    this.setState({ collapse: !this.state.collapse });
  }
  onExited() {
    this.setState({ collapsed: false });
  }
  onEntering() {
    this.setState({ collapsed: true });
  }
  toggleGraph(graph) {
    if (this.state.activeGraph !== graph) {
      this.setState({
        activeGraph: graph
      });
    }
  }
  nextMonth() {
    let newDate = new Date(this.state.d.getFullYear(), this.state.d.getMonth()+1)
    let tableConfig = this.getTableConfig(newDate.getTime())
    this.setState({
      d: newDate,
      tableConfig: tableConfig,
      tableData: this.getTableData(tableConfig.count)
    });
  }
  prevMonth() {
    let newDate = new Date(this.state.d.getFullYear(), this.state.d.getMonth()-1)
    let tableConfig = this.getTableConfig(newDate.getTime())
    this.setState({
      d: newDate,
      tableConfig: tableConfig,
      tableData: this.getTableData(tableConfig.count)
    });
  }

  getTableData(count){
    let data = {}
    for(let i=0; i<=count;i++){
      data[i] = [((Math.random()>0.5)?5600:4300), 5100]
    }
    return data
  }

  setFilterOption(name, value){
    let filter = {}
    filter[name] = value
    filter = {
      ...this.state.filter,
      ...filter
    }
    
    this.setState({
      filter
    })
   
  }

  // fetchTableData(){
  //   // getTableData(count)
  // }
  
  getTableConfig(time){
    let c = {}
    let d = new Date(time);
    c.from = d.getDay()


    d.setDate(d.getDate()-1)  // последнее число предидущего месяца
    c.pmc = d.getDate()
    d.setDate(d.getDate()+1)
    
    d.setMonth(d.getMonth()+1)  // 1ое число следующего месяца
    d.setDate(d.getDate()-1)  // последнее число нужного месяца
    c.count = d.getDate()
    c.rows = (c.from+c.count-1)/7

    return c
  }
  // componentDidMount() {  }

  render() {
    

    let c = this.state.tableConfig
    let data = this.state.tableData

    let tableRows = []
    // console.log(c);
   
    // tableRows.push()
    
    let cd = -c.from+1
    for(let i=0;i<c.rows;i++){
      var dates = []
      for(let j=0;j<7;j++){
        cd++
        var date = ''
        var v1 = ''
        var v2 = ''
        var cn = ''
        var sign = ''
        if(cd>0&&cd<=c.count) {
          date = cd
          if(data.hasOwnProperty(cd)){
            v1 = data[cd][0]
            v2 = data[cd][1]
            if(data[cd][0] > data[cd][1]){
               cn = 'green'
               sign = '↗'
            }
            else{
              cn = 'red'
              sign = '↘'
            }
          }
        }else if(cd<=0){
          date = c.pmc+cd
        }else{
          date = cd-c.count
        }
        dates.push(<td className={cn} key={j}><span className="date">{date}</span><span className="v1">{v1}{sign}</span><span className="v2">{v2}</span></td>)
      }
      tableRows.push(<tr key={i}>{dates}</tr>)
    }

    let mn = this.state.d.getMonth()
    


    let graphConfig = {
      xAxis: {
        type: 'value',
        name: 'Количество дней до заезда',
        nameLocation: 'center',
        nameGap: 30,
        axisTick: {
          show: false
        },
        // axisLine: {
        //   show: false
        // },
        splitLine:{
          show: false
        }
      },
      yAxis: {
        type: 'value',
        name: 'Количество проданных номеров',
        nameLocation: 'center',
        nameGap: 30,
        // axisLine: {
        //   show: false
        // },
        axisTick: {
          show: false
        },
      },
      grid: {
        left: 60,
        // top: 0,
        // right: 0,
        // bottom: 0
      },
      tooltip: {
        trigger: 'axis',
        // formatter: "text<br/>{b}: {c}"
        formatter: function (a) {
          let o = a[0];
          // console.log(o);
          return o.value[0] + ': '+ o.value[1].toFixed(2)
        }
      },
      series: [{
          lineStyle: {
            color:"#51beaa"  
          },
          data: this.state.graphData[this.state.activeGraph],
          type: 'line',
      }]
    }
    // console.log(this.state.filter);

    return (
      <Container fluid className="table-cont">
        <Row className="head">
          <Col sm='8' className="calendar-filters">
            <div className="panel">
              <Row className="text-center">
                <Col>
                <ButtonGroup>
                  <CustomSelect onChange={v => this.setFilterOption('tarif',v)} items={{1:"Тариф 1", 2: "Тариф 2", 3: "Тариф 3"}} />
                {/* </Col>
                <Col> */}
                  <CustomSelect onChange={v => this.setFilterOption('roomType',v)} items={{1:"Тип 1", 2: "Тип 2", 3: "Тип 3"}} />
                {/* </Col>
                <Col> */}
                  <CustomSelect onChange={v => this.setFilterOption('peopleAmount',v)} items={{1:"1", 2: "2", 3: "3"}} />
                </ButtonGroup>
                </Col>
              </Row>
            </div>
          </Col>
          <Col sm='4' className="calendar-graph">
            <div className="graph-panel panel">
              <Button className="toggle" color="primary" onClick={this.toggle}>V</Button>
              <Nav pills>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeGraph === 'hotel' })}
                    onClick={() => { this.toggleGraph('hotel'); }}
                  >
                    Отель
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeGraph === 'competitors' })}
                    onClick={() => { this.toggleGraph('competitors'); }}
                  >
                    Конкуренты
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.activeGraph === 'market' })}
                    onClick={() => { this.toggleGraph('market'); }}
                  >
                    Рынок
                  </NavLink>
                </NavItem>
              </Nav>
              

              
              <Collapse
                onEntering={this.onEntering}
                // onEntered={this.onEntered}
                // onExiting={this.onExiting}
                onExited={this.onExited}
                isOpen={this.state.collapse}>
                <div>
                  <ReactEcharts
                    option={graphConfig}
                    style={{ height: '400px' }}
                    // theme="light"
                  />
                  <div>
                    <h4>Тарифы группы конкурентов</h4>
                    <Table borderless>
                      <thead><tr><th>Название</th><th>Цена</th><th>Разница</th></tr></thead>
                      <tbody>
                        {this.state.competitorsTariffs.map((r,n) => <tr key={n}><td>{r.name}</td><td>{r.price}</td><td>{r.diff}</td></tr> )}
                      </tbody>
                    </Table>
                  </div>
                  {/* <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                      <Row>
                        <Col sm="12">
                          <h4>Tab 1 Contents</h4>
                        </Col>
                      </Row>
                    </TabPane>
                    <TabPane tabId="2">
                      <Row>
                        <Col sm="12">
                          <h4>Tab 2 Contents</h4>
                        </Col>
                      </Row>
                    </TabPane>
                    <TabPane tabId="3">
                      <Row>
                        <Col sm="12">
                          <h4>Tab 3 Contents</h4>
                        </Col>
                      </Row>
                    </TabPane>
                  </TabContent> */}
                </div>
              </Collapse>
            </div>
          </Col>
        </Row>
        <Row>
          <Col sm={this.state.collapsed?'8':'12'} className="calendar">
            <div className="panel">
              <span>{this.state.d.getFullYear()}</span>
              <button onClick={this.prevMonth} >&lt;-</button>
              <span style={{minWidth: '100px', display: 'inline-block'}}>{MONTHLABELS[mn]} ({mn+1})</span>
              <button onClick={this.nextMonth}>-&gt;</button>
              <div className="table-wrap">
                <Table bordered>
                  <thead><tr><th>пн</th><th>вт</th><th>ср</th><th>чт</th><th>пт</th><th>сб</th><th>вс</th></tr></thead>
                  <tbody>
                    {tableRows}
                  </tbody>
                </Table>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default Calendar;
