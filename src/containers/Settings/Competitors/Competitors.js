import React, { Component } from "react";
import Select from "react-select";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";
import cx from "classnames";

import {
  buildConsumers,
  filterConsumers
} from "../../../redux/actions/jsonFormed";

import { sortCompetitorsBy } from "../../../redux/actions/settings";

import RangeComponent from "../../../components/ChildComponents/Range/Range";
import HotelObject from "../../../components/ChildComponents/HotelObject";
import Star from "../../../components/ChildComponents/Star";
import Input from "../../../components/ChildComponents/Input";

class Competitors extends Component {
  state = {
    selectedHotels: {},
    options: [
      { value: 0, label: "умолчанию" },
      { value: 1, label: "дистанции" },
      { value: 2, label: "рейтингу" }
    ],
    selectedOption: 0
  };

  selectHotel = id => {
    const { selectedHotels } = this.state;
    const { buildConsumers, filterConsumers } = this.props;
    if (Object.keys(selectedHotels).some(h => h === id)) {
      delete selectedHotels[id];
      this.setState(
        {
          selectedHotels
        },
        () => filterConsumers(id)
      );
    } else {
      this.setState(
        {
          selectedHotels: {
            [id]: id,
            ...selectedHotels
          }
        },
        () => buildConsumers(id)
      );
    }
  };

  handleSelectOption = selectedOption => {
    this.setState(
      {
        selectedOption: selectedOption.value
      },
      () => this.props.sortCompetitorsBy(selectedOption.value)
    );
  };

  render() {
    const {
      distance,
      currentDistance,
      onChangeRange,
      onChangeQuery,
      stars,
      starsE,
      handleStars,
      competitors,
      query,
      settingsPending
    } = this.props;
    const { selectedHotels, options, selectedOption } = this.state;
    return (
      <div className="competitors-container">
        <div className="competitors-container__left-side">
          <h3 className="left-side__title">Выбрать конкурентов</h3>
          <Input
            type="text"
            className="left-side__input"
            placeholder="Найдите объект рядом с вами"
            value={query}
            onChange={e => onChangeQuery(e.target.value)}
          />
          <div className="left-side__secondary-title">
            <span>Расстояние от вашего объекта</span>
          </div>
          <div className="left-side__range">
            <RangeComponent
              onChangeRange={onChangeRange}
              maxDistance={distance}
              currentDistance={currentDistance}
            />
          </div>
          <div className="left-side__last-filters">
            <div className="last-filters__stars">
              <div className="filter-description">
                <span>Количество звезд</span>
              </div>
              {stars.map(star => (
                <Star
                  key={star.id}
                  className={cx(
                    "star",
                    {
                      "star-active":
                        starsE[1] >= starsE[0]
                          ? star.id >= starsE[0] && star.id <= starsE[1]
                          : star.id <= starsE[0] && star.id >= starsE[1]
                    },
                    {
                      "star-active": starsE[0] === star.id
                    }
                  )}
                  onClick={() => handleStars(star.id)}
                />
              ))}
            </div>
            <div className="last-filters__sortedBy">
              {settingsPending ? (
                <div className="sortedBy__loader">
                  <Loader
                    type="TailSpin"
                    color="#2b776f"
                    height={25}
                    width={25}
                  />
                </div>
              ) : (
                <>
                  <div className="filter-description">Сортировать по:</div>
                  <Select
                    placeholder="Выберите опцию"
                    options={options}
                    onChange={this.handleSelectOption}
                    value={options.filter(
                      option => option.value === selectedOption
                    )}
                  />
                </>
              )}
            </div>
          </div>
        </div>
        <div className="competitors-container__right-side">
          <div className="filter-description">
            <span>Выбрано объектов</span>
          </div>
          {settingsPending ? (
            <div className="loader-hotels">
              <Loader type="TailSpin" color="#2b776f" height={50} width={50} />
            </div>
          ) : !competitors.length ? (
            <div className="competitors_empty">Конкуренты не найдены</div>
          ) : (
            competitors.map(competitor => (
              <HotelObject
                key={competitor.hotel_id}
                id={competitor.hotel_id}
                title={competitor.hotel_title}
                stars={competitor.stars}
                starsBuilder={stars}
                className={"star-active"}
                rating={competitor.rating}
                distance={competitor.distance}
                selectedHotels={selectedHotels}
                selectHotel={this.selectHotel}
              />
            ))
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  buildConsumers: id => {
    dispatch(buildConsumers(id));
  },
  filterConsumers: id => {
    dispatch(filterConsumers(id));
  },
  sortCompetitorsBy: sortBy => {
    dispatch(sortCompetitorsBy(sortBy));
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Competitors);
