import React, { Component } from "react";
import cx from "classnames";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";

import {
  addRecommendationToStore,
  deleteRecommendationFromStore
} from "../../../redux/actions/jsonFormed";

import { sendDataToServer } from "../../../redux/actions/settings";

import { getCurrentHotel } from "../../../redux/selectors";
import {
  getRecommendationsAfterQuery,
  getHotelIdSelector,
  getAllFactorySettingsSelector
} from "../../../redux/selectors/jsonFormed";

import MoreRecInfo from "../MoreRecInfo";

import Input from "../../../components/ChildComponents/Input";
import Button from "../../../components/ChildComponents/Button";
import InfoModal from "../../../components/ChildComponents/Modals/InfoModal";

import arrowDown from "../icons/arrowDown.svg";
import arrowUp from "../icons/arrowUp.svg";
import cross from "../icons/cross.svg";

import { getUniqId } from "../../../utils";
import { recommendationFactory } from "../factory";

class Recommendations extends Component {
  state = {
    recommendations: [],
    currentRecommendation: "",
    currentRecommendationId: -1,
    isOpenInfoModal: false,
    message: ""
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.recommendations !== nextProps.recommendationsAfterQuery) {
      return {
        recommendations: nextProps.recommendationsAfterQuery
      };
    }
    return;
  }

  handleChange = e => {
    this.setState({
      currentRecommendation: e.target.value
    });
  };

  addRecommendation = () => {
    const { recommendations, currentRecommendation } = this.state;
    const { addRecommendationToStore, hotelId } = this.props;
    const recWithId = { id: getUniqId(), title: currentRecommendation };
    this.setState(
      {
        recommendations: [...recommendations, recWithId],
        currentRecommendation: ""
      },
      () => addRecommendationToStore(recommendationFactory(recWithId, hotelId))
    );
  };

  removeRecommendation = id => {
    const { recommendations } = this.state;
    const { deleteRecommendationFromStore } = this.props;
    this.setState(
      {
        recommendations: recommendations.filter(item => item.id !== id)
      },
      () => deleteRecommendationFromStore(id)
    );
  };

  expandMoreInfo = id => {
    id === this.state.currentRecommendationId
      ? this.setState({
          currentRecommendationId: -1
        })
      : this.setState({
          currentRecommendationId: id
        });
  };

  handleSendDataToServer = () => {
    const {
      hotelIdOurSystem,
      allFactorySettings,
      sendDataToServer
    } = this.props;
    const builtObject = {
      hotel_id: hotelIdOurSystem,
      data: allFactorySettings
    };
    sendDataToServer(builtObject, this.handleResponseSendingFromServer);
  };

  handleResponseSendingFromServer = message => {
    this.setState({
      isOpenInfoModal: true,
      message
    });
  };

  closeInfoModal = () => {
    this.setState({
      isOpenInfoModal: false
    });
  };

  render() {
    const {
      currentRecommendation,
      recommendations,
      currentRecommendationId,
      isOpenInfoModal,
      message
    } = this.state;
    const { settingsPending } = this.props;
    return (
      <div className="recommendations-container">
        <h3 className="recommendations-container__title">
          Настроить рекомендации к категориям номеров
        </h3>
        {settingsPending ? (
          <div className="loader-hotels-rec">
            <Loader type="TailSpin" color="#2b776f" height={50} width={50} />
          </div>
        ) : (
          <>
            {recommendations.length > 0 && (
              <div className="recommendations-container__list">
                {recommendations.map((rec, i) => (
                  <div key={rec.id}>
                    <div
                      className={cx("recommendations-container__item", {
                        item_even: i % 2 === 0
                      })}
                      onClick={() => this.expandMoreInfo(rec.id)}
                    >
                      <span className="recommendation__title">{rec.title}</span>
                      <div className="recommendation__actions">
                        <img
                          src={
                            currentRecommendationId === rec.id
                              ? arrowUp
                              : arrowDown
                          }
                          className="recommendation__arrow"
                          alt="Развернуть"
                        />
                        <img
                          src={cross}
                          alt="Удалить"
                          className="recommendation__cross"
                          onClick={() => this.removeRecommendation(rec.id)}
                        ></img>
                      </div>
                    </div>
                    {currentRecommendationId === rec.id && (
                      <MoreRecInfo
                        currentRecommendationId={currentRecommendationId}
                      />
                    )}
                  </div>
                ))}
              </div>
            )}

            <div className="recommendations-container__input">
              <Input
                type="text"
                placeholder="Введите название рекомендации"
                value={currentRecommendation}
                onChange={this.handleChange}
                className={cx("input__text")}
              ></Input>
              <Button
                title="Добавить"
                onClick={this.addRecommendation}
                className="input__button"
                disabled={!currentRecommendation.length}
              />
            </div>
            {recommendations.length > 0 && (
              <div className="recommendations-container__save">
                <Button
                  title="Сохранить"
                  className="input__button"
                  onClick={this.handleSendDataToServer}
                ></Button>
              </div>
            )}
            <InfoModal
              isOpen={isOpenInfoModal}
              closeModal={this.closeInfoModal}
              modalTitle="Информация"
              message={message}
              btnClassName="input__button"
            />
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hotelId: getHotelIdSelector(state),
  hotelIdOurSystem: getCurrentHotel(state),
  allFactorySettings: getAllFactorySettingsSelector(state),
  recommendationsAfterQuery: getRecommendationsAfterQuery(state)
});

const mapDispatchToProps = dispatch => ({
  addRecommendationToStore: recommendation => {
    dispatch(addRecommendationToStore(recommendation));
  },
  deleteRecommendationFromStore: id => {
    dispatch(deleteRecommendationFromStore(id));
  },
  sendDataToServer: (data, callback) => {
    dispatch(sendDataToServer(data, callback));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Recommendations);
