import React, { Component } from "react";
import { connect } from "react-redux";

import {
  addOwnHotelInfo,
  addHotelCompetitorToJson,
  addOtherInfoRec,
  addCategoryPms,
  removeCategoryPmsFromStore
} from "../../../redux/actions/jsonFormed";
import {
  getConsumersSelector,
  getRecommndationsFactory,
  getHotelIdSelector
} from "../../../redux/selectors/jsonFormed";
import { getCurrentHotelTitle } from "../../../redux/selectors";

import CompetitorsSettings from "../CompetitorSettings";

import Input from "../../../components/ChildComponents/Input";
import Button from "../../../components/ChildComponents/Button";

import CompetitorsModal from "../../../components/ChildComponents/Modals/CompetitorsModal";
import OwHotelModal from "../../../components/ChildComponents/Modals/OwnHotelModal";
import OtherSettingsModal from "../../../components/ChildComponents/Modals/OtherSettingsModal";

import arrowUp from "../icons/arrowUp.svg";
import arrowDown from "../icons/arrowDown.svg";

import { competitorFactory, categoryPms } from "../factory";
import { getUniqId } from "../../../utils";

import get from "lodash/get";
import debounce from "lodash/debounce";

class MoreRecInfo extends Component {
  constructor(props) {
    super(props);
    const { currentRecommendationId, hotelOwnId } = this.props;
    const {
      standart_category,
      person_capacity,
      breakfast,
      free_cancel
    } = this.props.recommndationsFactory.filter(
      item => item.id === currentRecommendationId
    )[0].parsing.concurents[0][hotelOwnId];

    const { base_price } = this.props.recommndationsFactory.filter(
      item => item.id === currentRecommendationId
    )[0].parsing;

    const hc = this.props.recommndationsFactory
      .filter(item => item.id === currentRecommendationId)[0]
      .parsing.concurents.slice(1);

    const {
      price_edges,
      profile_name,
      profile_group,
      current_category,
      categorys,
      excl_list
    } = this.props.recommndationsFactory.filter(
      item => item.id === currentRecommendationId
    )[0];

    this.state = {
      modalIsOpen: false,
      modalOtherSettings: false,
      hotelCompetitors: [...hc],
      stepCompetitors: [],
      currentCompetitorId: -1,
      isSettingsHotelOpen: false,
      pmsCategory: "",
      pmsCategories: categorys,
      removeIndex: -1,
      pmsCategoryState: {},
      excl_list,
      ownHotelInfo: {
        base_price,
        standart_category: [standart_category],
        person_capacity,
        breakfast,
        free_cancel
      },
      breakfasts: [
        { id: 1, title: "Да", value: [1] },
        { id: 2, title: "Нет", value: [0] },
        { id: 3, title: "Да/нет", value: [0, 1] }
      ],
      cancels: [
        { id: 1, title: "Да", value: [1] },
        { id: 2, title: "Нет", value: [0] },
        { id: 3, title: "Да/нет", value: [0, 1] }
      ],
      price_edges,
      otherSettings: {
        profile_name,
        profile_group,
        current_category
      },
      currentPrice: {
        cost: "",
        name: ""
      },
      historyName: "",
      exclName: ""
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.pmsCategories[0].standart_category.title !==
      this.state.pmsCategories[0].standart_category.title
    ) {
      this.props.addCategoryPms(
        categoryPms(this.state.pmsCategoryState),
        this.props.currentRecommendationId
      );
    }
  }

  handleCategoryPms = e => {
    const value = e.target.value;
    const { pmsCategories } = this.state;
    const pmsCategoryState = {
      id: getUniqId(),
      title: value,
      history: pmsCategories[0].standart_category.history
    };
    this.setState({
      pmsCategories: [categoryPms(pmsCategoryState)],
      pmsCategoryState
    });
  };

  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };

  addHotelCompetitor = () => { // fix this piece of obscenity
    const { stepCompetitors, hotelCompetitors, removeIndex } = this.state;
    const { addHotelCompetitorToJson, currentRecommendationId } = this.props;
    this.setState(
      {
        hotelCompetitors:
          removeIndex === -1
            ? [
                ...hotelCompetitors,
                ...stepCompetitors.map(item =>
                  competitorFactory(item.hotel_id, item.hotel_title)
                )
              ]
            : [
                ...hotelCompetitors.slice(
                  0,
                  hotelCompetitors.length - removeIndex
                ),
                ...stepCompetitors.map(item =>
                  competitorFactory(item.hotel_id, item.hotel_title)
                )
              ],
        removeIndex: stepCompetitors.length
      },
      () => {
        addHotelCompetitorToJson(
          stepCompetitors.map(item =>
            competitorFactory(item.hotel_id, item.hotel_title)
          ),
          currentRecommendationId,
          removeIndex
        );
        this.closeModal();
      }
    );
  };

  checkHotelCompetitor = competitor => {
    const { stepCompetitors } = this.state;
    if (stepCompetitors.some(sc => sc.hotel_id === competitor.hotel_id)) {
      this.setState({
        stepCompetitors: stepCompetitors.filter(
          sc => sc.hotel_id !== competitor.hotel_id
        )
      });
    } else {
      this.setState({
        stepCompetitors: [...stepCompetitors, competitor]
      });
    }
  };

  expandMoreInfo = id => {
    if (id === this.state.currentCompetitorId) {
      this.setState({
        currentCompetitorId: -1
      });
    } else {
      this.setState({
        currentCompetitorId: id
      });
    }
  };

  settingsHotelOpen = () => {
    this.setState({
      isSettingsHotelOpen: true
    });
  };

  closeHotelSettingsModal = () => {
    this.setState({ isSettingsHotelOpen: false });
  };

  addOwnHotelInfoAction = () => {
    const {
      addOwnHotelInfo,
      currentRecommendationId,
      hotelOwnId,
      hotelOwnTitle
    } = this.props;
    const { ownHotelInfo, pmsCategories } = this.state;
    this.setState(
      {
        isSettingsHotelOpen: false
      },
      () =>
        addOwnHotelInfo(
          ownHotelInfo,
          currentRecommendationId,
          hotelOwnId,
          hotelOwnTitle,
          pmsCategories
        )
    );
  };

  handleOwnHotelInfo = (field, e) => {
    const value = e.target.value;
    const arraySetting = this.getSettingArray(field, value);
    this.setState(prevState => ({
      ownHotelInfo: {
        ...prevState.ownHotelInfo,
        [field]: arraySetting
      }
    }));
  };

  getSettingArray = (field, value) => {
    if (field === "breakfast" || field === "free_cancel") {
      switch (Number(value)) {
        case 0:
          return [0];
        case 1:
          return [1];
        default:
          return [0, 1];
      }
    }
    if (field === "person_capacity") {
      return [value];
    }
    return value;
  };

  getPropertyFromObj = (field, obj) => {
    const builder = obj[Object.keys(obj)];
    if (builder !== undefined) {
      return field === "id" ? builder.hotel_id : builder.hotel_title;
    }
  };

  toggleOtherSettings = () => {
    this.setState({
      modalOtherSettings: true
    });
  };

  closeOtherSettings = () => {
    this.setState({
      modalOtherSettings: false
    });
  };

  handlePriceLevel = (field, e) => {
    const value = e.target.value;
    this.setState(prevState => ({
      currentPrice: {
        ...prevState.currentPrice,
        [field]: value
      }
    }));
  };

  addPriceLevel = () => {
    const { currentPrice, price_edges } = this.state;
    const { cost, name } = currentPrice;
    this.setState({
      price_edges: { ...price_edges, [cost]: name },
      currentPrice: { cost: "", name: "" }
    });
  };

  removePriceLevel = price => {
    const { price_edges } = this.state;
    delete price_edges[price];
    this.setState({
      price_edges
    });
  };

  addHistoryLevel = () => {
    const { historyName, pmsCategories } = this.state;
    const pmsObject = pmsCategories[0];
    this.setState({
      pmsCategories: [
        {
          standart_category: {
            ...pmsObject.standart_category,
            history: [...pmsObject.standart_category.history, historyName]
          }
        }
      ],
      historyName: ""
    });
  };

  removeHistoryLevel = item => {
    const { pmsCategories } = this.state;
    const pmsObject = pmsCategories[0];
    this.setState({
      pmsCategories: [
        {
          standart_category: {
            ...pmsObject.standart_category,
            history: pmsObject.standart_category.history.filter(
              elem => elem !== item
            )
          }
        }
      ]
    });
  };

  addExclName = () => {
    const { exclName } = this.state;
    this.setState({
      excl_list: [...this.state.excl_list, exclName],
      exclName: ""
    });
  };

  handleExclName = e => {
    const value = e.target.value;
    this.setState({
      exclName: value
    });
  };

  removeExclData = item => {
    const { excl_list } = this.state;
    this.setState({
      excl_list: excl_list.filter(data => data !== item)
    });
  };

  handleHistory = e => {
    const value = e.target.value;
    this.setState({
      historyName: value
    });
  };

  addOtherSettings = () => {
    const { currentRecommendationId, addOtherInfoRec } = this.props;
    const { price_edges, otherSettings, excl_list } = this.state;
    this.setState(
      {
        modalOtherSettings: false
      },
      () =>
        addOtherInfoRec(
          { price_edges, otherSettings, excl_list },
          currentRecommendationId
        )
    );
  };

  handleOtherInfo = (field, e) => {
    const value = e.target.value;
    this.setState(prevState => ({
      otherSettings: {
        ...prevState.otherSettings,
        [field]: value
      }
    }));
  };

  render() {
    const {
      modalIsOpen,
      isSettingsHotelOpen,
      hotelCompetitors,
      currentCompetitorId,
      breakfasts,
      cancels,
      ownHotelInfo,
      stepCompetitors,
      modalOtherSettings,
      price_edges,
      currentPrice,
      otherSettings,
      pmsCategories,
      historyName,
      excl_list,
      exclName
    } = this.state;
    const { consumers, currentRecommendationId } = this.props;
    return (
      <div className="more-info-container">
        <div className="more-info__pms">
          <Input
            label
            lableTitle="Название в категории PMS"
            id="pms-input"
            type="text"
            className="more-info__input"
            onChange={this.handleCategoryPms}
            value={get(pmsCategories[0], "standart_category.title", "")}
          />
        </div>
        <div
          className="more-info__extra-settings"
          onClick={this.toggleOtherSettings}
        >
          <span className="more-info__label label">
            Прочие настройки (уровни цен и т.д.)
          </span>
          <img src={arrowDown} alt="Развернуть"></img>
        </div>
        <div className="more-info__competitors">
          <span className="more-info__label label bold">Конкуренты</span>
          <Button
            title="Добавить"
            className="input__button lh"
            onClick={this.openModal}
          />
        </div>
        {hotelCompetitors.length > 0 && (
          <div className="more-info__hotel-competitors">
            {hotelCompetitors.map((hc, index) => (
              <div key={hc.hotel_id}>
                <div
                  className="hotel-competitor"
                  onClick={() =>
                    this.expandMoreInfo(
                      hc.hotel_id !== undefined
                        ? hc.hotel_id
                        : this.getPropertyFromObj("id", hc)
                    )
                  }
                >
                  <span className="competitor__title">
                    {hc.hotel_title || this.getPropertyFromObj("title", hc)}
                  </span>
                  <img
                    src={
                      currentCompetitorId === hc.hotel_id ? arrowUp : arrowDown
                    }
                    className="recommendation__arrow"
                    alt="Развернуть"
                  />
                </div>
                {(currentCompetitorId === hc.hotel_id ||
                  currentCompetitorId ===
                    this.getPropertyFromObj("id", hc)) && (
                  <CompetitorsSettings
                    currentCompetitorId={currentCompetitorId}
                    currentRecommendationId={currentRecommendationId}
                    currentIndex={index}
                  />
                )}
              </div>
            ))}
          </div>
        )}
        <div className="more-info__own-hotel">
          <span
            className="own-holel__label label"
            onClick={this.settingsHotelOpen}
          >
            Мой отель
          </span>
        </div>
        <CompetitorsModal
          isOpen={modalIsOpen}
          closeModal={this.closeModal}
          addHotelCompetitor={this.addHotelCompetitor}
          checkHotelCompetitor={this.checkHotelCompetitor}
          modalTitle="Текущие конкуренты"
          btnClassName="input__button"
          consumers={consumers}
          stepCompetitors={stepCompetitors}
        />
        <OwHotelModal
          isOpen={isSettingsHotelOpen}
          closeModal={this.closeHotelSettingsModal}
          addOwnHotelInfoAction={this.addOwnHotelInfoAction}
          modalTitle="Мой отель"
          btnClassName="input__button"
          historyName={historyName}
          handleHistory={this.handleHistory}
          breakfasts={breakfasts}
          cancels={cancels}
          ownHotelInfo={ownHotelInfo}
          handleOwnHotelInfo={this.handleOwnHotelInfo}
          pmsCategories={pmsCategories}
          addHistoryLevel={this.addHistoryLevel}
          removeHistoryLevel={this.removeHistoryLevel}
        />
        <OtherSettingsModal
          isOpen={modalOtherSettings}
          closeModal={this.closeOtherSettings}
          modalTitle="Прочие настройки"
          btnClassName="input__button"
          price_edges={price_edges}
          currentPrice={currentPrice}
          otherSettings={otherSettings}
          handlePriceLevel={this.handlePriceLevel}
          addPriceLevel={this.addPriceLevel}
          removePriceLevel={this.removePriceLevel}
          addOtherSettings={this.addOtherSettings}
          handleOtherInfo={this.handleOtherInfo}
          excl_list={excl_list}
          removeExclData={this.removeExclData}
          addExclName={this.addExclName}
          exclName={exclName}
          handleExclName={this.handleExclName}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  consumers: getConsumersSelector(state),
  hotelOwnId: getHotelIdSelector(state),
  hotelOwnTitle: getCurrentHotelTitle(state),
  recommndationsFactory: getRecommndationsFactory(state)
});

const mapDispatchToProps = dispatch => ({
  addOwnHotelInfo: (data, recId, hotelOwnId, hotelOwnTitle, pmsCategories) =>
    dispatch(
      addOwnHotelInfo(data, recId, hotelOwnId, hotelOwnTitle, pmsCategories)
    ),
  addHotelCompetitorToJson: (data, recId, ri) =>
    dispatch(addHotelCompetitorToJson(data, recId, ri)),
  addOtherInfoRec: (data, recId) => dispatch(addOtherInfoRec(data, recId)),
  addCategoryPms: debounce(
    (data, recId) => dispatch(addCategoryPms(data, recId)),
    1000
  ),
  removeCategoryPmsFromStore: (id, recId) =>
    dispatch(removeCategoryPmsFromStore(id, recId))
});

export default connect(mapStateToProps, mapDispatchToProps)(MoreRecInfo);
