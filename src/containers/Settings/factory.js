import { getUniqId } from "../../utils";

export const recommendationFactory = ({ id, title }, selfID) => ({
  id,
  profile_name: "",
  profile_title: title,
  profile_group: "",
  current_category: "",
  excl_list: [],
  price_edges: {},
  parsing: {
    selfID,
    base_price: 0,
    concurents: [
      {
        [selfID]: {
          standart_category: "",
          hotel_title: "Мой отель",
          breakfast: [1],
          free_cancel: [1],
          person_capacity: [1]
        }
      }
    ]
  },
  categorys: [
    {
      standart_category: {
        id: getUniqId(),
        title: "",
        history: []
      }
    }
  ]
});

export const competitorFactory = (hotel_id, hotel_title) => {
  return {
    [hotel_id]: {
      hotel_id,
      standart_category: "",
      hotel_title,
      breakfast: [1],
      free_cancel: [1],
      person_capacity: [1]
    }
  };
};

export const categoryPms = pmsCategory => {
  const { title, id, history } = pmsCategory;
  return {
    standart_category: {
      id,
      title,
      history
    }
  };
};
