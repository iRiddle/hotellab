import React, { Component } from "react";
import Select from "react-select";
import { connect } from "react-redux";
import debounce from "lodash/debounce";
import isEqual from "lodash/isEqual";
import isEmpty from "lodash/isEmpty";

import { getRecommndationsFactory } from "../../../redux/selectors/jsonFormed";
import { getCompetitorsSelector } from "../../../redux/selectors/settings";
import { addHotelCompetitorInfoToJson } from "../../../redux/actions/jsonFormed";

import Input from "../../../components/ChildComponents/Input";

class CompetitorSettings extends Component {
  constructor(props) {
    super(props);

    const {
      currentCompetitorId,
      currentRecommendationId,
      currentIndex,
      competitorsSelector
    } = this.props;
    let room_titles = [];

    if (
      !isEmpty(
        competitorsSelector.filter(
          item => item.hotel_id === currentCompetitorId
        )[0]
      )
    ) {
      const competitor = competitorsSelector.filter(
        item => item.hotel_id === currentCompetitorId
      )[0];
      room_titles = competitor.room_titles;
    } else {
      room_titles = [];
    }

    const {
      standart_category,
      person_capacity,
      breakfast,
      free_cancel,
      hotel_title,
      hotel_id
    } = this.props.recommndationsFactory.filter(
      item => item.id === currentRecommendationId
    )[0].parsing.concurents[currentIndex + 1][currentCompetitorId];

    this.state = {
      competitorSettings: {
        hotel_id,
        standart_category,
        hotel_title,
        person_capacity,
        breakfast,
        free_cancel
      },
      breakfasts: [
        { id: 1, title: "Да", value: [1] },
        { id: 2, title: "Нет", value: [0] },
        { id: 3, title: "Да/нет", value: [0, 1] }
      ],
      cancels: [
        { id: 1, title: "Да", value: [1] },
        { id: 2, title: "Нет", value: [0] },
        { id: 3, title: "Да/нет", value: [0, 1] }
      ],
      room_titles: room_titles.map(item => ({ value: item, label: item }))
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.competitorSettings !== this.state.competitorSettings) {
      this.props.addHotelCompetitorInfoToJson(
        this.state.competitorSettings,
        this.props.currentCompetitorId,
        this.props.currentRecommendationId,
        this.props.currentIndex + 1
      );
    }
  }

  handleCompetitorInfo = (field, e) => {
    const value = e.target.value;
    const arraySetting = this.getSettingArray(field, value);
    this.setState(prevState => ({
      competitorSettings: {
        ...prevState.competitorSettings,
        [field]: arraySetting
      }
    }));
  };

  handlePMSInfo = selectedOption => {
    this.setState(prevState => ({
      competitorSettings: {
        ...prevState.competitorSettings,
        standart_category: selectedOption.value
      }
    }));
  };

  getSettingArray = (field, value) => {
    if (field === "breakfast" || field === "free_cancel") {
      switch (Number(value)) {
        case 0:
          return [0];
        case 1:
          return [1];
        default:
          return [0, 1];
      }
    }
    if (field === "person_capacity") {
      return [Number(value)];
    }
    return value;
  };

  render() {
    const { breakfasts, cancels, competitorSettings, room_titles } = this.state;
    const {
      person_capacity,
      standart_category,
      breakfast,
      free_cancel
    } = competitorSettings;
    return (
      <div className="competitors-settings__container">
        <div className="competitors-settings__pms-select">
          <span>Название в категории</span>
          <Select
            value={
              room_titles.filter(item => item.value === standart_category)[0]
            }
            options={room_titles}
            onChange={this.handlePMSInfo}
            placeholder="Выберите название в категории"
          />
        </div>
        <div className="competitors-settings__breakfast">
          <span className="label">Завтрак</span>
          {breakfasts.map(bf => (
            <label class="breakfast__item" key={bf.hotel_id}>
              {bf.title}
              <input
                type="radio"
                name="cbreakfast"
                value={bf.value}
                onChange={e => this.handleCompetitorInfo("breakfast", e)}
                checked={isEqual(breakfast, bf.value)}
              />
              <span class="breakfast__checkmark"></span>
            </label>
          ))}
        </div>
        <div className="competitors-settings__breakfast">
          <span className="label">Отмены</span>
          {cancels.map(bf => (
            <label class="breakfast__item" key={bf.hotel_id}>
              {bf.title}
              <input
                type="radio"
                name="ccancels"
                value={bf.value}
                onChange={e => this.handleCompetitorInfo("free_cancel", e)}
                checked={isEqual(free_cancel, bf.value)}
              />
              <span class="breakfast__checkmark"></span>
            </label>
          ))}
        </div>
        <Input
          label
          lableTitle="Вместимость"
          id="capc-input"
          type="number"
          className="own-hotel-info__input"
          max={100}
          min={1}
          value={person_capacity}
          onChange={e => this.handleCompetitorInfo("person_capacity", e)}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  recommndationsFactory: getRecommndationsFactory(state),
  competitorsSelector: getCompetitorsSelector(state)
});

const mapDispatchToProps = dispatch => ({
  addHotelCompetitorInfoToJson: debounce(
    (data, competitorId, recommndationId, currentIndex) =>
      dispatch(
        addHotelCompetitorInfoToJson(
          data,
          competitorId,
          recommndationId,
          currentIndex
        )
      ),
    1000
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(CompetitorSettings);
