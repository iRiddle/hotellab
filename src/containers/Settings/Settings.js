import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Container, Row, Col } from "reactstrap";
import { connect } from "react-redux";

import debounce from "lodash/debounce";

import Recommendations from "./Recommendations";
import Competitors from "./Competitors";

import {
  getCompetitors,
  getRecommendationsJson
} from "../../redux/actions/settings";

import {
  getCompetitorsSelector,
  getCompetitorsMaxDistanceSelector,
  getSettingsPending
} from "../../redux/selectors/settings";
import { getCurrentHotel } from "../../redux/selectors";

import "./style.css";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      stars: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }],
      sortedBy: "numCount",
      currentDistance: 0,
      starsE: []
    };
  }

  componentDidMount() {
    const {
      currentHotel,
      getCompetitorsDataPrimary,
      getRecommendationsJson
    } = this.props;
    getCompetitorsDataPrimary(currentHotel, "");
    getRecommendationsJson(currentHotel);
  }

  componentDidUpdate(prevProps, prevState) {
    const { query, currentDistance, starsE } = this.state;
    const {
      getCompetitorsData,
      currentHotel,
      getRecommendationsJson
    } = this.props;
    if (
      query !== prevState.query ||
      currentDistance !== prevState.currentDistance ||
      starsE !== prevState.starsE
    ) {
      getCompetitorsData(currentHotel, query, currentDistance, starsE);
    }
    if (prevProps.currentHotel !== currentHotel) {
      getCompetitorsData(currentHotel, "");
      getRecommendationsJson(currentHotel);
    }
  }

  onChangeRange = currentDistance => {
    this.setState({
      currentDistance
    });
  };

  onChangeQuery = text => {
    this.setState({
      query: text
    });
  };

  handleStars = id => {
    const { starsE } = this.state;
    if (starsE[0] && starsE[0] === id) {
      this.setState({
        starsE: []
      });
      return;
    }
    if (starsE.length === 2) {
      this.setState({
        starsE: [id]
      });
      return;
    }
    this.setState({
      starsE: [...starsE, id]
    });
  };

  render() {
    const { currentDistance, stars, query, starsE } = this.state;
    const { competitorsMaxDistance, competitors, settingsPending } = this.props;
    const { distance } = competitorsMaxDistance;
    return (
      <Container fluid>
        <Row>
          <Col xs="12" sm="12" lg="8">
            <Competitors
              currentDistance={currentDistance}
              query={query}
              stars={stars}
              starsE={starsE}
              distance={distance}
              competitors={competitors}
              onChangeRange={this.onChangeRange}
              handleStars={this.handleStars}
              onChangeQuery={this.onChangeQuery}
              settingsPending={settingsPending}
            />
          </Col>
          <Col xs="12" sm="12" lg="4">
            <Recommendations settingsPending={settingsPending} />
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  competitorsMaxDistance: getCompetitorsMaxDistanceSelector(state),
  competitors: getCompetitorsSelector(state),
  settingsPending: getSettingsPending(state),
  currentHotel: getCurrentHotel(state)
});

const mapDispatchToProps = dispatch => ({
  getCompetitorsData: debounce((hotelId, query, currentDistance, starsE) => {
    dispatch(getCompetitors(hotelId, query, currentDistance, starsE));
  }, 1000),
  getCompetitorsDataPrimary: (hotelId, query, currentDistance, starsE) => {
    dispatch(getCompetitors(hotelId, query, currentDistance, starsE));
  },
  getRecommendationsJson: hotelId => {
    dispatch(getRecommendationsJson(hotelId));
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Settings);
