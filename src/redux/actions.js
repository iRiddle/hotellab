import {
  SET_FILTER_YEAR_AGO_SALES_RATE,
  SET_FILTER_SOURCES_SALES_RATE,
  SET_SALES_RATE_DATE_RS,
  SET_SALES_RATE_DATE_RV,
  SET_FILTER_CATEGORY_SALES_RATE,
  SET_SALES_RATE,
  SELECT_HOTEL,
  SET_CALENDAR_FILTER_SELECTED_OPTIONS,
  SET_CALENDAR_DATA,
  SET_STATE,
  SET_CALENDAR_SELECTED_DATE,
  SET_CALENDAR_SELECTED_DATE_DATA,
  START_LOADIONG_SELECTED_DATE_DETAILS,
  SET_ARTBOARD
} from "./actionTypes";

export const SetFilterCategory = id => ({
  type: SET_FILTER_CATEGORY_SALES_RATE,
  payload: { id }
});

export const SetFilterYearAgo = id => ({
  type: SET_FILTER_YEAR_AGO_SALES_RATE,
  payload: { id }
});

export const SetFilterSources = id => ({
  type: SET_FILTER_SOURCES_SALES_RATE,
  payload: { id }
});

export const SetFilterDate = (getType, date) => ({
  type: getType,
  payload: { date }
});

export const selectHotel = hid => ({
  type: SELECT_HOTEL,
  payload: { hid }
});

export const setCalendarFilterSelectedOptions = (filterId, value) => ({
  type: SET_CALENDAR_FILTER_SELECTED_OPTIONS,
  payload: { filterId, value }
});

export const loadConfig = config => ({
  type: SET_STATE,
  payload: { config }
});

export const loadsalesRate = config => ({
  type: SET_SALES_RATE,
  payload: { config }
});

export const loadartboard = config => ({
  type: SET_ARTBOARD,
  payload: { config }
});

export const setCalendarData = data => ({
  type: SET_CALENDAR_DATA,
  payload: { data }
});

export const setCalendarSelectedDate = (y, m, d) => ({
  type: SET_CALENDAR_SELECTED_DATE,
  payload: { y, m, d }
});

export const setSelectedDateData = data => ({
  type: SET_CALENDAR_SELECTED_DATE_DATA,
  payload: { data }
});
export const startLoadingSelectedDateDetails = () => ({
  type: START_LOADIONG_SELECTED_DATE_DETAILS,
  payload: {}
});

export function fetchInitialStateArtboard(nds, breakfast, currentTitle) {
  return (dispatch, getState) => {
    let state = getState();
    if (!state.hotels.currentHotel) {
      var hotel_id = false;
    } else {
      var hotel_id = state.hotels.currentHotel;
    }
    let req = {
      hotel_id: hotel_id,
      nds: nds === undefined ? false : !!nds,
      breakfast: breakfast === undefined ? false : !!breakfast,
      budget: currentTitle === 0 || currentTitle === undefined ? false : true
    };

    fetch("/privat/api/dashboard_data", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(req)
    })
      .then(function(response) {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            response.json().then(onServerSpecResponse);
            throw new Error("Response status: " + response.status);
          }
        } else {
          throw new TypeError("Oops, we haven't got JSON!");
        }
      })
      .then(function(json) {
        dispatch(loadartboard(json));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
}

export function fetchSelectedDateDetails() {
  return (dispatch, getState) => {
    dispatch(startLoadingSelectedDateDetails());
    let state = getState();
    let req = extractReqBase(state);

    req.year = state.calendar.selectedDate.y;
    req.month = state.calendar.selectedDate.m;
    req.day = state.calendar.selectedDate.d;

    fetch("/privat/api/details", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(req)
    })
      .then(function(response) {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            response.json().then(onServerSpecResponse);
            throw new Error("Response status: " + response.status);
          }
        } else {
          throw new TypeError("Oops, we haven't got JSON!");
        }
      })
      .then(function(json) {
        dispatch(setSelectedDateData(json));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
}

export function fetchInitialState() {
  return dispatch => {
    fetch("/privat/api/ajax", {
      method: "GET"
    })
      .then(function(response) {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            response.json().then(onServerSpecResponse);
            throw new Error("Response status: " + response.status);
          }
        } else {
          throw new TypeError("Oops, we haven't got JSON!");
        }
      })
      .then(function(json) {
        dispatch(loadConfig(json));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
}

const extractReqBase = state => {
  let filters = {};
  for (var fid in state.calendar.filters) {
    if (
      state.calendar.filters[fid].selected &&
      state.calendar.filters[fid].selected.length > 0
    )
      filters[fid] = state.calendar.filters[fid].selected;
  }
  let req = {
    filters,
    hotelId: state.calendar.currentHotel
  };
  return req;
};

export function fetchCalendarData() {
  return (dispatch, getState) => {
    let state = getState();
    let req = extractReqBase(state);

    fetch("/privat/api/ajax", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(req)
    })
      .then(function(response) {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            response.json().then(onServerSpecResponse);
            throw new Error("Response status: " + response.status);
          }
        } else {
          throw new TypeError("Oops, we haven't got JSON!");
        }
      })
      .then(function(json) {
        // console.log('json', json);
        dispatch(setCalendarData(json));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
}

export function fetchInitialStateSalesRate(nds, breakfast) {
  return (dispatch, getState) => {
    let state = getState();

    if (!state.hotels.currentHotel) {
      var hotel_id = false;
    } else {
      var hotel_id = state.hotels.currentHotel;
    }

    let req = {
      hotel_id: hotel_id,
      categories: state.salesRate.filterActiveCategory,
      sources: state.salesRate.filterActiveSources,
      rsStart: state.salesRate.filterActiveDate.rs.dateStart,
      rsEnd: state.salesRate.filterActiveDate.rs.dateEnd,
      rvStart: state.salesRate.filterActiveDate.rv.dateStart,
      rvEnd: state.salesRate.filterActiveDate.rv.dateEnd,
      year_ago: state.salesRate.filterActiveYear_ago || false,
      nds: nds === undefined ? false : !!nds,
      breakfast: breakfast === undefined ? false : !!breakfast
    };

    fetch("/privat/api/dynamics", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(req)
    })
      .then(function(response) {
        var contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          } else {
            response.json().then(onServerSpecResponse);
            throw new Error("Response status: " + response.status);
          }
        } else {
          throw new TypeError("Oops, we haven't got JSON!");
        }
      })
      .then(function(json) {
        dispatch(loadsalesRate(json));
        // console.log("Приход с бэка: ", json);
        // console.log("Отправляем на бэк: ", req);
      })
      .catch(function(error) {
        console.log(error);
      });
  };
}

function onServerSpecResponse(json) {
  switch (json.action) {
    case "redirect":
      if (json.url) window.location = json.url;
      break;
    default:
      console.log(json);
      break;
  }
}

//  function setBook(data) {
//   return { type: 'SET_BOOK', data: data };
//  }
