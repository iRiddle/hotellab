import { createSelector } from "reselect";
import get from "lodash/get";

const getMarketData = state =>
  get(state, "calendar.selectedDateDetails.market", []);

export const getMarketAllStars = createSelector(getMarketData, gmd => gmd);

export const getStarsCount = createSelector(getMarketData, gmd =>
  gmd
    .map(item => item.slice(1)[0])
    .filter(item => item !== 0)
    .slice(0)
    .reduce(
      (accum, current, index, arr) => {
        if (index === 0) arr.splice(1);
        return {
          star_none: accum.star_none + current.star_none,
          star_2: accum.star_2 + current.star_2,
          star_3: accum.star_3 + current.star_3,
          star_4: accum.star_4 + current.star_4,
          star_5: accum.star_5 + current.star_5
        };
      },
      {
        star_none: 0,
        star_2: 0,
        star_3: 0,
        star_4: 0,
        star_5: 0
      }
    )
);
