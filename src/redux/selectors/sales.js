import { createSelector } from "reselect";
const get = require("lodash/get");
const isArray = require("lodash/isArray");
const salesRate = store => store.salesRate;
export const getCurrentFactArray = store => store.salesRate.list.data;
export const getComparedSalesPace = store =>
  store.comparedSalesPace.comparedSalesPace;

export const getCurrentFactArraySelector = createSelector(
  getCurrentFactArray,
  gcfa => get(gcfa, "dyn", [])
);

export const getComparedSalesPaceSelector = createSelector(
  getComparedSalesPace,
  gcsp => get(gcsp, "difference", [])
);

export const getComparedDynSelector = createSelector(
  getComparedSalesPace,
  gcsp => get(gcsp, "data.dyn", [])
);

export const getFilterActiveCategory = createSelector(salesRate, sr =>
  get(sr, "filterActiveCategory", [])
);

export const getFilterActiveSources = createSelector(salesRate, sr =>
  get(sr, "filterActiveSources", [])
);

export const getDifferenceFromStore = createSelector(
  getComparedSalesPaceSelector,
  getComparedDynSelector,
  gcf => {
    let getCombinedDifferenceData = [];
    if (!isArray(gcf)) {
      const objValues = Object.values(gcf);
      getCombinedDifferenceData = objValues.map(item => {
        return {
          ...item,
          isDifference: true
        };
      });

      return getCombinedDifferenceData;
    }
    getCombinedDifferenceData = gcf.map(item => {
      return {
        ...item,
        isDifference: true
      };
    });

    return getCombinedDifferenceData;
  }
);
