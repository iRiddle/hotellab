import { createSelector } from "reselect";
const get = require("lodash/get");

const getBookingDepth = store => get(store, "bookingDepth.bookingDepth", {});

export const getBookingDepthChartSelector = createSelector(
  getBookingDepth,
  gbd => get(gbd, "booking_depth", {})
);

export const getMainMetricsSelector = createSelector(
  getBookingDepth,
  gbd => get(gbd, "main_metrics", {})
);

export const getFiltersSelector = createSelector(
  getBookingDepth,
  gbd => get(gbd, "filters", {})
);
