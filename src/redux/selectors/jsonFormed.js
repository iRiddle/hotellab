import { createSelector } from "reselect";

const get = require("lodash/get");

const getConsumers = state => get(state, "jsonFormed.consumers", []);
const getSettings = state => get(state, "settings.settings", {});
const getFactorySettings = state =>
  get(state, "jsonFormed.factorySettings", {});

const getRecommendations = state =>
  get(state, "jsonFormed.factorySettings", []);

export const getHotelIdSelector = state =>
  get(state, "jsonFormed.ownIdFromDB", "");

export const getCompetitorsSelector = createSelector(getSettings, gs =>
  get(gs, "competitors", [])
);

export const getConsumersSelector = createSelector(
  getConsumers,
  getCompetitorsSelector,
  (gc, gcs) => gcs.filter(gcItem => gc.includes(gcItem.hotel_id))
);

export const getRecommndationsFactory = createSelector(
  getRecommendations,
  gr => gr
);

export const getAllFactorySettingsSelector = createSelector(
  getFactorySettings,
  gfs => gfs
);

export const getRecommendationsAfterQuery = createSelector(
  getRecommendations,
  gr => gr.map(item => ({ id: item.id, title: item.profile_title }))
);
