import { createSelector } from "reselect";

const get = require("lodash/get");
const maxBy = require("lodash/maxBy");

const getSettings = state => get(state, "settings.settings", {});
export const getSettingsPending = state =>
  get(state, "settings.isFetching", false);
// const getSelfId = state => state.self_id;

export const getCompetitorsSelector = createSelector(getSettings, gs =>
  get(gs, "competitors", [])
);

export const getCompetitorsMaxDistanceSelector = createSelector(
  getCompetitorsSelector,
  gcs => {
    const maxByDistance = maxBy(gcs, item => item.distance);
    return maxByDistance || {};
  }
);
