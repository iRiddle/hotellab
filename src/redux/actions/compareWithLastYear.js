export const COMPARE_WITH_LAST_YEAR_PENDING =
  "COMPARE_WITH_LAST_YEAR::COMPARE_WITH_LAST_YEAR_PENDING";
export const COMPARE_WITH_LAST_YEAR_REJECTED =
  "COMPARE_WITH_LAST_YEAR::COMPARE_WITH_LAST_YEAR_REJECTED";
export const COMPARE_WITH_LAST_YEAR_FULFILLED =
  "COMPARE_WITH_LAST_YEAR::COMPARE_WITH_LAST_YEAR_FULFILLED";

const compareWithLastYearPending = () => ({
  type: COMPARE_WITH_LAST_YEAR_PENDING
});

const compareWithLastYearFulfilled = data => ({
  type: COMPARE_WITH_LAST_YEAR_FULFILLED,
  payload: data
});

const compareWithLastYearRejected = error => ({
  type: COMPARE_WITH_LAST_YEAR_REJECTED,
  payload: error
});

export const compareWithLastYear = (
  hotel,
  periodStart,
  periodEnd,
  periodStartLive,
  periodEndLive,
  year_ago,
  byPeriod,
  callback
) => dispatch => {
  dispatch(compareWithLastYearPending());
  const by_period = byPeriod === "dayWeek" ? "weekdays" : byPeriod;

  const req = {
    hotel_id: hotel,
    rsStart: periodStartLive,
    rsEnd: periodEndLive,
    rvStart: periodStart,
    rvEnd: periodEnd,
    year_ago,
    by_period
  };
  fetch("/privat/api/dynamics", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(req)
  })
    .then(response => {
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.includes("application/json")) {
        if (response.status >= 200 && response.status < 300) {
          return response.json();
        } else {
          dispatch(compareWithLastYearRejected(response.status));
        }
      } else {
        throw new TypeError("Oops, we haven't got JSON!");
      }
    })
    .then(data => {
      dispatch(compareWithLastYearFulfilled(data));
      callback(data.difference);
    });
};
