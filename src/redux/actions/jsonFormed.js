export const BUILD__CONSUMERS = "SETTINGS::BUILD_CONSUMERS";
export const FILTER__CONSUMERS = "SETTINGS::FILTER_CONSUMERS";

export const ADD__RECOMMENDATION__TO__STORE =
  "SETTINGS::ADD__RECOMMENDATION__TO__STORE";
export const REMOVE__RECOMMENDATION__FROM__STORE =
  "SETTINGS::REMOVE__RECOMMENDATION__FROM__STORE";

export const ADD__OWN__HOTEL__INFO = "SETTINGS::ADD__OWN__HOTEL__INFO";

export const ADD_COMPETITOR_TO_REC = "SETTINGS::ADD_COMPETITOR_TO_REC";

export const ADD_COMPETITOR_INFO_REC = "SETTINGS::ADD_COMPETITOR_INFO_REC";

export const ADD_OTHER_INFO_REC = "SETTINGS::ADD_OTHER_INFO_REC";

export const ADD_CATEGORY_PMS_REC = "SETTINGS::ADD_CATEGORY_PMS_REC";
export const REMOVE_CATEGORY_PMS_REC = "SETTINGS::REMOVE_CATEGORY_PMS_REC";
////////////////////////////////////////////////

export const addRecommendationToStore = data => ({
  type: ADD__RECOMMENDATION__TO__STORE,
  data
});

export const deleteRecommendationFromStore = id => ({
  type: REMOVE__RECOMMENDATION__FROM__STORE,
  id
});

/////////////////////////////////////////////

export const buildConsumers = data => ({
  type: BUILD__CONSUMERS,
  data
});

export const filterConsumers = id => ({
  type: FILTER__CONSUMERS,
  id
});

///////////////////////////////////////////

export const addOwnHotelInfo = (
  data,
  id,
  ownId,
  hotelOwnTitle,
  pmsCategories
) => ({
  type: ADD__OWN__HOTEL__INFO,
  data,
  id,
  ownId,
  hotelOwnTitle,
  pmsCategories
});

export const addHotelCompetitorToJson = (data, recId, ri) => ({
  type: ADD_COMPETITOR_TO_REC,
  data,
  recId,
  ri
});

/////////////////////////////////////////////

export const addHotelCompetitorInfoToJson = (data, comId, recId, index) => ({
  type: ADD_COMPETITOR_INFO_REC,
  data,
  comId,
  recId,
  index
});

////////////////////////////////////////////

export const addOtherInfoRec = (data, recId) => ({
  type: ADD_OTHER_INFO_REC,
  data,
  recId
});

////////////////////////////////////////////

export const addCategoryPms = (data, recId) => ({
  type: ADD_CATEGORY_PMS_REC,
  data,
  recId
});

export const removeCategoryPmsFromStore = (id, recId) => ({
  type: REMOVE_CATEGORY_PMS_REC,
  id,
  recId
});
