import { determineStarsNumber } from "../../utils";

export const GET_COMPETITORS_PENDING =
  "SETTINGS-COMPETITORS::GET_COMPETITORS_PENDING";
export const GET_COMPETITORS_FULFILLED =
  "SETTINGS-COMPETITORS::GET_COMPETITORS_FULFILLED";
export const GET_COMPETITORS_REJECTED =
  "SETTINGS-COMPETITORS::GET_COMPETITORS_REJECTED";
export const SORT_COMPETITORS_BY = "SETTINGS-COMPETITORS::SORT_COMPETITORS_BY";

export const GET_RECOMMENDATIONS_JSON_PENDING =
  "RECOMMENDATIONS_JSON::GET_RECOMMENDATIONS_JSON_PENDING";
export const GET_RECOMMENDATIONS_JSON_FULFILLED =
  "RECOMMENDATIONS_JSON::GET_RECOMMENDATIONS_JSON_FULFILLED";
export const GET_RECOMMENDATIONS_JSON_REJECTED =
  "RECOMMENDATIONS_JSON::GET_RECOMMENDATIONS_JSON_REJECTED";

const getCompetitorsPending = () => ({
  type: GET_COMPETITORS_PENDING
});

const getCompetitorsFulfilled = data => ({
  type: GET_COMPETITORS_FULFILLED,
  data
});

const getCompetitorsRejected = error => ({
  type: GET_COMPETITORS_REJECTED,
  error
});

const getRecommendationsJsonPending = () => ({
  type: GET_RECOMMENDATIONS_JSON_PENDING
});

const getRecommendationsJsonFulfilled = data => ({
  type: GET_RECOMMENDATIONS_JSON_FULFILLED,
  data
});

const getRecommendationsJsonRejected = error => ({
  type: GET_RECOMMENDATIONS_JSON_REJECTED,
  error
});

export const getCompetitors = (
  hotel_id,
  query,
  distance,
  starsE
) => dispatch => {
  dispatch(getCompetitorsPending());

  const req = {
    hotel_id,
    query,
    distance: distance !== undefined && distance[0],
    stars: determineStarsNumber(starsE)
  };

  fetch("/privat/api/get_competitors", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(req)
  })
    .then(response => {
      console.log(response);
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.includes("application/json")) {
        if (response.status >= 200 && response.status < 300) {
          return response.json();
        } else {
          dispatch(getCompetitorsRejected(response.status));
        }
      } else {
        console.log("Oops, we haven't got JSON!");
      }
    })
    .then(data => {
      dispatch(getCompetitorsFulfilled(data));
    });
};

export const sendDataToServer = (data, callback) => {
  return dispatch => {
    fetch("/privat/api/add_json", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => {
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.includes("application/json")) {
        if (response.status >= 200 && response.status < 300) {
          callback("Данные успешно отправлены на сервер!");
        } else {
          callback(
            "Что-то пошло не так. Мы усердно стараемся исправить ошибку и в скором времени все будет готово!"
          );
        }
      }
    });
  };
};

export const getRecommendationsJson = hotelId => dispatch => {
  dispatch(getRecommendationsJsonPending());

  fetch(`/privat/api/get_json/${hotelId}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      console.log(response);
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.includes("application/json")) {
        if (response.status >= 200 && response.status < 300) {
          return response.json();
        } else {
          dispatch(getRecommendationsJsonRejected(response.status));
        }
      } else {
        console.log("Oops, we haven't got JSON!");
      }
    })
    .then(data => {
      dispatch(getRecommendationsJsonFulfilled(data));
    });
};

export const sortCompetitorsBy = sortBy => ({
  type: SORT_COMPETITORS_BY,
  sortBy
});
