export const GET_BOOKING_DEPTH_PENDING =
  "BOOKING_DEPTH::GET_BOOKING_DEPTH_PENDING";
export const GET_BOOKING_DEPTH_REJECTED =
  "BOOKING_DEPTH::GET_BOOKING_DEPTH_REJECTED";
export const GET_BOOKING_DEPTH_FULFILLED =
  "BOOKING_DEPTH::GET_BOOKING_DEPTH_FULFILLED";

const getBookingDepthPending = () => ({
  type: GET_BOOKING_DEPTH_PENDING
});

const getBookingDepthFulfilled = data => ({
  type: GET_BOOKING_DEPTH_FULFILLED,
  payload: data
});

const getBookingDepthRejected = error => ({
  type: GET_BOOKING_DEPTH_REJECTED,
  payload: error
});

export const getBookingDepth = (
  hotelId,
  sources,
  roomTypes,
  nds,
  breakfast,
  start_date,
  end_date
) => dispatch => {
  dispatch(getBookingDepthPending());
  const req = {
    hotel_id: hotelId,
    sources,
    categories: roomTypes,
    nds: nds === undefined ? true : !!nds,
    breakfast: breakfast === undefined ? true : !!breakfast,
    start_date,
    end_date
  };
  fetch("/privat/api/booking_depth", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(req)
  })
    .then(response => {
      const contentType = response.headers.get("content-type");
      if (contentType && contentType.includes("application/json")) {
        if (response.status >= 200 && response.status < 300) {
          return response.json();
        } else {
          dispatch(getBookingDepthRejected(response.status));
        }
      } else {
        throw new TypeError("Oops, we haven't got JSON!");
      }
    })
    .then(data => {
      dispatch(getBookingDepthFulfilled(data));
    });
};
