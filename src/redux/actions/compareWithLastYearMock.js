const dyn = [
  {
    ADR: 8168,
    date: "23.06.2019",
    kn: 1,
    profit: 8168,
    difference: 4,
    equality: 1
  },
  {
    ADR: 6834,
    date: "24.06.2019",
    kn: 2,
    profit: 13668,
    difference: 1,
    equality: -1
  },
  {
    ADR: 5943,
    date: "25.06.2019",
    kn: 3,
    profit: 17829,
    difference: 3,
    equality: 0
  },
  {
    ADR: 6062,
    date: "26.06.2019",
    kn: 3,
    profit: 18186,
    difference: 20,
    equality: 1
  },
  {
    ADR: 6052.5,
    date: "27.06.2019",
    kn: 4,
    profit: 24210,
    difference: 12,
    equality: -1
  },
  {
    ADR: 6683,
    date: "28.06.2019",
    kn: 3,
    profit: 20049,
    difference: 5,
    equality: 1
  },
  {
    ADR: 5762,
    date: "29.06.2019",
    kn: 2,
    profit: 11524,
    difference: 4,
    equality: 1
  },
  {
    ADR: 5762,
    date: "30.06.2019",
    kn: 2,
    profit: 11524,
    difference: 8,
    equality: -1
  },
  {
    ADR: 6024,
    date: "01.07.2019",
    kn: 1,
    profit: 6024,
    difference: 9,
    equality: -1
  },
  { ADR: 0, date: "02.07.2019", kn: 0, profit: 0, difference: 1, equality: 1 },
  { ADR: 0, date: "03.07.2019", kn: 0, profit: 0, difference: 22, equality: 1 },
  { ADR: 0, date: "04.07.2019", kn: 0, profit: 0, difference: 13, equality: 0 },
  { ADR: 0, date: "05.07.2019", kn: 0, profit: 0, difference: 42, equality: 0 },
  { ADR: 0, date: "06.07.2019", kn: 0, profit: 0, difference: 1, equality: -1 },
  { ADR: 0, date: "07.07.2019", kn: 0, profit: 0, difference: 4, equality: 1 },
  { ADR: 0, date: "08.07.2019", kn: 0, profit: 0, difference: 54, equality: 1 },
  { ADR: 0, date: "09.07.2019", kn: 0, profit: 0, difference: 2, equality: 0 },
  { ADR: 0, date: "10.07.2019", kn: 0, profit: 0, difference: 5, equality: 1 },
  { ADR: 0, date: "11.07.2019", kn: 0, profit: 0, difference: 64, equality: 1 },
  {
    ADR: 0,
    date: "12.07.2019",
    kn: 0,
    profit: 0,
    difference: 12,
    equality: -1
  },
  { ADR: 0, date: "13.07.2019", kn: 0, profit: 0, difference: 8, equality: 1 },
  { ADR: 0, date: "14.07.2019", kn: 0, profit: 0, difference: 9, equality: 1 },
  { ADR: 0, date: "15.07.2019", kn: 0, profit: 0, difference: 7, equality: 1 },
  {
    ADR: 5250,
    date: "16.07.2019",
    kn: 1,
    profit: 5250,
    difference: 45,
    equality: 1
  },
  {
    ADR: 5250,
    date: "17.07.2019",
    kn: 1,
    profit: 5250,
    difference: 6,
    equality: 1
  },
  {
    ADR: 5250,
    date: "18.07.2019",
    kn: 1,
    profit: 5250,
    difference: 43,
    equality: 1
  },
  {
    ADR: 5250,
    date: "19.07.2019",
    kn: 1,
    profit: 5250,
    difference: 54,
    equality: 0
  },
  {
    ADR: 5250,
    date: "20.07.2019",
    kn: 1,
    profit: 5250,
    difference: 23,
    equality: -1
  },
  {
    ADR: 5250,
    date: "21.07.2019",
    kn: 1,
    profit: 5250,
    difference: 67,
    equality: 1
  },
  {
    ADR: 5250,
    date: "22.07.2019",
    kn: 1,
    profit: 5250,
    difference: 12,
    equality: 1
  },
  {
    ADR: 5250,
    date: "23.07.2019",
    kn: 1,
    profit: 5250,
    difference: 6,
    equality: -1
  },
  { ADR: 0, date: "24.07.2019", kn: 0, profit: 0, difference: 23, equality: 0 },
  { ADR: 0, date: "25.07.2019", kn: 0, profit: 0, difference: 67, equality: 1 },
  { ADR: 0, date: "26.07.2019", kn: 0, profit: 0, difference: 98, equality: 1 },
  { ADR: 0, date: "27.07.2019", kn: 0, profit: 0, difference: 32, equality: 1 },
  {
    ADR: 0,
    date: "28.07.2019",
    kn: 0,
    profit: 0,
    difference: 34,
    equality: -1
  },
  { ADR: 0, date: "29.07.2019", kn: 0, profit: 0, difference: 12, equality: 1 },
  { ADR: 0, date: "30.07.2019", kn: 0, profit: 0, difference: 1, equality: 1 },
  { ADR: 0, date: "31.07.2019", kn: 0, profit: 0, difference: 2, equality: 1 },
  { ADR: 0, date: "01.08.2019", kn: 0, profit: 0, difference: 8, equality: -1 },
  { ADR: 0, date: "02.08.2019", kn: 0, profit: 0, difference: 23, equality: 1 },
  { ADR: 0, date: "03.08.2019", kn: 0, profit: 0, difference: 5, equality: 1 },
  { ADR: 0, date: "04.08.2019", kn: 0, profit: 0, difference: 43, equality: 1 },
  { ADR: 0, date: "05.08.2019", kn: 0, profit: 0, difference: 12, equality: 0 },
  { ADR: 0, date: "06.08.2019", kn: 0, profit: 0, difference: 2, equality: 1 },
  { ADR: 0, date: "07.08.2019", kn: 0, profit: 0, difference: 5, equality: 0 },
  {
    ADR: 6000,
    date: "08.08.2019",
    kn: 1,
    profit: 6000,
    difference: 23,
    equality: 0
  },
  {
    ADR: 6000,
    date: "09.08.2019",
    kn: 1,
    profit: 6000,
    difference: 63,
    equality: 0
  },
  {
    ADR: 6000,
    date: "10.08.2019",
    kn: 1,
    profit: 6000,
    difference: 2,
    equality: 1
  },
  {
    ADR: 6000,
    date: "11.08.2019",
    kn: 1,
    profit: 6000,
    difference: 5,
    equality: 1
  },
  {
    ADR: 6000,
    date: "12.08.2019",
    kn: 1,
    profit: 6000,
    difference: 6,
    equality: -1
  },
  {
    ADR: 6000,
    date: "13.08.2019",
    kn: 1,
    profit: 6000,
    difference: 2,
    equality: 1
  },
  {
    ADR: 6000,
    date: "14.08.2019",
    kn: 1,
    profit: 6000,
    difference: 4,
    equality: 1
  },
  {
    ADR: 6000,
    date: "15.08.2019",
    kn: 1,
    profit: 6000,
    difference: 6,
    equality: 1
  },
  {
    ADR: 6000,
    date: "16.08.2019",
    kn: 1,
    profit: 6000,
    difference: 7,
    equality: 1
  },
  {
    ADR: 6000,
    date: "17.08.2019",
    kn: 1,
    profit: 6000,
    difference: 21,
    equality: 1
  },
  { ADR: 0, date: "18.08.2019", kn: 0, profit: 0, difference: 4, equality: 1 },
  {
    ADR: 6000,
    date: "19.08.2019",
    kn: 1,
    profit: 6000,
    difference: 4,
    equality: 1
  },
  {
    ADR: 6000,
    date: "20.08.2019",
    kn: 1,
    profit: 6000,
    difference: 4,
    equality: -1
  },
  {
    ADR: 6000,
    date: "21.08.2019",
    kn: 1,
    profit: 6000,
    difference: 4,
    equality: 1
  }
];
