import { createSelector } from "reselect";

const get = require("lodash/get");

export const getHotelsState = store => get(store, "hotels.list", []);
export const getSalesRateData = store => get(store, "salesRate", {}); //here

export const getCurrentHotel = store => store.hotels.currentHotel;
export const getCurrentHotelTitle = createSelector(
  getHotelsState,
  getCurrentHotel,
  (ghs, gch) => ghs[gch].title
);

export const getCurrentDate = store => store.salesRate.filterActiveDate.rv;
export const getCurrentDateLiving = store =>
  store.salesRate.filterActiveDate.rs;

export const totalRooms = store => store.salesRate.totalRooms;
export const getLastUpdateDate = store => store.salesRate.updated;

export const getHotels = store => {
  const hotelsState = getHotelsState(store);
  let hotels = {};
  for (var id in hotelsState) {
    hotels[id] = hotelsState[id].title;
  }
  return hotels;
};

export const getHotelsIds = store => {
  const hotelsState = getHotelsState(store);
  return Object.keys(hotelsState);
};

export const getCalendarFilters = store => store.calendar.filters;

export const getCalendarData = store => get(store, "calendar.data", {});

export const getArtboardData = store => get(store, "artboard.config", {});

export const getCurrencySymbolSelector = createSelector(
  getArtboardData,
  gad => gad.currency
);

// sales rate data

export const getUpdateDateSelector = createSelector(
  getSalesRateData,
  gsrd => gsrd.updated
);

export const getListDataFactSelector = createSelector(getSalesRateData, gsrd =>
  get(gsrd, "list.data.fact", [])
);

export const getListDataTotalFactSelector = createSelector(
  getSalesRateData,
  gsrd => get(gsrd, "list.dataTotal.fact", {})
);

export const getListDataDynSelector = createSelector(getSalesRateData, gsrd =>
  get(gsrd, "list.data.dyn", [])
);

export const getListDataTotalDynSelector = createSelector(
  getSalesRateData,
  gsrd => get(gsrd, "list.dataTotal.dyn", {})
);

export const getListFiltersCategorySelector = createSelector(
  getSalesRateData,
  gsrd => get(gsrd, "list.filters.category", [])
);

export const getListFiltersSourcesSelector = createSelector(
  getSalesRateData,
  gsrd => get(gsrd, "list.filters.sources", [])
);

export const getFilterActiveDateSelector = createSelector(
  getSalesRateData,
  gsrd => get(gsrd, "filterActiveDate", {})
);

export const getFilterActiveYearAgoSelector = createSelector(
  getSalesRateData,
  gsrd => get(gsrd, "filterActiveYear_ago", false)
);

export const getCopyPluginSelector = createSelector(getSalesRateData, gsrd =>
  get(gsrd, "copy", {})
);
