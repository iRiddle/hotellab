import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import rootReducer from "./reducers";

import { composeWithDevTools } from 'redux-devtools-extension';


export default createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(thunk),
        // other store enhancers if any
    )
);


// export default createStore(
//     rootReducer,
//     applyMiddleware(thunk),
//     devToolsEnhancer(
//         // Specify name here, actionsBlacklist, actionsCreators and other options if needed
//         {
//           actionsBlacklist: []
//         }
//     )
// );
