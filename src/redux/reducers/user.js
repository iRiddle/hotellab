// import { SELECT_HOTEL } from "../actionTypes";
import _config from '../../Config'
import { SET_STATE } from '../actionTypes';

let config = _config

function getInitialState(config) {
  if(!config) return {
    loading: true
  }
  return {...config.user}
}

const initialState = getInitialState(config)

const hotels = (state = initialState, action) => {
  switch (action.type) {
    case SET_STATE: {
      let newState = getInitialState(action.payload.config)
      if(newState)
        return newState
      else return state
    }
    default: {
      return state;
    }
  }
};

export default hotels;
