import { SELECT_HOTEL, SET_STATE } from "../actionTypes";
import _config from '../../Config'

let config = _config||{}

// const initialState = {
//   list: config.hotels||{}
// };

function getInitialState(config) {
  let state = {
    list: config.hotels||{}
  }

  let updated = '11.01.2018'

  // const currentHotel = config.currentHotel||Object.keys(config.hotels||{})[0]
  // if(config.calendarUpdateDate){
  //   let d = new Date(config.calendarUpdateDate);
  //   updated = d.getDate()+'.'+d.getMonth()+'.'+d.getFullYear()
  // }
  // if(config.hotels && config.hotels[currentHotel] && config.hotels[currentHotel].last_forecast){ // TODO !!!
  //   updated = config.hotels[currentHotel].last_forecast;
  // }

  // state.updated = updated

  state.currentHotel = config.currentHotel||Object.keys(state.list)[0]

  return state
}


// initialState.currentHotel = config.currentHotel||Object.keys(initialState.list)[0]
const initialState = getInitialState(config)

const hotels = (state = initialState, action) => {
  switch (action.type) {
    case SET_STATE: {
      let newState = getInitialState(action.payload.config)
      if(newState)
        return newState
      else return state
    }
    case SELECT_HOTEL: {
      return {...state, currentHotel: action.payload.hid};
    }
    default: {
      return state;
    }
  }
};

export default hotels;
