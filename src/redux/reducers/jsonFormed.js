import { combineReducers } from "redux";

import {
  BUILD__CONSUMERS,
  FILTER__CONSUMERS,
  ADD__RECOMMENDATION__TO__STORE,
  REMOVE__RECOMMENDATION__FROM__STORE,
  ADD__OWN__HOTEL__INFO,
  ADD_COMPETITOR_TO_REC,
  ADD_COMPETITOR_INFO_REC,
  ADD_OTHER_INFO_REC,
  ADD_CATEGORY_PMS_REC
} from "../actions/jsonFormed";

import {
  GET_RECOMMENDATIONS_JSON_PENDING,
  GET_RECOMMENDATIONS_JSON_FULFILLED,
  GET_RECOMMENDATIONS_JSON_REJECTED
} from "../actions/settings";

import { getUniqId } from "../../utils";

const isFetching = (state = false, action) => {
  switch (action.type) {
    case GET_RECOMMENDATIONS_JSON_REJECTED:
    case GET_RECOMMENDATIONS_JSON_FULFILLED:
      return false;
    case GET_RECOMMENDATIONS_JSON_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = "", action) => {
  switch (action.type) {
    case GET_RECOMMENDATIONS_JSON_REJECTED:
      return action.error;
    default:
      return state;
  }
};

const consumers = (state = [], action) => {
  switch (action.type) {
    case BUILD__CONSUMERS:
      return [...state, action.data];
    case FILTER__CONSUMERS:
      return state.filter(item => item !== action.id);
    default:
      return state;
  }
};

const factorySettings = (state = [], action) => {
  switch (action.type) {
    case GET_RECOMMENDATIONS_JSON_FULFILLED:
      const { json } = action.data;
      return [
        ...json.map(item => ({
          id: getUniqId(),
          ...item,
          parsing: {
            ...item.parsing,
            concurents: item.parsing.concurents.map((item, index) => {
              if (index !== 0) {
                const key = Object.keys(item);
                return { [key]: { ...item[key], hotel_id: key[0] } };
              }
              return { ...item };
            })
          }
        }))
      ];
    case ADD__RECOMMENDATION__TO__STORE:
      return [...state, action.data];
    case REMOVE__RECOMMENDATION__FROM__STORE:
      return state.filter(item => item.id !== action.id);
    case ADD__OWN__HOTEL__INFO: {
      const {
        base_price,
        standart_category,
        person_capacity,
        breakfast,
        free_cancel
      } = action.data;
      const { pmsCategories } = action;
      return state.map(item =>
        item.id === action.id
          ? {
              ...item,
              categorys: pmsCategories,
              parsing: {
                ...item.parsing,
                base_price: Number(base_price),
                concurents: [
                  {
                    [action.ownId]: {
                      standart_category:
                        Array.isArray(standart_category) === true
                          ? standart_category[0]
                          : standart_category,
                      hotel_title: "Мой отель",
                      person_capacity: [Number(person_capacity)],
                      breakfast,
                      free_cancel
                    }
                  },
                  ...item.parsing.concurents.slice(1)
                ]
              }
            }
          : { ...item }
      );
    }
    case ADD_COMPETITOR_TO_REC: {
      const { data, recId, ri } = action;
      return state.map(item =>
        item.id === recId
          ? {
              ...item,
              parsing: {
                ...item.parsing,
                // fix this piece of obscenity
                concurents:
                  ri === -1
                    ? [
                        ...item.parsing.concurents.slice(0, 1),
                        ...item.parsing.concurents.slice(1),
                        ...data
                      ]
                    : [
                        ...item.parsing.concurents.slice(0, 1),
                        ...item.parsing.concurents.slice(
                          1,
                          item.parsing.concurents.length - ri
                        ),
                        ...data
                      ]
              }
            }
          : { ...item }
      );
    }
    case ADD_COMPETITOR_INFO_REC: {
      const { data, recId, comId, index } = action;
      const {
        standart_category,
        person_capacity,
        breakfast,
        free_cancel,
        hotel_title,
        hotel_id
      } = data;
      return state.map(item =>
        item.id === recId
          ? {
              ...item,
              parsing: {
                ...item.parsing,
                concurents: [
                  ...item.parsing.concurents.slice(0, index),
                  {
                    [comId]: {
                      hotel_id,
                      standart_category,
                      hotel_title,
                      person_capacity,
                      breakfast,
                      free_cancel
                    }
                  },
                  ...item.parsing.concurents.slice(index + 1)
                ]
              }
            }
          : { ...item }
      );
    }
    case ADD_OTHER_INFO_REC: {
      const { data, recId } = action;
      const { price_edges, otherSettings, excl_list } = data;
      const { profile_name, profile_group, current_category } = otherSettings;
      return state.map(item =>
        item.id === recId
          ? {
              ...item,
              profile_name,
              profile_group,
              current_category,
              price_edges,
              excl_list
            }
          : { ...item }
      );
    }
    case ADD_CATEGORY_PMS_REC: {
      const { data, recId } = action;
      return state.map(item =>
        item.id === recId
          ? {
              ...item,
              categorys: [data]
            }
          : { ...item }
      );
    }
    default:
      return state;
  }
};

const ownIdFromDB = (state = "", action) => {
  switch (action.type) {
    case GET_RECOMMENDATIONS_JSON_FULFILLED:
      return action.data.self_id;
    default:
      return state;
  }
};

const reducer = combineReducers({
  consumers,
  factorySettings,
  ownIdFromDB,
  isFetching,
  error
});

export default reducer;
