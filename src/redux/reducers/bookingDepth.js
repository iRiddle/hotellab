import { combineReducers } from "redux";

import {
  GET_BOOKING_DEPTH_PENDING,
  GET_BOOKING_DEPTH_REJECTED,
  GET_BOOKING_DEPTH_FULFILLED
} from "../actions/getBookingDepth";

const isFetching = (state = false, action) => {
  switch (action.type) {
    case GET_BOOKING_DEPTH_REJECTED:
    case GET_BOOKING_DEPTH_FULFILLED:
      return false;
    case GET_BOOKING_DEPTH_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = "", action) => {
  switch (action.type) {
    case GET_BOOKING_DEPTH_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const bookingDepth = (state = [], action) => {
  switch (action.type) {
    case GET_BOOKING_DEPTH_FULFILLED:
      return action.payload;
    default:
      return state;
  }
};

const reducer = combineReducers({
  isFetching,
  error,
  bookingDepth
});

export default reducer;
