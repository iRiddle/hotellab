import { combineReducers } from "redux";

import {
  COMPARE_WITH_LAST_YEAR_PENDING,
  COMPARE_WITH_LAST_YEAR_REJECTED,
  COMPARE_WITH_LAST_YEAR_FULFILLED,
} from "../actions/compareWithLastYear";

const isFetching = (state = false, action) => {
  switch (action.type) {
    case COMPARE_WITH_LAST_YEAR_REJECTED:
    case COMPARE_WITH_LAST_YEAR_FULFILLED:
      return false;
    case COMPARE_WITH_LAST_YEAR_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = "", action) => {
  switch (action.type) {
    case COMPARE_WITH_LAST_YEAR_REJECTED:
      return action.payload;
    default:
      return state;
  }
};

const comparedSalesPace = (state = [], action) => {
  switch (action.type) {
    case COMPARE_WITH_LAST_YEAR_FULFILLED:
      return action.payload;
    default:
      return state;
  }
};


const reducer = combineReducers({
  comparedSalesPace,
  isFetching,
  error,
});

export default reducer;
