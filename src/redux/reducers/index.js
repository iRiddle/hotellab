import { combineReducers } from "redux";
// import visibilityFilter from "./visibilityFilter";
import hotels from "./hotels";
import calendar from "./calendar";
import salesRate from "./salesRate";
import user from "./user";
import artboard from "./artboard";
import comparedSalesPace from "./compareWithLastYear";
import bookingDepth from "./bookingDepth";
import settings from "./settings";
import jsonFormed from "./jsonFormed";

export default combineReducers({
  hotels,
  calendar,
  salesRate,
  artboard,
  user,
  comparedSalesPace,
  bookingDepth,
  settings,
  jsonFormed
});
