import {
  SET_FILTER_YEAR_AGO_SALES_RATE,
  SET_FILTER_SOURCES_SALES_RATE,
  SET_SALES_RATE_DATE_RV,
  SET_SALES_RATE_DATE_RS,
  SET_FILTER_CATEGORY_SALES_RATE,
  SET_SALES_RATE,
  SET_SALES_DEPTH_DATE_RS
} from "../actionTypes";
import _config from "../../Config";
import moment from "moment";
let config = _config || {};

function formatDay(d) {
  let mm = d.split(".")[1];
  let dd = d.split(".")[0];
  let yy = d.split(".")[2];

  let ds = new Date(mm + "," + dd + "," + yy);
  let days = ["Вскр", "Пнд", "Втр", "Ср", "Чтв", "Птн", "Сб"];

  return days[ds.getDay()];
}

function copyTableParse(getData, getTotal) {
  let tableFact =
    "Дата,Д/недели,Проданные номера,ADR,Доход,Загрузка,RevPAR,Номеров в продаже\n";
  let factTotal = getTotal.fact;
  tableFact +=
    "" +
    "Итого, , " +
    factTotal.sales +
    ", " +
    factTotal.ADR +
    "₽, " +
    factTotal.profit +
    "₽, " +
    factTotal.load +
    "%, " +
    factTotal.RevPAR +
    ", " +
    factTotal.rooms +
    "\n";

  let fact = getData.fact;
  for (let ifact = 0; ifact < fact.length; ifact++) {
    tableFact +=
      "" +
      fact[ifact]["date"] +
      ", " +
      formatDay(fact[ifact]["date"]) +
      ", " +
      fact[ifact]["sales"] +
      ", " +
      fact[ifact]["ADR"] +
      "₽, " +
      fact[ifact]["profit"] +
      "₽, " +
      fact[ifact]["load"] +
      "%, " +
      parseInt(fact[ifact]["RevPAR"]) +
      ", " +
      fact[ifact]["rooms"] +
      "\n";
  }

  let tableDyn = "К/Н, ADR, Доход\n";
  let dynTotal = getTotal.dyn;
  tableDyn +=
    "" + dynTotal.kn + ", " + dynTotal.ADR + "₽, " + dynTotal.profit + "₽\n";

  let dyn = getData.dyn;
  for (let idyn = 0; idyn < dyn.length; idyn++) {
    tableDyn +=
      "" +
      dyn[idyn]["kn"] +
      ", " +
      dyn[idyn]["ADR"] +
      "₽, " +
      dyn[idyn]["profit"] +
      "₽\n";
  }

  return tableFact + "\n\n\n\n\n\n" + tableDyn;
}

function excelTableParse(getData, getTotal) {
  let tableFact =
    "Дата\tД/недели\tПроданные номера\tADR\tДоход\tЗагрузка\tRevPAR\tНомеров в продаже\t\n";
  let factTotal = getTotal.fact;
  tableFact +=
    "" +
    "Итого\t" +
    "\t" +
    factTotal.sales +
    "\t" +
    factTotal.ADR +
    "₽\t" +
    factTotal.profit +
    "₽\t" +
    factTotal.load +
    "%\t" +
    factTotal.RevPAR +
    "\t" +
    factTotal.rooms +
    "\t\n";

  let fact = getData.fact;
  for (let ifact = 0; ifact < fact.length; ifact++) {
    tableFact +=
      "" +
      fact[ifact]["date"] +
      "\t" +
      formatDay(fact[ifact]["date"]) +
      "\t" +
      fact[ifact]["sales"] +
      "\t" +
      fact[ifact]["ADR"] +
      "₽\t" +
      fact[ifact]["profit"] +
      "₽\t" +
      fact[ifact]["load"] +
      "%\t" +
      parseInt(fact[ifact]["RevPAR"]) +
      "\t" +
      fact[ifact]["rooms"] +
      "\t\n";
  }

  let tableDyn = "К/Н\tADR\tДоход\t\n";
  let dynTotal = getTotal.dyn;
  tableDyn +=
    "" + dynTotal.kn + "\t" + dynTotal.ADR + "₽\t" + dynTotal.profit + "₽\t\n";

  let dyn = getData.dyn;
  for (let idyn = 0; idyn < dyn.length; idyn++) {
    tableDyn +=
      "" +
      dyn[idyn]["kn"] +
      "\t" +
      dyn[idyn]["ADR"] +
      "₽\t" +
      dyn[idyn]["profit"] +
      "₽\t\n";
  }

  return tableFact + "\n\n\n\n\n\n" + tableDyn;
}

function setDateForFilterNew(data) {
  if (!data)
    return {
      loading: true
    };

  return {
    dateEnd: data.end,
    dateStart: data.start
  };
}

function newFormatDateInFilter(filters) {
  var newDate = new Date();
  var newDate = newDate.setMonth(newDate.getMonth() + 2);
  var newDate = new Date(newDate);

  if (!filters) {
    var dRsS =
      newDate.getDate() +
      "." +
      newDate.getMonth() +
      "." +
      newDate.getFullYear();
    var dRvS =
      newDate.getDate() +
      "." +
      newDate.getMonth() +
      "." +
      newDate.getFullYear();

    var getTypes = "loading";
  } else {
    var dRsS = filters.rsStart;
    var dRvS = filters.rvStart;
  }

  var dsRsE = new Date(
    newDate.getMonth() + "," + newDate.getDate() + "," + newDate.getFullYear()
  );
  var dsRsE = dsRsE.setDate(dsRsE.getDate() + 60);
  var dsRsE = new Date(dsRsE);

  var dsRsSt = new Date(
    newDate.getMonth() + "," + newDate.getDate() + "," + newDate.getFullYear()
  );
  var dsRsSt = dsRsSt.setMonth(dsRsSt.getMonth());
  var dsRsSt = new Date(dsRsSt);

  var dsRvSt = new Date(
    newDate.getMonth() + "," + newDate.getDate() + "," + newDate.getFullYear()
  );

  var dsRvSt = dsRvSt.setDate(
    moment()
      .add(-1, "days")
      .format("DD.MM.YYYY")
  );
  var dsRvSt = new Date(dsRvSt);

  switch (getTypes) {
    case "loading":
      var dsRsE = moment()
        .add(2, "M")
        .format("DD.MM.YYYY");
      var dsRsSt = moment().format("DD.MM.YYYY");
      var dsRvSt = moment()
        .add(-1, "days")
        .format("DD.MM.YYYY");
      break;

    default:
      var dsRsE = moment()
        .add(2, "M")
        .format("DD.MM.YYYY");
      var dsRsSt = moment().format("DD.MM.YYYY");
      var dsRvSt = moment()
        .add(-1, "days")
        .format("DD.MM.YYYY");

      break;
  }

  return {
    rs: {
      dateStart: dsRsSt,
      dateEnd: dsRsE
    },
    rv: {
      dateStart: dsRvSt,
      dateEnd: dsRsSt
    }
  };
}

function calcTotal(getData) {
  if (!getData)
    return {
      loading: true
    };
  let dataFact = getData.fact;
  let dataTotalFact = {
    sales: dataFact.sales,
    ADR: dataFact.ADR,
    profit: dataFact.profit,
    load: dataFact.load,
    RevPAR: dataFact.RevPAR,
    rooms: dataFact.rooms
  };

  let dataDyn = getData.dyn;
  let dataTotalDyn = {
    kn: dataDyn.kn,
    ADR: dataDyn.ADR,
    profit: dataDyn.profit
  };

  return {
    fact: dataTotalFact,
    dyn: dataTotalDyn
  };
}

function getInitial(config, state) {
  let updated = config.updateDate || "11.01.2018";
  if (!config || !state)
    return {
      loading: true,
      filterActiveDate: newFormatDateInFilter(config.filters)
    };

  var getFilterActiveCategory = state.filterActiveCategory || [];
  var getFilterActiveSources = state.filterActiveSources || [];
  var getFilterActiveDate = state.filterActiveDate;
  var getFilterActiveYearAgo = state.filterActiveYear_ago || false;

  const updatedFilterDate = {
    rs: { dateEnd: config.filters.rsEnd, dateStart: config.filters.rsStart },
    rv: { ...getFilterActiveDate.rv }
  };
  return {
    list: {
      data: config.data,
      dataTotal: calcTotal(config.dataTotal),
      filters: config.filters
    },
    filterActiveDate: updatedFilterDate,
    filterActiveCategory: getFilterActiveCategory,
    filterActiveSources: getFilterActiveSources,
    copy: {
      copy: copyTableParse(config.data, config.dataTotal),
      excel: excelTableParse(config.data, config.dataTotal)
    },
    filterActiveYear_ago: getFilterActiveYearAgo,
    totalRooms: config.totalRooms,
    updated
  };
}
const initialState = getInitial(config);

const salesRate = (state = initialState, action) => {
  switch (action.type) {
    case SET_SALES_RATE: {
      let newState = getInitial(action.payload.config, state);
      if (newState) return newState;
      else return state;
    }
    case SET_FILTER_CATEGORY_SALES_RATE: {
      return {
        ...state,
        filterActiveCategory: action.payload.id
      };
    }
    case SET_SALES_RATE_DATE_RS: {
      var postRv = {
        start: state.filterActiveDate.rv.dateStart,
        end: state.filterActiveDate.rv.dateEnd
      };
      return {
        ...state,
        filterActiveDate: {
          rs: setDateForFilterNew(action.payload.date),
          rv: setDateForFilterNew(postRv)
        }
      };
    }
    case SET_SALES_RATE_DATE_RV: {
      var postRs = {
        start: state.filterActiveDate.rs.dateStart,
        end: state.filterActiveDate.rs.dateEnd
      };
      return {
        ...state,
        filterActiveDate: {
          rs: setDateForFilterNew(postRs),
          rv: setDateForFilterNew(action.payload.date)
        }
      };
    }

    case SET_SALES_DEPTH_DATE_RS: {
      var postRs = {
        start: state.filterActiveDate.rs.dateStart,
        end: state.filterActiveDate.rs.dateEnd
      };
      return {
        ...state,
        filterActiveDate: {
          rs: setDateForFilterNew(postRs),
          rv: setDateForFilterNew(action.payload.date)
        }
      };
    }
    case SET_FILTER_SOURCES_SALES_RATE: {
      return {
        ...state,
        filterActiveSources: action.payload.id
      };
    }
    case SET_FILTER_YEAR_AGO_SALES_RATE: {
      return {
        ...state,
        filterActiveYear_ago: action.payload.id
      };
    }
    default: {
      return state;
    }
  }

  return state || {};
};

export default salesRate;
