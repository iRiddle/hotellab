import { SET_ARTBOARD } from "../actionTypes";
// import _config from "../../Config";
// let config = _config || {};

// function getInitial(config, state) {
//   if (!config || !state) {
//     return {
//       loading: true
//     };
//   }

//   return {
//     list: {
//       data: config
//     }
//   };
// }
// const initialState = getInitial(config);

const artboard = (state = {}, action) => {
  switch (action.type) {
    case SET_ARTBOARD: {
      // let newState = getInitial(action.payload.config, state);
      // if (newState) return newState;
      return action.payload;
    }
    default: {
      return state;
    }
  }
};

export default artboard;
