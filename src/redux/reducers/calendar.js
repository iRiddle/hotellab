import {
  SET_CALENDAR_FILTER_SELECTED_OPTIONS,
  SELECT_HOTEL,
  SET_CALENDAR_DATA,
  SET_STATE,
  SET_CALENDAR_SELECTED_DATE_DATA,
  SET_CALENDAR_SELECTED_DATE,
  START_LOADIONG_SELECTED_DATE_DETAILS
} from "../actionTypes";
import _config from "../../Config";

let config = _config;

const FIDS = ["roomTypes", "recommendations"];

function getFiltersStateFromConfig(config, currentHotel) {
  let filters = {};
  if (
    !currentHotel ||
    !config.hotels[currentHotel] ||
    !config.hotels[currentHotel].filters
  )
    return filters;
  for (var i in FIDS) {
    var fid = FIDS[i];
    var filter = { ...config.hotels[currentHotel].filters[fid] };
    filters[fid] = filter;
  }
  return filters;
}

function getInitialState(config) {
  if (!config)
    return {
      loading: true,
      filters: getFiltersStateFromConfig(config, currentHotel)
    };
  const currentHotel =
    config.currentHotel || Object.keys(config.hotels || {})[0];
  let updated = "11.01.2018";

  if (config.calendarUpdateDate) {
    let d = new Date(config.calendarUpdateDate);
    updated = d.getDate() + "." + d.getMonth() + "." + d.getFullYear();
  }
  if (
    config.hotels &&
    config.hotels[currentHotel] &&
    config.hotels[currentHotel].last_forecast
  ) {
    // TODO !!!
    updated = config.hotels[currentHotel].last_forecast;
  }
  let data = config.calendarData;
  let now = new Date();
  return {
    loading: false,
    currentHotel,
    filters: getFiltersStateFromConfig(config, currentHotel),
    data,
    updated,
    hotels: config.hotels,
    // currentDateDetails: {},
    selectedDateDetails: {
      loading: true
    },
    selectedDate: {
      y: now.getFullYear(),
      m: now.getMonth(),
      d: now.getDate()
    }
  };
}
const initialState = getInitialState(config);

const calendar = (state = initialState, action) => {
  switch (action.type) {
    case SET_STATE: {
      let newState = getInitialState(action.payload.config);
      if (newState) return newState;
      else return state;
    }
    case START_LOADIONG_SELECTED_DATE_DETAILS: {
      return { ...state, selectedDateDetails: { loading: true } };
    }
    case SET_CALENDAR_SELECTED_DATE_DATA: {
      return {
        ...state,
        selectedDateDetails: action.payload.data
      };
    }
    case SET_CALENDAR_SELECTED_DATE: {
      let selectedDate = {
        y: action.payload.y,
        m: action.payload.m,
        d: action.payload.d
      };
      return {
        ...state,
        selectedDate
      };
    }
    case SELECT_HOTEL: {
      let currentHotel = action.payload.hid;
      let updated = "11.01.2018";
      // if(config.hotels[currentHotel].last_forecast){ // TODO !!!
      //   updated = config.hotels[currentHotel].last_forecast;
      // }
      if (state.hotels[currentHotel].last_forecast) {
        // TODO !!!
        updated = state.hotels[currentHotel].last_forecast;
      }

      return {
        ...state,
        updated,
        currentHotel,
        filters: getFiltersStateFromConfig(state, action.payload.hid)
      };
    }
    case SET_CALENDAR_DATA: {
      // console.log(action.payload.data);

      return {
        ...state,
        data: action.payload.data
      };
    }
    case SET_CALENDAR_FILTER_SELECTED_OPTIONS: {
      let filters = { ...state.filters };
      filters[action.payload.filterId].selected = action.payload.value;
      // console.log(filters);

      // fetchCalendarData(filters, state.currentHotel)

      return {
        ...state,
        filters
      };
    }
    default: {
      return state;
    }
  }
};

export default calendar;
