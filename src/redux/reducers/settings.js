import { combineReducers } from "redux";

import {
  GET_COMPETITORS_PENDING,
  GET_COMPETITORS_FULFILLED,
  GET_COMPETITORS_REJECTED,
  SORT_COMPETITORS_BY
} from "../actions/settings";

const isFetching = (state = false, action) => {
  switch (action.type) {
    case GET_COMPETITORS_REJECTED:
    case GET_COMPETITORS_FULFILLED:
      return false;
    case GET_COMPETITORS_PENDING:
      return true;
    default:
      return state;
  }
};

const error = (state = "", action) => {
  switch (action.type) {
    case GET_COMPETITORS_REJECTED:
      return action.error;
    default:
      return state;
  }
};

const settings = (state = {}, action) => {
  switch (action.type) {
    case GET_COMPETITORS_FULFILLED:
      return action.data;
    case SORT_COMPETITORS_BY:
      const { sortBy } = action;
      const { competitors } = state;
      switch (sortBy) {
        case 0: {
          return {
            ...state,
            competitors: [...competitors].sort(
              (a, b) => a.distance - b.distance
            )
          };
        }
        case 1: {
          return {
            ...state,
            competitors: [...competitors].sort(
              (a, b) => b.distance - a.distance
            )
          };
        }
        case 2: {
          return {
            ...state,
            competitors: [...competitors].sort((a, b) => b.rating - a.rating)
          };
        }
      }
    default:
      return state;
  }
};

const reducer = combineReducers({
  settings,
  isFetching,
  error
});

export default reducer;
