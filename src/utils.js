const numeral = require("numeral");
const uniqid = require("uniqid");

const formatMoney = v => {
  if (v !== undefined) {
    if (isNaN(v) && v.includes("₽")) {
      const parseRuble = parseFloat(v.replace("₽", ""));
      return parseRuble.toFixed().replace(/\d(?=(\d{3})+$)/g, "$& ");
    }
    return v.toFixed().replace(/\d(?=(\d{3})+$)/g, "$& ");
  }
};

const determineStarsNumber = stars => {
  if (stars !== undefined && stars !== null) {
    if (stars.length === 1) {
      return {
        gte: stars[0],
        lte: stars[0]
      };
    }
    if (stars[1] > stars[0]) {
      return {
        gte: stars[0],
        lte: stars[1]
      };
    } else {
      return {
        gte: stars[1],
        lte: stars[0]
      };
    }
  }
  return {
    gte: 0,
    lte: 0
  };
};

const getUniqId = () => {
  return uniqid();
};

function copyToClipboard(str) {
  var el = document.createElement("textarea");
  el.value = str;
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
}

function formatDate(d) {
  let m = d.getMonth() + 1;
  let day = d.getDate();
  return (
    (day < 10 ? "0" : "") +
    day +
    "/" +
    (m < 10 ? "0" : "") +
    m +
    "/" +
    d.getFullYear()
  );
}

const formatFilterDate = d => {
  if (!d) {
    var ds = d;
  } else {
    var mm = d.split(".")[1];
    var dd = d.split(".")[0];
    var yy = d.split(".")[2];

    var ds = mm + "," + dd + "," + yy;
  }

  return ds;
};

function groupBy(getKey, items) {
  return items.reduce((groups, item) => {
    const k = getKey(item);
    if (!groups[k]) groups[k] = [item];
    else groups[k].push(item);
    return groups;
  }, {});
}
export {
  formatMoney,
  copyToClipboard,
  formatDate,
  groupBy,
  formatFilterDate,
  determineStarsNumber,
  getUniqId
};

numeral.register("locale", "ru", {
  delimiters: {
    thousands: " ",
    decimal: ","
  },
  abbreviations: {
    thousand: "тыс.",
    million: "млн.",
    billion: "млрд.",
    trillion: "трлн."
  },
  ordinal: function() {
    return ".";
  },
  currency: {
    symbol: "руб."
  }
});
