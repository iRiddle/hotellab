import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import store from "./redux/store";

// import './index.css';
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "./i18n";
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/css/bootstrap-theme.css';
import "bootstrap/dist/css/bootstrap.min.css";
import "./css/index.css";

// if(!window.config) {alert('Error!'); window.location = '/'; }
if (window.config && window.config.hasOwnProperty("action")) {
  switch (window.config.action) {
    case "redirect":
      window.location = window.config.url || "/";
      break;
    default:
      break;
  }
}

// basename="/devUI/"
ReactDOM.render(
  <Suspense fallback="loading">
    <Provider store={store}>
      <App />
    </Provider>
  </Suspense>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

window.hasOwnProperty("console") &&
  window.console.log(" ____     ____\n [__] ___ [__]    Hi\n\n");
