import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import Navbar from "./components/Navbar";
import Main from "./components/Main";
import { connect } from "react-redux";
import {
  fetchInitialState,
  fetchInitialStateSalesRate,
  fetchInitialStateArtboard
} from "./redux/actions";

const numeral = require("numeral");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dots: ""
    };
  }

  componentDidMount = () => {
    this.props.fetchInitialState();
    this.props.fetchInitialStateSalesRate();
    this.props.fetchInitialStateArtboard();
    numeral.locale("ru");
  };

  render() {
    if (this.props.loading) {
      return (
        <div
          className="loading_gag"
          style={{
            width: "620px",
            // textAlign: 'center',
            margin: "300px auto"
          }}
        >
          <h1>Ожидание данных от сервера{this.state.dots}</h1>
        </div>
      );
    }

    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Main />
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  const loading = state.calendar.loading;
  return { loading };
};

const mapDispatchToProps = dispatch => ({
  fetchInitialState: () => dispatch(fetchInitialState()),
  fetchInitialStateSalesRate: () => dispatch(fetchInitialStateSalesRate()),
  fetchInitialStateArtboard: () => dispatch(fetchInitialStateArtboard())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
