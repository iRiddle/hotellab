function Workbook() {
  if (!(this instanceof Workbook)) return new Workbook();

  this.SheetNames = [];

  this.Sheets = {};
}

const download = (url, name) => {
  let a = document.createElement("a");
  a.href = url;
  a.download = name;
  a.click();

  window.URL.revokeObjectURL(url);
};

function s2ab(s) {
  const buf = new ArrayBuffer(s.length);

  const view = new Uint8Array(buf);

  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;

  return buf;
}

export default (dataFact, dataTemps) => {
  const stepData = dataFact.map(factItem => {
    return {
      Дата: factItem.date || "Итого:",
      "Проданные номера": factItem.sales,
      ADR: factItem.ADR,
      Доход: factItem.profit,
      Загрузка: factItem.load,
      RevPAR: factItem.RevPAR,
      "Номеров в продаже": factItem.rooms
    };
  });
  import("xlsx").then(XLSX => {
    const wb = new Workbook();
    let ws = XLSX.utils.json_to_sheet(stepData);
    let ws2 = XLSX.utils.json_to_sheet(dataTemps);
    console.log(ws);
    // const wsAll  = 
    // const wsStep = Object.keys(ws).reduce((acc, key) => {
    //   if (key === "A1") {
    //     acc[key] = {
    //       ...ws[key],
    //       s: {
    //         font: {
    //           name: "Arial",
    //           sz: 12,
    //           bold: true,
    //           italic: false,
    //           wrapText: true,
    //           underline: false
    //         },
    //         alignment: {
    //           wrapText: true,
    //           vertical: "top"
    //         },
    //         fill: { fgColor: { rgb: "FFFFAA00" } },
    //         border: {
    //           top: { style: "thin", color: { auto: 1 } },
    //           right: { style: "thin", color: { auto: 1 } },
    //           bottom: { style: "thin", color: { auto: 1 } },
    //           left: { style: "thin", color: { auto: 1 } }
    //         }
    //       }
    //     };
    //   } else {
    //     acc[key] = { ...ws[key] };
    //   }
    //   return acc;
    // }, {});

    // console.log(wsStep);
    // ws.A2.s = {
    //   font: {
    //     name: "Arial",
    //     sz: 12,
    //     bold: true,
    //     italic: false,
    //     wrapText: true,
    //     underline: false
    //   },
    //   alignment: {
    //     wrapText: true,
    //     vertical: "top"
    //   },
    //   fill: { fgColor: { rgb: "FFFFAA00" } },
    //   border: {
    //     top: { style: "thin", color: { auto: 1 } },
    //     right: { style: "thin", color: { auto: 1 } },
    //     bottom: { style: "thin", color: { auto: 1 } },
    //     left: { style: "thin", color: { auto: 1 } }
    //   }
    // };
    wb.SheetNames.push("");
    wb.Sheets[""] = ws;
    // wb.Sheets[""] = ws2;
    console.log(wb);
    const wbout = XLSX.write(wb, {
      bookType: "xlsx",
      bookSST: true,
      type: "binary"
    });

    let url = window.URL.createObjectURL(
      new Blob([s2ab(wbout)], { type: "application/octet-stream" })
    );

    download(url, "fact.xlsx");
  });
};
// ================================
// const spy = {
//     A1: { id: 1 },
//     A2: { id: 2 },
//     A3: { id: 3 }
//   };

//   const spy2 = Object.keys(spy).reduce((acc, key) => {
//     if (key === "A1") {
//       acc[key] = { ...spy[key], req: "redacted" };
//     } else {
//       acc[key] = { ...spy[key] };
//     }
//     return acc;
//   }, {});

//   console.log(spy2);
// ================================

// var excelCell = {
//   v: coloumnVal,
//   t: "s",
//   s: {
//     font: {
//       name: "Arial",
//       sz: 12,
//       bold: true,
//       italic: false,
//       wrapText: true,
//       underline: false
//     },
//     alignment: {
//       wrapText: true,
//       vertical: "top"
//     },
//     fill: { fgColor: { rgb: "FFFFAA00" } },
//     border: {
//       top: { style: "thin", color: { auto: 1 } },
//       right: { style: "thin", color: { auto: 1 } },
//       bottom: { style: "thin", color: { auto: 1 } },
//       left: { style: "thin", color: { auto: 1 } }
//     }
//   }
// };

// const spy = {
//   A1: { key: 1, log: 2 },
//   A2: { key: 1, log: 2 },
//   A3: { key: 1, log: 2 }
// };
// const spyUp = Object.keys(spy).reduce((acc, key) => {
//     console.log(acc)
//   acc[key] = { ...acc[key], test: "test" };
//   return acc;
// }, {});

// console.log(spyUp);

// const spy = {
//     A1: { key: 1, log: 2 },
//     A2: { key: 1, log: 2 },
//     A3: { key: 1, log: 2 }
//   };
//   const spyUp = Object.keys(spy).reduce((acc, key) => return {...acc[key], 'test': '13' }, {})
//   console.log(spyUp);

// const spy = {
//     A1: { key: 1, log: 2 },
//     A2: { key: 1, log: 2 },
//     A3: { key: 1, log: 2 }
//   };
//   for (var key in spy) {
//       // skip loop if the property is from prototype
//       if (!spy.hasOwnProperty(key)) continue;

//       var obj = spy[key];
//   console.log(obj);
//       for (var prop in obj) {
//           // skip loop if the property is from prototype
//           if(!obj.hasOwnProperty(prop)) continue;

//           // your code
//           alert(prop + " = " + obj[prop]);
//       }
//   }

//   console.log(spyUp);

// const spy = {
//   A1: { key: 1, log: 2 },
//   A2: { key: 1, log: 2 },
//   A3: { key: 1, log: 2 }
// };
// const spyUp = Object.keys(spy).reduce((acc, key) => {
//   return { acc: { ...key[acc], test: "test" } };
// }, {});
// console.log(spyUp);
