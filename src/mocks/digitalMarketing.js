export const headerInfo = [
  {
    id: 1,
    title: "constants.websiteRevenue",
    count: 215000,
    statistics: "constants.lastYear",
    statisticsCount: 5,
    isDangerous: false
  },
  {
    id: 2,
    title: "constants.newVisitors",
    count: 1150,
    statistics: "constants.previousPeriod",
    statisticsCount: -2,
    isDangerous: true
  },
  {
    id: 3,
    title: "constants.adrFromTheSite",
    count: 4500,
    statistics: "constants.lastYear",
    statisticsCount: 4,
    isDangerous: false
  },
  {
    id: 4,
    title: "constants.lastConversion",
    count: "4,3",
    statistics: "constants.lastYear",
    statisticsCount: 3,
    isDangerous: false
  },
  {
    id: 5,
    title: "constants.OTA",
    count: 45000,
    statistics: "constants.lastYear",
    statisticsCount: 3,
    isDangerous: false
  }
];

export const bookingFromSite = [
  { id: 1, count: "", date: 29, color: "booking-color-white" },
  { id: 2, count: "", date: 30, color: "booking-color-white" },
  { id: 3, count: 25, date: 1, color: "booking-color-gray" },
  { id: 4, count: 2, date: 2, color: "booking-color-red" },
  { id: 5, count: 56, date: 3, color: "booking-color-green" },
  { id: 6, count: 12, date: 4, color: "booking-color-red" },
  { id: 7, count: 6, date: 5, color: "booking-color-red" },
  { id: 8, count: 57, date: 6, color: "booking-color-green" },
  { id: 9, count: 58, date: 7, color: "booking-color-green" },
  { id: 10, count: 45, date: 8, color: "booking-color-green" },
  { id: 11, count: 11, date: 9, color: "booking-color-red" },
  { id: 12, count: 0, date: 10, color: "booking-color-red" },
  {
    id: 13,
    count: 14,
    date: 11,
    color: "booking-color-light-red"
  },
  {
    id: 14,
    count: 13,
    date: 12,
    color: "booking-color-light-red"
  },
  {
    id: 15,
    count: 15,
    date: 13,
    color: "booking-color-light-red"
  },
  { id: 16, count: 12, date: 14, color: "booking-color-red" },
  { id: 17, count: 46, date: 15, color: "booking-color-green" },
  { id: 18, count: 54, date: 16, color: "booking-color-green" },
  { id: 19, count: 57, date: 17, color: "booking-color-green" },
  { id: 20, count: 145, date: 18, color: "booking-color-green" },
  { id: 21, count: 1879, date: 19, color: "booking-color-green" },
  {
    id: 22,
    count: 14,
    date: 20,
    color: "booking-color-light-red"
  },
  {
    id: 23,
    count: 17,
    date: 21,
    color: "booking-color-light-red"
  },
  { id: 24, count: 23, date: 22, color: "booking-color-gray" },
  { id: 25, count: 26, date: 23, color: "booking-color-gray" },
  { id: 26, count: 23, date: 24, color: "booking-color-gray" },
  { id: 27, count: 28, date: 25, color: "booking-color-gray" },
  { id: 28, count: 45, date: 26, color: "booking-color-green" },
  { id: 29, count: 46, date: 27, color: "booking-color-green" },
  { id: 30, count: 47, date: 28, color: "booking-color-green" },
  { id: 31, count: 54, date: 29, color: "booking-color-green" },
  { id: 32, count: 58, date: 30, color: "booking-color-green" },
  { id: 33, count: 24, date: 31, color: "booking-color-gray" },
  { id: 34, count: "", date: 1, color: "booking-color-white" },
  { id: 35, count: "", date: 2, color: "booking-color-white" }
];

export const leadTable = [
  {
    id: 1,
    title: "constants.search",
    calls: 5,
    chats: 25,
    requests: 10,
    leads: 40,
    spentLeads: 42640,
    costLead: 1066,
    earn: 42640,
    adr: 3800,
    soldNums: 100
  },
  {
    id: 2,
    title: "constants.yan",
    calls: 0,
    chats: 0,
    requests: 5,
    leads: 5,
    spentLeads: 15000,
    costLead: 3000,
    earn: 15000,
    adr: 2000,
    soldNums: 24
  },
  {
    id: 3,
    title: "constants.target",
    calls: 17,
    chats: 22,
    requests: 64,
    leads: 103,
    spentLeads: 70090,
    costLead: 690,
    earn: 70090,
    adr: 2700,
    soldNums: 67
  },
  {
    id: 4,
    title: "constants.2gis",
    calls: 13,
    chats: 5,
    requests: 5,
    leads: 23,
    spentLeads: 5295,
    costLead: 230,
    earn: 5295,
    adr: 123750,
    soldNums: 568
  }
];

export const brandPhrases = [
  { id: 0, title: "constants.lastSearchPhrase", visits: "constants.visits" },
  { id: 1, title: "Library отель москва", visits: 9 },
  { id: 2, title: "Лайбрари отель", visits: 5 },
  { id: 3, title: "library hotel moscow", visits: 6457 },
  { id: 4, title: "hotel moscow luxury", visits: 342 },
  { id: 5, title: "гостиница лайбрари москва россия", visits: 1 }
];

export const trafficAnalytics = [
  // not used
  {
    id: 1,
    title: "Переходы из поисковых систем",
    color: "traffic-green",
    count: 25
  },
  { id: 2, title: "Прямые заходы", color: "traffic-red", count: 34 },
  {
    id: 3,
    title: "Внутренние переходы",
    color: "traffic-light-green",
    count: 43
  },
  {
    id: 4,
    title: "Переходы из социальных сетей",
    color: "traffic-pink",
    count: 1
  }
];

export const rateFutureDates = [
  {
    id: 1,
    date: "20.09",
    loaded: 100,
    testColor: "rateFuture-color1",
    count: 105216,
    color: "#009688"
  },
  {
    id: 2,
    date: "21.09",
    loaded: 85,
    testColor: "rateFuture-color2",
    count: 99876,
    color: "#1b8478"
  },
  {
    id: 3,
    date: "19.09",
    loaded: 82,
    testColor: "rateFuture-color3",
    count: 80457,
    color: "#396f66"
  },
  {
    id: 4,
    date: "18.09",
    loaded: 72,
    testColor: "rateFuture-color4",
    count: 65487,
    color: "#515f58"
  },
  {
    id: 5,
    date: "22.09",
    loaded: 62,
    testColor: "rateFuture-color5",
    count: 40923,
    color: "#6e4b46"
  },
  {
    id: 6,
    date: "23.09",
    loaded: 50,
    testColor: "rateFuture-color6",
    count: 35678,
    color: "#8a3735"
  },
  {
    id: 7,
    date: "25.09",
    loaded: 43,
    testColor: "rateFuture-color7",
    count: 25674,
    color: "#a42525"
  }
];
