import React, { Component } from "react";

import alasql from "alasql";
import XLSX from "xlsx";

import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";

import {
  fetchInitialStateSalesRate,
  SetFilterCategory,
  SetFilterSources,
  SetFilterYearAgo
} from "./../redux/actions";
import { compareWithLastYear } from "../redux/actions/compareWithLastYear";
import {
  getCurrentHotel,
  getCurrentDate,
  getCurrentDateLiving,
  totalRooms,
  getCurrencySymbolSelector,
  getUpdateDateSelector,
  getListDataFactSelector,
  getListDataTotalFactSelector,
  getListDataDynSelector,
  getListDataTotalDynSelector,
  getListFiltersCategorySelector,
  getListFiltersSourcesSelector,
  getFilterActiveDateSelector,
  getFilterActiveYearAgoSelector,
  getCopyPluginSelector
} from "../redux/selectors";

import {
  getDifferenceFromStore,
  getFilterActiveCategory,
  getFilterActiveSources
} from "../redux/selectors/sales";

import { copyToClipboard } from "../utils";

function Wrapper(WrappedComponent) {
  return class extends Component {
    export = () => {
      alasql.utils.isBrowserify = false;
      alasql.utils.global.XLSX = XLSX;

      const data1 = alasql('SELECT * FROM HTML("#table1",{headers:true})');
      const data2 = alasql('SELECT * FROM HTML("#table2",{headers:true})');
      var opts = [
        { sheetid: "Факт", header: true },
        { sheetid: "Темпы продаж", header: false }
      ];
      alasql('SELECT INTO XLSX("Статистика.xlsx",?) FROM ?', [
        opts,
        [data1, data2]
      ]);
    };
    state = {
      fact: {
        data: this.props.listDataFactSelector,
        dataSum: this.props.listDataTotalFactSelector
      },
      dyn: {
        data: this.props.listDataDynSelector,
        dataSum: this.props.listDataTotalDynSelector
      },
      updateDate: this.props.updateDate,
      dataFilter: {
        category: [],
        filterDateStock: this.props.filterActiveDateSelector,
        sources: [],
        year_ago: this.props.filterActiveYearAgoSelector
      },

      stepPeriodArray: [],
      stepSalesArray: [],

      period: "day",
      isEqualWithLastYear: false,
      fields: [],
      getDataToExcelTrigger: false,
      daysWeek: [
        "Понедельник",
        "Вторник",
        "Среда",
        "Четверг",
        "Пятница",
        "Суббота",
        "Воскресенье"
      ],
      currentNds: 0,
      currentBreakfast: 0,
      ndsArray: [
        { id: 0, title: "С НДС" },
        { id: 1, title: "Без НДС" }
      ],
      breakfastArray: [
        { id: 0, title: "С завтраком" },
        { id: 1, title: "Без завтрака" }
      ]
    };

    static getDerivedStateFromProps(nextProps, prevState) {
      if (
        nextProps.listDataFactSelector !== prevState.fact.data ||
        nextProps.listDataTotalFactSelector !== prevState.fact.dataSum ||
        nextProps.listDataDynSelector !== prevState.dyn.data ||
        nextProps.listDataTotalDynSelector !== prevState.dyn.dataSum
      ) {
        return {
          fact: {
            data: nextProps.listDataFactSelector,
            dataSum: nextProps.listDataTotalFactSelector
          },
          dyn: {
            data: nextProps.listDataDynSelector,
            dataSum: nextProps.listDataTotalDynSelector
          }
        };
      }
      if (nextProps.updateDate !== prevState.updateDate) {
        return {
          updateDate: nextProps.updateDate
        };
      }
      if (
        nextProps.listDataDynSelector !== prevState.dyn.data ||
        nextProps.listDataTotalDynSelector !== prevState.dyn.dataSum
      ) {
        return {
          dyn: {
            data: nextProps.listDataDynSelector,
            dataSum: nextProps.listDataTotalDynSelector
          }
        };
      }
      if (
        nextProps.listFiltersCategorySelector !==
          prevState.dataFilter.category ||
        nextProps.filterActiveDateSelector !==
          prevState.dataFilter.filterDateStock ||
        nextProps.listFiltersSourcesSelector !== prevState.dataFilter.sources ||
        nextProps.filterActiveYearAgoSelector !== prevState.dataFilter.year_ago
      ) {
        return {
          dataFilter: {
            category: nextProps.listFiltersCategorySelector,
            filterDateStock: nextProps.filterActiveDateSelector,
            sources: nextProps.listFiltersSourcesSelector,
            year_ago: nextProps.filterActiveYearAgoSelector
          }
        };
      }
      if (localStorage.getItem("i18nextLng") !== nextProps.currentLanguage) {
        return {
          ndsArray: [
            { id: 0, title: nextProps.t("vatIncluded") },
            { id: 1, title: nextProps.t("vatWithout") }
          ]
        };
      }
      return;
    }

    componentDidMount() {
      document.title = "Темпы продаж ⚠";
      this.props.fetchTest();
    }

    componentDidUpdate(prevProps, prevState) {
      // console.log(
      //   this.props.filterActiveCategory,
      //   this.props.filterActiveSources
      // );
      if (this.props.listDataFactSelector !== prevState.fact.data) {
        this.showByPeriod(this.state.period, this.props.listDataFactSelector);
      }

      if (
        prevState.currentNds !== this.state.currentNds ||
        prevState.currentBreakfast !== this.state.currentBreakfast
      ) {
        this.props.filterByNds(
          this.state.currentNds,
          this.state.currentBreakfast
        );
      }

      if (
        prevProps.filterActiveCategory !== this.props.filterActiveCategory ||
        prevProps.filterActiveSources !== this.props.filterActiveSources
      ) {
        this.props.fetchTest();
      }
    }

    copyCSV = () => {
      copyToClipboard(this.props.copyPluginSelector.copy);
    };

    formatDay = (d, isStepDay) => {
      const { t } = this.props;
      let mm = d.split(".")[1];
      let dd = d.split(".")[0];
      let yy = d.split(".")[2];
      let ds = new Date(mm + "," + dd + "," + yy);
      let days = [
        t("days.sun"),
        t("days.mon"),
        t("days.tue"),
        t("days.wed"),
        t("days.th"),
        t("days.fri"),
        t("days.sat")
      ];

      switch (isStepDay) {
        case "week":
          return days.length - ds.getDay() + 1;
        case "month":
          return ds.getDay();
        default:
          return days[ds.getDay()];
      }
    };
    // TODO: перенести в utils
    formatFilterDate = d => {
      if (!d) {
        var ds = d;
      } else {
        var mm = d.split(".")[1];
        var dd = d.split(".")[0];
        var yy = d.split(".")[2];

        var ds = mm + "," + dd + "," + yy;
      }

      return ds;
    };

    selectNds = id => {
      id === "С НДС" || id === "VAT included"
        ? this.setState({
            currentNds: 0
          })
        : this.setState({
            currentNds: 1
          });
    };

    selectBreakfast = id => {
      id === "С завтраком" || id === "With breakfast"
        ? this.setState({
            currentBreakfast: 0
          })
        : this.setState({
            currentBreakfast: 1
          });
    };

    setFilterOption = (filter, data) => {
      if (filter === "roomCategory") {
        this.props.filterCategory(data);
      } else {
        this.props.filterSources(data);
      }
    };

    handlePenultimateYear = () => {
      const { isEqualWithLastYear, period } = this.state;
      const {
        getCurrentHotelData,
        compareWithLastYearAction,
        getCurrentDateData,
        getCurrentDateLivingData
      } = this.props;
      const getStartDate = getCurrentDateData.dateStart;
      const getEndDate = getCurrentDateData.dateEnd;

      const getStartLivingDate = getCurrentDateLivingData.dateStart;
      const getEndLivingDate = getCurrentDateLivingData.dateEnd;
      if (!isEqualWithLastYear) {
        compareWithLastYearAction(
          getCurrentHotelData,
          getStartDate,
          getEndDate,
          getStartLivingDate,
          getEndLivingDate,
          true,
          period,
          () =>
            this.setState({
              isEqualWithLastYear: true
            })
        );
      } else {
        this.setState({
          isEqualWithLastYear: false
        });
      }
    };

    showByPeriod = (period, force) => {
      const { fact, dyn } = this.state;
      let stepPeriodArray = [];
      let stepSalesArray = [];
      switch (period) {
        case "day": {
          this.setState({
            fact: {
              data: this.props.listDataFactSelector,
              dataSum: this.props.listDataTotalFactSelector
            },
            isEqualWithLastYear: false,
            period,
            stepPeriodArray: [],
            stepSalesArray: []
          });
          break;
        }

        case "week": {
          let stepDaySales =
            force !== undefined
              ? this.formatDay(force[0].date, "week")
              : this.formatDay(dyn.data[0].date, "week");

          let stepDay =
            force !== undefined
              ? this.formatDay(force[0].date, "week")
              : this.formatDay(fact.data[0].date, "week");
          for (let i = 0, j = fact.data.length; i < j; i += stepDay) {
            stepDay = this.formatDay(fact.data[i].date, "week");

            stepDaySales = this.formatDay(dyn.data[i].date, "week");

            const tempArray = fact.data.slice(i, i + stepDay);
            stepPeriodArray = [
              ...stepPeriodArray,
              this.parseInfoFromPeriodArray(tempArray, "temp", false)
            ];

            const tempArraySales = dyn.data.slice(i, i + stepDaySales);
            stepSalesArray = [
              ...stepSalesArray,
              this.parseInfoFromPeriodArray(tempArraySales, "sales", false)
            ];
          }

          this.setState({
            period,
            stepPeriodArray,
            stepSalesArray,

            isEqualWithLastYear: false
          });
          break;
        }
        case "month": {
          for (let i = 0; i < fact.data.length; i++) {
            let dateString = moment(fact.data[i].date, "DD.MM.YYYY")
              .endOf("month")
              .format("DD.MM.YYYY");
            let indexByDateStr =
              fact.data.findIndex(el => el.date === dateString) !== -1
                ? fact.data.findIndex(el => el.date === dateString)
                : fact.data.length;
            let tempArray = fact.data.slice(i, indexByDateStr + 1);
            let tempArraySales = dyn.data.slice(i, indexByDateStr + 1);
            i = indexByDateStr === -1 ? fact.data.length : indexByDateStr;

            stepPeriodArray = [
              ...stepPeriodArray,
              this.parseInfoFromPeriodArray(tempArray, "temp", false)
            ];

            stepSalesArray = [
              ...stepSalesArray,
              this.parseInfoFromPeriodArray(tempArraySales, "sales", false)
            ];
          }
          this.setState({
            stepPeriodArray,
            stepSalesArray,
            period,
            isEqualWithLastYear: false
          });
          break;
        }
        case "dayWeek": {
          let dayWeekArray = {
            monday: [],
            tuesday: [],
            wednesday: [],
            thursday: [],
            friday: [],
            saturday: [],
            sunday: []
          };

          let dayWeekArraySales = {
            monday: [],
            tuesday: [],
            wednesday: [],
            thursday: [],
            friday: [],
            saturday: [],
            sunday: []
          };

          for (let i = 0; i < fact.data.length; i++) {
            let dateString = moment(fact.data[i].date, "DD.MM.YYYY").format(
              "dddd"
            );
            switch (dateString) {
              case "Monday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  monday: [...dayWeekArray.monday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  monday: [...dayWeekArraySales.monday, dyn.data[i]]
                };
                break;
              }
              case "Tuesday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  tuesday: [...dayWeekArray.tuesday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  tuesday: [...dayWeekArraySales.tuesday, dyn.data[i]]
                };
                break;
              }
              case "Wednesday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  wednesday: [...dayWeekArray.wednesday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  wednesday: [...dayWeekArraySales.wednesday, dyn.data[i]]
                };
                break;
              }
              case "Thursday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  thursday: [...dayWeekArray.thursday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  thursday: [...dayWeekArraySales.thursday, dyn.data[i]]
                };
                break;
              }
              case "Friday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  friday: [...dayWeekArray.friday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  friday: [...dayWeekArraySales.friday, dyn.data[i]]
                };
                break;
              }
              case "Saturday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  saturday: [...dayWeekArray.saturday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  saturday: [...dayWeekArraySales.saturday, dyn.data[i]]
                };
                break;
              }
              case "Sunday": {
                dayWeekArray = {
                  ...dayWeekArray,
                  sunday: [...dayWeekArray.sunday, fact.data[i]]
                };
                dayWeekArraySales = {
                  ...dayWeekArraySales,
                  sunday: [...dayWeekArraySales.sunday, dyn.data[i]]
                };
                break;
              }
              default:
                break;
            }
          }

          for (let data in dayWeekArray) {
            stepPeriodArray = [
              ...stepPeriodArray,
              this.parseInfoFromPeriodArray(dayWeekArray[data], "temp", true)
            ];
          }

          for (let data in dayWeekArraySales) {
            stepSalesArray = [
              ...stepSalesArray,

              this.parseInfoFromPeriodArray(
                dayWeekArraySales[data],
                "sales",
                true
              )
            ];
          }

          for (let i = 0; i < stepPeriodArray.length; i++) {
            if (!isNaN(stepPeriodArray[i].ADR)) {
              stepSalesArray[i] = { ...stepSalesArray[i], isNanObj: true };
            }
          }

          this.setState({
            stepPeriodArray,
            stepSalesArray,
            period,
            isEqualWithLastYear: false
          });
          break;
        }
        default:
          break;
      }
    };

    parseInfoFromPeriodArray = (array, isArray, isDayWeek) => {
      const { allNumbersHoter } = this.props;
      let date = "";
      if (!isDayWeek) {
        date = `${array.slice(0, 1)[0].date} - ${array.slice(-1)[0].date}`;
      }

      if (isArray === "temp") {
        const parsedArray = array.reduce(
          (accumulator, currentValue) => {
            return {
              ADR: accumulator.ADR + currentValue.ADR,
              RevPAR: accumulator.RevPAR + currentValue.RevPAR,
              load: accumulator.load + currentValue.load,
              rooms: accumulator.rooms + currentValue.rooms,
              sales: accumulator.sales + currentValue.sales,
              profit: accumulator.profit + currentValue.profit
            };
          },
          {
            ADR: 0,
            RevPAR: 0,
            load: 0,
            profit: 0,
            rooms: 0,
            sales: 0
          }
        );
        const combinedObject = {
          ...parsedArray,
          date,
          ADR: parsedArray.profit / parsedArray.sales,
          load: parseInt(parsedArray.load / array.length),
          RevPAR: parseInt(
            parsedArray.profit / (allNumbersHoter * array.length)
          )
        };
        return combinedObject;
      } else {
        const parsedArray = array.reduce(
          (accumulator, currentValue) => {
            return {
              kn: accumulator.kn + currentValue.kn,
              ADR: accumulator.ADR + currentValue.ADR,
              profit: accumulator.profit + currentValue.profit
            };
          },
          {
            kn: 0,
            ADR: 0,
            profit: 0
          }
        );
        const combinedObject = {
          ...parsedArray,
          ADR: parsedArray.profit / parsedArray.kn,
          kn: parseInt(parsedArray.kn),
          profit: parseInt(parsedArray.profit)
        };
        return combinedObject;
      }
    };

    getDataToExcel = currentPack => {
      const parseDynData = this.state.dyn.data.map(item => {
        return {
          knTemps: item.kn,
          adrTemps: item.ADR,
          profitTemps: item.profit
        };
      });

      const parseDynDataSum = {
        knTemps: this.state.dyn.dataSum.kn,
        adrTemps: this.state.dyn.dataSum.ADR,
        profitTemps: this.state.dyn.dataSum.profit
      };

      this.setState({
        fields: [
          currentPack.dataSum,
          ...currentPack.data,
          [parseDynDataSum, ...parseDynData]
        ]
      });
      return [
        currentPack.dataSum,
        ...currentPack.data,
        parseDynDataSum,
        ...parseDynData
      ];
    };

    render() {
      console.log(this.props);
      return (
        <WrappedComponent
          {...this.state}
          {...this.props}
          formatFilterDate={this.formatFilterDate}
          formatDay={this.formatDay}
          showByPeriod={this.showByPeriod}
          exportToExcel={this.export}
          handlePenultimateYear={this.handlePenultimateYear}
          selectNds={this.selectNds}
          selectBreakfast={this.selectBreakfast}
          setFilterOption={this.setFilterOption}
        />
      );
    }
  };
}

const mapStateToProps = state => ({
  getCurrentHotelData: getCurrentHotel(state),
  getCurrentDateData: getCurrentDate(state),
  getCurrentDateLivingData: getCurrentDateLiving(state),
  currencySymbol: getCurrencySymbolSelector(state),
  allNumbersHoter: totalRooms(state),
  parseDifference: getDifferenceFromStore(state),
  updateDate: getUpdateDateSelector(state),
  listDataFactSelector: getListDataFactSelector(state),
  listDataTotalFactSelector: getListDataTotalFactSelector(state),
  listDataDynSelector: getListDataDynSelector(state),
  listDataTotalDynSelector: getListDataTotalDynSelector(state),
  listFiltersCategorySelector: getListFiltersCategorySelector(state),
  listFiltersSourcesSelector: getListFiltersSourcesSelector(state),
  filterActiveDateSelector: getFilterActiveDateSelector(state),
  filterActiveYearAgoSelector: getFilterActiveYearAgoSelector(state),
  copyPluginSelector: getCopyPluginSelector(state),
  filterActiveCategory: getFilterActiveCategory(state),
  filterActiveSources: getFilterActiveSources(state)
});

const mapDispatchToProps = dispatch => ({
  filterCategory: id => {
    dispatch(SetFilterCategory(id));
    // dispatch(fetchInitialStateSalesRate());
  },

  filterSources: id => {
    dispatch(SetFilterSources(id));
    // dispatch(fetchInitialStateSalesRate());
  },
  fetchTest: () => {
    dispatch(fetchInitialStateSalesRate());
  },
  filterYearAgo: id => {
    dispatch(SetFilterYearAgo(id));
    dispatch(fetchInitialStateSalesRate());
  },
  filterByNds: (nds, breakfast) => {
    dispatch(fetchInitialStateSalesRate(nds, breakfast));
  },
  compareWithLastYearAction: (
    hotel,
    periodStart,
    periodEnd,
    periodStartLive,
    periodEndLive,
    year_ago,
    byPeriod,
    callback
  ) => {
    dispatch(
      compareWithLastYear(
        hotel,
        periodStart,
        periodEnd,
        periodStartLive,
        periodEndLive,
        year_ago,
        byPeriod,
        callback
      )
    );
  }
});

const composeHOC = compose(
  connect(mapStateToProps, mapDispatchToProps),
  Wrapper
);

export default composeHOC;
